import React, { Component } from 'react';

import Home from './pages/home';

// import PageDepartamentos from './pages/departamentos';

class App extends Component {
    render() { 
        return ( 
            // <Home/>
            <Home from={this.props.location.pathname} />
        );
    }
}
 
export default App;