import React, { Component } from 'react'
import PageCadastro from './pages/cadastro'

export default class Cadastro extends Component {
    render() {
        return (
            <PageCadastro from={this.props.location.state ? this.props.location.state.from : "/"} />
        )
    }
}
