import React, { Component } from 'react';

import PageCarrinho from './pages/carrinho';

class Carrinho extends Component {
    render() { 
        return ( 
            // <Home/>
            <PageCarrinho from={this.props.location.pathname} />
        );
    }
}
 
export default Carrinho;