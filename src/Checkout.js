import React, { Component } from 'react'
import Checkout from './pages/checkout'

export default class CheckoutPage extends Component {
    render() {
        return (
            <Checkout from={this.props.location.state ? this.props.location.state.from : "/"} />
        )
    }
}
