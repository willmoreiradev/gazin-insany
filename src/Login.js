import React, { Component } from 'react'
import PageLogin from './pages/login'

export default class Login extends Component {
    render() {
        return (
            <PageLogin from={this.props.location.state ? this.props.location.state.from : "/"} />
        )
    }
}
