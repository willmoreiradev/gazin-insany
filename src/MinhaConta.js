import React, { Component } from 'react'
import MinhaConta from './pages/minhaConta'

export default class MinhaContaPage extends Component {
    render() {
        return (
            <MinhaConta from={this.props.location.pathname ? this.props.location.pathname : "/conta/dados"} />
        )
    }
}
