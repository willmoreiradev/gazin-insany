import styled from 'styled-components';

import { BoxWhite } from '../pages/checkout/styles';

export const MultiInput = styled.div`
    width: 100%;
    display: flex;
    div{
        &:first-child{
            width: 326px;
            margin-right: 32px;
        }
        &:last-child{
            width: 109px
        }
    }
    @media(max-width: 1200px) {
        flex-direction: column;
        margin-bottom: 15px;
        div {
            width: 100% !important;
            margin: 0 !important;
            margin-bottom: 15px !important;
            &:last-child {
                margin-bottom: 0px !important;
            }
        }
    }

`

export const StyleInput = styled.div`
    margin-bottom: 24px;
    position: relative;
    .topInput{
        display: flex;
        justify-content: space-between;
        align-items: center;
        width: 100%;
        span{
            font-family: 'Inter';
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
            text-align: center;
            letter-spacing: -0.1px;
            color: #8A8A8A;
        }
    }
    label {
        font: normal 500 14px/17px 'Inter'; 
        color: #363843;
        display: block;
        margin-bottom: 11px;
    }
    div {
        position: relative;
    }
    input {
        width: 100%;
        height: 54px;
        border: 1px solid #DCDDE3;
        border-radius: 6px;
        padding: 0px 18px;
        font-size: 14px;
        color: #A0A4B3;
    }
    img{
        position: absolute;
        top: 19px;
        right: 32px;
        cursor: pointer;
    }
    select {
        width: 109px;
        height: 54px;
    }
    @media(max-width: 1200px) {
        &:last-child {
            margin-bottom: 0px;
        }
        select {
            width: 100%;
        }
    }
    @media(max-width: 480px) {
        margin-bottom: 15px;
    }
`;


export const GroupCheckBox = styled.div`
    display: flex;
    align-items: center;
  div {
    display: flex;
    align-items: center;
    margin-left: 46px;
    &:first-child {
        margin-left: 0px;
    }
    input {
        width: auto;
        height: auto;
        padding: 0;
        margin-right: 8px;
    }
    label {
        margin-bottom: 0px;
        font-size: 16px;
        line-height: 19px;
        color: #646981;
    }
  }
`;

export const RadioButton = styled.div`
  display: flex;
  align-items: center;
  label {
      width: auto;
      margin: 0;
  }
  input {
        width: auto;
        height: auto;
        margin-right: 15px;
        font-size: 16px;
        line-height: 19px;
        color: #646981;
  }
`;

export const DoubleInput = styled.div`
  display: flex;
  justify-content: space-between;
  .input-endereco {
    width: 290px;
  }
  div {
      &:first-child {
          margin-right: 32px;
      }
  }
  @media(max-width: 1200px) {
    flex-direction: column;
    div {
        &:first-child {
            margin-right: 0;
            margin-bottom: 24px;
        }
    }
    .input-endereco {
        width: 100%;
    }
  }
`;

export const ContainerGlobal = styled.div`
    width: 100%;
    max-width: 1306px;
    margin: 0 auto;
    padding: 0px 15px;
    .geral {
        .left {
            display: ${props => props.expand_grid ? 'flex !important' : 'grid !important'};
            flex-wrap: wrap;
            justify-content: space-between;
            div${BoxWhite} {
                width:${props => props.expand_grid ? '48%' : 'auto;'};
                &:last-child {
                    width:${props => props.expand_grid ? '100%' : 'auto;'};
                    margin-top: ${props => props.expand_grid ? '30px' : '0'}
                }
            }
        }
    }
    @media(max-width: 480px) {
        .geral {
            .left {
                display: flex !important;
                flex-direction: column;
                align-items: flex-start;
            }
        }
    }
`;