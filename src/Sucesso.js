import React, { Component } from 'react';

import Sucesso from './pages/checkout/sucesso';

class Sucesso extends Component {
    render() { 
        return ( 
            <Sucesso from={this.props.location.pathname} />
        );
    }
}
 
export default Sucesso;