import React, { Component } from 'react'

import {Link} from 'react-router-dom'

import styled from 'styled-components'

import IconPin from '../../images/icon-pin.svg'

import { StyleInput} from '../../Style/global';

const Left = styled.div`
    width: 296px;
    ul {
        display: flex;
        flex-direction: column;
        li {
            position: relative;
            display: flex;
            align-items: center;
            padding: 37px;
            padding-right: 10px;
            height: 89px;
            background-color: #F6F6F6;
            border: 1px solid #DDDDDD;
            border-right: none;
            border-top: none;
            cursor: pointer;
            transition: all .3s;
            &:after {
                content: "";
                position: absolute;
                top: 0;
                right: -1px;
                width: 1px;
                height: 100%;
                background-color: #ffffff;
                opacity: 0;
                transition: all .3s;
            }
            &.selected {
                background-color: #ffffff;
                transition: all .3s;
                &:after {
                    opacity: 1;
                    transition: all .3s;
                }
                .circle {
                    &:before {
                        transform: scale(1);
                        transition: all .3s;
                    }
                }
                .info{
                    span{
                        font-weight: 600;
                        color: #363843;
                    }
                }
            }
            &:hover {
                background-color: #FFFFFF;
                transition: all .3s;
            }
            &:first-child {
                border-radius: 6px 0px 0px 0px;
                border-top: 1px solid #DDDDDD;
            }
            &:last-child {
                border-radius: 0px 0px 0px 6px;
                border-top: none;
            }
            .info {
                display: flex;
                align-items: center;
                img{
                    margin-right: 37px;
                    margin-right: 28px;
                }
                span {
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: normal;
                    font-size: 16px;
                    line-height: 20px;

                    color: #646981;
                }
                h5 {
                    font-size: 15px;
                    line-height: 24px;
                    color: #363843;
                }
                p {
                    margin: 0;
                    font-size: 13px;
                    line-height: 21px;
                    color: #A0A4B3;
                    strong {
                        font-size: 13px;
                        line-height: 21px;
                        color: #646981;
                    }
                }
            }
        }
    }
`;

const Right = styled.div`
    width: 762px;
    border-radius: 0px 6px 6px 6px;
    border: 1px solid #DDDDDD;
    background-color: #ffffff;
    padding: 39px 29px 41px 53px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    form{
        width: 100%;
    }
    .demais{
        padding: 0px 112px 18px 103px;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        h2{
            font: normal normal 16px/24px 'Inter';
            margin-bottom: 20px;
        }
        div${StyleInput}{
            width: 100%;
            margin-bottom: 26px;
            &:last-child{
                margin-bottom: 0;
            }
            select{
                width: 100%;
            }
        }
    }
`;

const RetirarLojaContainer = styled.section`
    padding-top: 71px;
    padding-bottom: 87px;
    background-color: #F6F6F8;
    .container{
        width: 1060px;
    }
    .title{
        .titulo-entrega{
            font: normal 600 18px/26px 'Inter';
            color: #363843;
            margin-bottom: 10px;
        }
        .items-entrega{
            li{
                font:normal normal 16px/28px 'Inter';
                color: #363843;
            }
            margin-bottom: 40px;
        }
        .escolha-loja{
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 40px;
            h2{
                font: normal 600 18px/26px 'Inter';
                color: #363843;
            }
            span{
                font:normal normal 18px/26px 'Inter';
                color: #6BB70B;
            }
        }
    }
    .pessoa-info{
        display: flex;
        align-items: center;
        justify-content: space-between;

        div${StyleInput}{
            &:first-child{
                margin-right: 58px;
            }
            height: 54px;
            width: 500px;
        }
    }
    .checkGroup{
        margin-top: 45px;
        margin-bottom: 25px;
        h4{
            font:normal normal 16px/22px 'Inter';
            color: #363843;
            margin-bottom: 16px;
        }
    }
    .checkbox-label {
        position: relative;
        margin-bottom: 15px;
        cursor: pointer;
        font-size: 22px;
        line-height: 24px;
        clear: both;
        padding-left: 37px;
        display: flex;
        align-items: center;
        font: normal normal 16px/19px 'Inter';
        color: #363843;
        height: 20px;
        margin-bottom: 15px;
    }

    .checkbox-label input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    .checkbox-label .checkbox-custom {
        position: absolute;
        top: 0px;
        left: 0px;
        height: 20px;
        width: 20px;
        background-color: #FFFFFF;
        border-radius: 5px;
        border: 1px solid #DCDDE3;
    }


    .checkbox-label input:checked ~ .checkbox-custom {
        background-color: #FFFFFF;
        border-radius: 5px;
        -webkit-transform: rotate(0deg) scale(1);
        -ms-transform: rotate(0deg) scale(1);
        transform: rotate(0deg) scale(1);
        opacity:1;
        border: 1px solid #0D71F0;
    }


    .checkbox-label .checkbox-custom::after {
        position: absolute;
        content: "";
        left: 12px;
        top: 12px;
        height: 0px;
        width: 0px;
        border-radius: 5px;
        border: solid #0D71F0;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(0deg) scale(0);
        -ms-transform: rotate(0deg) scale(0);
        transform: rotate(0deg) scale(0);
        opacity:1;
    }


    .checkbox-label input:checked ~ .checkbox-custom::after {
        -webkit-transform: rotate(45deg) scale(1);
        -ms-transform: rotate(45deg) scale(1);
        transform: rotate(45deg) scale(1);
        opacity:1;
        left: 5px;
        top: 1px;
        width: 6px;
        height: 10px;
        border: solid #0D71F0;
        border-width: 0 2px 2px 0;
        background-color: transparent;
        border-radius: 0;
    }

    .checkbox-label input:checked ~ .checkbox-custom::before {
        left: -3px;
        top: -3px;
        width: 24px;
        height: 24px;
        border-radius: 5px;
        -webkit-transform: scale(3);
        -ms-transform: scale(3);
        transform: scale(3);
        opacity:0;
        z-index: 999;
    }
`

const Opcoes = styled.div`
    display: flex;
    align-items: flex-start;
`

const LojaProxima = styled.label`
    cursor: pointer;
    width: 100%;
    padding-top: 30px;
    padding-left: 8px;
    padding-bottom: 34px;
    border-bottom: 1px solid #DCDDE3;
    display: flex;
    justify-content: space-between;
    &:first-child{
        padding-top: 0;
    }
    &:last-child{
        border-bottom: none;
        padding-bottom: 0;
    }
    input{
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    input:checked ~ .radio{
        opacity: 1;
    }
    input:checked ~ .radio .checkmark{
        background-color: #0D71F0;
    }
    .radio{
        display: flex;
        align-items: center;
        justify-content: center;
        .checkmark{
            height: 8.8px;
            width: 8.8px;
            border-radius: 9px;
            background-color: #ffffff;
        }
        width: 20px;
        border-radius: 20px;
        opacity: 0.4;
        border: 1px solid #A0A4B3;
        height: 19px;
        margin-right: 16px;
    }
    .text{
        width: 470px;
        .end{
            font:normal normal 16px/24px 'Inter';
            color: #363843;
            span{
                display: inline-block;
            }
            .rua{
                font:normal bold 16px/24px 'Inter';
                color: #363843;
            }
            .retire{
                font:normal 600 16px/24px 'Inter';
                color: #FFA82E;
            }
        }
    }
    .distancia{
        display: flex;
        justify-content: flex-end;
        justify-self: flex-end;
        width: 175px;
        span{
            font: normal normal 16px/24px 'Inter';
            text-align: right;
            color: #646981;
        }
    }
`


export default class RetirarLoja extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            typeRetirar: 'proximas',
            estado: null
        }
        this.entregaSelecionada = this.entregaSelecionada.bind(this)
    }
    entregaSelecionada(event) {
        const allStores = document.querySelectorAll('.entregas li');
        allStores.forEach(index => index.classList.remove('selected'));
        let loja = event.currentTarget;
        loja.classList.add('selected');
        this.setState({typeRetirar: loja.id})
    }
    render() {
        if(this.props.show){
            return(
                <RetirarLojaContainer>
                    <div className="container">
                        <div className="title">
                            <h2 className="titulo-entrega">Entrega 02 de 02 por Gazin</h2>

                            <ul className="items-entrega">
                                <li>01 IPhone 8 64GB Cinza Espacial Tela 4.7"</li>
                                <li>01 IPhone 8 64GB Cinza Espacial Tela 4.7"</li>
                            </ul>

                            <div className="escolha-loja">
                                <h2>
                                    Escolha a loja mais próxima a você para fazer a retirada
                                </h2>
                                <span>
                                    Frete Grátis
                                </span>
                            </div>
                        </div>
                        <Opcoes>
                            <Left>
                                <ul className="entregas">
                                    <li onClick={this.entregaSelecionada} id="proximas" className="selected">
                                        <div className="info">
                                            <img src={IconPin} alt=""/>
                                            <span>Lojas próximas</span>
                                        </div>
                                    </li>
                                    <li onClick={this.entregaSelecionada} id="demais" >
                                        <div className="info">
                                            <img src={IconPin} alt=""/>
                                            <span>Demais Locais</span>
                                        </div>
                                    </li>
                                </ul>
                            </Left>
                            <Right>
                                    {this.state.typeRetirar == 'proximas' ? 
                                        <form>
                                            <LojaProxima isProxima>
                                                <input type="radio" name="lojax" id="" />
                                                <div className="radio">
                                                    <div className="checkmark"></div>
                                                </div>
                                                <div className="text">
                                                    <span className="end">
                                                        <strong className="rua">Av. Pedro Manvailer, 2543 </strong>| 79002-365 Campo Grande <strong className="retire">Retire em 2 horas </strong >após a aprovação da compra. **
                                                    </span>
                                                </div>
                                                <div className="distancia">
                                                    <span>
                                                        0.95 Km
                                                    </span>
                                                </div>
                                            </LojaProxima>
                                            <LojaProxima>
                                                <input type="radio" name="lojax" id="" />
                                                <div className="radio">
                                                    <div className="checkmark"></div>
                                                </div>
                                                <div className="text">
                                                    <span className="end">
                                                        <strong className="rua">Av. Pedro Manvailer, 2543 </strong>| 79990-000 Campo Grande A partir de 2 dias úteis*
                                                    </span>
                                                </div>
                                                <div className="distancia">
                                                    <span>
                                                        1 Km
                                                    </span>
                                                </div>
                                            </LojaProxima>
                                            <LojaProxima>
                                                <input type="radio" name="lojax" id="" />
                                                <div className="radio">
                                                    <div className="checkmark"></div>
                                                </div>
                                                <div className="text">
                                                    <span className="end">
                                                        <strong className="rua">Rua Marechal Rondon, 1359 </strong>| 79990-000 Campo Grande A partir de 2 dias úteis*
                                                    </span>
                                                </div>
                                                <div className="distancia">
                                                    <span>
                                                        1.1 Km
                                                    </span>
                                                </div>
                                            </LojaProxima>
                                        </form>
                                        :
                                        <form>
                                            <div className="demais">
                                                <h2>Escolha abaixo a loja para retirada</h2>
                                                <StyleInput>
                                                    <label htmlFor="estado">Estado</label>
                                                    <select name="estado" onChange={(event)=>this.setState({estado: event.target.value})} required >
                                                        <option default selected disabled value="">Selecione: </option>
                                                        <option value="AC">Acre</option>
                                                        <option value="AM">Amazonas</option>
                                                        <option value="BA">Bahia</option>
                                                        <option value="GO">Goiás</option>
                                                        <option value="MT">Mato Grosso</option>
                                                        <option value="MS">Mato Grosso do Sul</option>
                                                        <option value="PA">Pará</option>
                                                        <option value="PR">Paraná</option>
                                                        <option value="RO">Rondônia</option>
                                                        <option value="TO">Tocantins</option>
                                                    </select>
                                                </StyleInput>
                                                <StyleInput>
                                                    <label htmlFor="cidade">Cidade</label>
                                                        {
                                                            this.state.estado == 'AC' ?
                                                            <select name="cidade" required >
                                                                <option>Acrelândia</option>
                                                                <option>Brasileia</option>
                                                                <option>Capixaba</option>
                                                                <option>Cruzeiro do Sul</option>
                                                                <option>Epitaciolândia</option>
                                                                <option>Feijó</option>
                                                                <option>Plácio de Castro</option>
                                                                <option>Rio Branco</option>
                                                                <option>Sena Madureira</option>
                                                                <option>Senador Guiomard</option>
                                                                <option>Tarauaca</option>
                                                            </select>
                                                            : 
                                                            this.state.estado == 'AM' ?
                                                                <select name="cidade" required >
                                                                    <option value="boca-do-acre">Boca do Acre</option>
                                                                    <option value="humaita">Humaitá</option>
                                                                    <option value="labrea">Labrea</option>
                                                                </select>
                                                            : 
                                                            <select>
                                                                <option default selected disabled value="">Primeiro selecione o estado</option>
                                                            </select>
                                                        }
                                                </StyleInput>
                                                <StyleInput>
                                                    <label htmlFor="bairro">Bairro</label>
                                                    <select>
                                                        <option default selected disabled value="">Primeiro selecione a cidade</option>
                                                    </select>
                                                </StyleInput>
                                                <StyleInput>
                                                    <label htmlFor="loja">Loja</label>
                                                    <select>
                                                        <option default selected disabled value="">Primeiro selecione o bairro</option>
                                                    </select>
                                                </StyleInput>
                                            </div>
                                        </form>
                                    }
                            </Right>
                        </Opcoes>
                        <form>
                            <div className="checkGroup">
                                <label class="checkbox-label checkPreferencias">
                                    <input name="outraPessoa" type="checkbox" />
                                    <span class="checkbox-custom rectangular"></span>
                                    Os produtos serão retirados por outra pessoa
                                </label>
                            </div>
                            <div className="pessoa-info">
                                <StyleInput>
                                    <label htmlFor="">Nome completo</label>
                                    <input type="text" name="nome" placeholder="Nome"/>
                                </StyleInput>
                                <StyleInput>
                                    <label htmlFor="">RG ou CNH</label>
                                    <input type="text" name="documento" placeholder=""/>
                                </StyleInput>
                            </div>
                        </form>
                    </div>
                </RetirarLojaContainer>
            )
        } 
        else return null
    }

}