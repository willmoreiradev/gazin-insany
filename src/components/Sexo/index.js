import React, { Component } from 'react';

import { Radio } from 'antd';
import 'antd/dist/antd.css';

class ComponentSexo extends Component {
  state = {
    value: 1,
  };
  onChange = e => {
    console.log('radio checked', e.target.value);
    this.setState({
      value: e.target.value,
    });
  };
  render() {
    return (
      <Radio.Group onChange={this.onChange} value={this.state.value}>
        <Radio value={1}>Feminino</Radio>
        <Radio value={2}>Masculino</Radio>
      </Radio.Group>
    );
  }
}
 
export default ComponentSexo;