import React, { Component } from 'react';
import styled from 'styled-components';
import Icone from '../fontawesome';

const Box = styled.div`
    width: 100%;
    max-width: 402px;
    height: 302px;
    border: 1px solid #DCDDE3;
    border-radius: 6px;
    padding: 32px;
    ul {
        display: flex;
        align-items: center;
        margin-bottom: 16px;
        li {
            margin-left: 10px;
            &:first-child {
                margin-left: 0px;
            }
            span {
                font-size: 20px;
                color: #FFA82E;
            }
        }
    }
    h3 {
        font: normal 600 18px/22px 'Inter';
        letter-spacing: -0.128571px;
        color: #363843;
        margin-bottom: 13px;
    }
    p {
        font-size: 15px;
        line-height: 24px;
        color: #646981;
        max-width: 328px;
    }
    @media(max-width: 480px) {
        height: auto;
    }
`;

const Cliente = styled.div`
    margin-top: 26px;
    span {
        font-size: 15px;
        line-height: 18px;
        color: #646981;
        strong {
            font: normal 500 15px/18px 'Inter';
            color: #363843;
        }
    }
`;

class BoxAvaliacao extends Component {
    render() {
        return (
            <Box>
                <ul>
                    <li>
                        <Icone nome_icone="star" />
                    </li>
                    <li>
                        <Icone nome_icone="star" />
                    </li>
                    <li>
                        <Icone nome_icone="star" />
                    </li>
                    <li>
                        <Icone nome_icone="star" />
                    </li>
                    <li>
                        <Icone nome_icone="star" />
                    </li>
                </ul>
                <h3>{this.props.titulo_avaliacao}</h3>
                <p>
                    {this.props.descricao_avaliacao}
                </p>
                <Cliente>
                    <span><strong>{this.props.nome_cliente}</strong> - Há {this.props.dia_avaliacao} dias</span>
                </Cliente>
            </Box>
        );
    }
}

export default BoxAvaliacao;