import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Icone from '../fontawesome'

import ImgBanner from '../../images/banner-promocao.jpg';
import LogoApple from '../../images/logo-apple.jpg';

const Banner = styled(Link)`
    border-radius: 3px;
    background: url(${ImgBanner}) no-repeat center center;
    background-size: cover;
`

const BoxLoja = styled.div`
    margin-top: 36px;
    margin-bottom: 43px;
    border: 1px solid #D8D8D8;
    border-radius: 6px;
    padding: 15px 40px 15px 25px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    .esq {
        display: flex;
        align-items: center;
        height: 139px;
        border-right: 1px solid #E3E3E3;
        padding-right: 90px;
        .info {
            strong {
                font-weight: 600;
                font-size: 18px;
                color: #363843;
                margin-bottom: 13px;
                display: block;
            }
            ul {
                display: flex;
                li {
                    margin-left: 12px;
                    margin-bottom: 18px;
                    &:first-child {
                        margin-left: 0px;
                    }
                    span {
                        font-size: 26px;
                        color: #FFA82E;
                    }
                }
            }
            span {
                font-size: 16px;
                line-height: 19px;
                color: #646981;
            }
        }
    }
    .dir {
        width: 617px;
        h3 {
            font: normal 600 18px/22px 'Inter';
            color: #363843;
            margin-bottom: 8px;
        }
        p {
            font-size: 14px;
            line-height: 20px;
            color: #646981;
            margin-bottom: 12px;
        }
        a {
            font-weight: 600;
            font-size: 14px;
            line-height: 17px;
            color: #515568;
        }
    }
    @media(max-width: 1200px) {
        margin-top: 26px;
        margin-bottom: 40px;
        padding: 20px 15px;
        flex-direction: column;
        align-items: flex-start;
        .esq {
            padding: 0;
            border-right: none;
            border-bottom: 1px solid #E3E3E3;
            padding-bottom: 20px;
            margin-bottom: 20px;
            width: 100%;
            justify-content: center;
        }
        .dir {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            p {
                text-align: center;
                font-size: 16px;
            }
        }
    }
    @media(max-width: 480px) {
        .esq {
            justify-content: flex-start;
            padding-bottom: 15px;
            margin-bottom: 15px;
            height: auto;
            .info {
                span {
                    font-size: 14px;
                }
                strong {
                    font-size: 14px;
                    line-height: 17px;
                    margin-bottom: 7px;
                }
                ul {
                    margin-bottom: 12px;
                    li {
                        margin-bottom: 0;
                        span {
                            font-size: 20px;
                        }
                    }
                }
            }
        }
        .dir {
            h3 {
                font-size: 16px;
            }
            p {
                display: none;
            }
        }
    }
`

const LogoLoja = styled.div`
    width: 169px;
    height: 103px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 75px;
    img {
        max-width: 100%;
    }
    @media(max-width: 1200px) {
        margin-right: 20px;
    }
    @media(max-width: 480px) {
        margin-right: 10px;
        width: 95px;
    }
`

class ComponentBannerPromocao extends Component {
    render() { 
        if(this.props.isLoja){
            return(
        <aside className="loja-apple">
            <div className="container">
                <BoxLoja>
                    <div className="esq">
                        <LogoLoja>
                            <img src={LogoApple} alt=""/>
                        </LogoLoja>
                        <div className="info">
                            <strong>Nota 4,9</strong>
                            <ul>
                                <li>
                                    <Icone nome_icone="star" />
                                </li>
                                <li>
                                    <Icone nome_icone="star" />
                                </li>
                                <li>
                                    <Icone nome_icone="star" />
                                </li>
                                <li>
                                    <Icone nome_icone="star" />
                                </li>
                                <li>
                                    <Icone nome_icone="star" />
                                </li>
                            </ul>
                            <span>(21) Avaliar produto</span>
                        </div>
                    </div>
                    <div className="dir">
                        <h3>Sobre a loja Apple Oficial</h3>
                        <p>A Apple compromete-se a criar e oferecer produtos e serviços de qualidade que melhoram a praticidade e promovem estilos de vida mais inteligentes para seus clientes…</p>
                        <a href="" target="_blank">Veja mais detalhes</a>
                    </div>
                </BoxLoja>
            </div>
        </aside>
            )
        }else{
            return ( 
                <aside className="banner-promocao">
                    <div className="container">
                        <Banner to="/detalhes-produto" className="area-img"></Banner>
                    </div>
                </aside> 
            );
        }
    }
}
 
export default ComponentBannerPromocao;