import React, { useState } from 'react';

import { SectionInfoPagamentoGeral, SectionNewsletterPromocoes } from './style';

//images

import IconCard from '../../images/icon-new-card.svg';
import IconDisconto from '../../images/icon-discount.svg';
import IconDelivery from '../../images/icon-home-delivery.svg';
import IconSeguranca from '../../images/icon-seguranca.svg';

export default function ComponentBoxesInfoCompra() {
    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    return ( 
        <>
            <SectionNewsletterPromocoes className="top-news">
                <div className="container">
                    <div className="texto">
                        <h2>Aproveite nossas promoções!</h2>
                        <p>Cadastre seu e-mail e receba ofertas exclusivas</p>
                    </div>
                    <form action="">
                        <input 
                            type="text" 
                            placeholder="Digite seu nome"
                            value={nome}
                            onChange={e => setNome(e.target.value)}
                        />
                        <input 
                            type="email" 
                            placeholder="Digite seu e-mail"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                        <button type="button">quero receber</button>
                    </form>
                </div>
            </SectionNewsletterPromocoes>
            <SectionInfoPagamentoGeral>
                <div className="container">
                    <div className="responsive">
                        <div className="card">
                            <img src={IconCard} alt="" className="icon"/>
                            <div className="txt">
                                <span>Pague em até</span>
                                <strong>12x sem juros</strong>
                            </div>
                        </div>
                        <div className="card">
                            <img src={IconDisconto} alt="" className="icon"/>
                            <div className="txt">
                                <strong>Receba 5% de desconto</strong>
                                <span>no boleto à vista</span>
                            </div>
                        </div>
                        <div className="card">
                            <img src={IconDelivery} alt="" className="icon"/>
                            <div className="txt">
                                <span>Entregamos em </span>
                                <strong>todo Brasil!</strong>
                            </div>
                        </div>
                        <div className="card">
                            <img src={IconSeguranca} alt="" className="icon"/>
                            <div className="txt">
                                <span>Compra 100% segura!</span>
                                <strong>54 anos de história</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </SectionInfoPagamentoGeral>
        </>
    );
}
 