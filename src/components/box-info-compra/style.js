import styled from 'styled-components';

export const SectionNewsletterPromocoes = styled.section`
  padding: 34px 0px;
  background: ${props => props.bg ? 'linear-gradient(85.36deg, #5C33FF 3.91%, #3399FF 98.18%)' : '#F6F6F6'};
  .container {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  .texto {
    h2 {
      font: bold 22px/27px 'Inter';
      color: ${props => props.bg ? '#fff' : '#0D71F0'};
      margin-bottom: 2px;
    }
    p {
      margin-bottom: 0px;
      font-weight: 500;
      font-size: 16px;
      line-height: 27px;
      color: ${props => props.bg ? '#fff' : '#646981'};
    }
  }
  form {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: space-between;
    max-width: 838px;
    input[type=text],
    input[type=email] {
      width: 308px;
      height: 54px;
      background-color: #FFFFFF;
      border: 1px solid #DCDDE3;
      box-sizing: border-box;
      border-radius: 6px;
      font-size: 14px;
      padding: 0px 24px;
      color: #363843;
      transition: all .3s;
      &:focus {
        border: 1px solid #0D71F0;
        transition: all .3s;
      }
      &::placeholder {
        color: #A0A4B3;
        transition: all .3s;
      }
    }
    button {
      border: 2px solid #FF9D2E;
      background-color: #FF9D2E;
      border-radius: 6px;
      width: 174px;
      height: 54px;
      color: #FFFFFF;
      letter-spacing: -0.114286px;
      font-weight: 600;
      font-size: 14px;
      text-transform: uppercase;
      transition: all .3s;
      &:hover {
        background-color: transparent;
        color: #FF9D2E;
        transition: all .3s;
      }
    }
  }
  @media(max-width: 480px) {
    padding: 20px 0px;
    &.top-news {
      display: none;
    }
    .container {
      flex-direction: column;
      align-items: center;
    }
    .texto {
      h2 {
        font-size: 18px;
        line-height: 27px;
        margin-bottom: 8px;
        text-align: center;
      }
      p {
        text-align: center;
        margin-bottom: 20px;
        font-size: 14px;
        line-height: 21px;
      }
    }
    form {
      flex-direction: column;
      width: 100%;
      input[type=text],
      input[type=email] {
        width: 100%;
        margin-bottom: 16px;
      }
      button {
        width: 100%;
      }
    }
  }
`;

export const SectionInfoPagamentoGeral = styled.section`
  padding-top: 40px;
  padding-bottom: 10px;
  .responsive {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  .card {
    width: 295px;
    height: 93px;
    border: 1px solid #0D71F0;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: center;
    .txt {
      margin-left: 10px;
      span {
        display: block;
        font-size: 16px;
        color: #363843;
        line-height: 1;
      }
      strong {
        font-size: 16px;
        color: #363843;
      }
    }
  }
  @media(max-width: 480px) {
    padding-top: 32px;
    padding-bottom: 0;
    .container {
      width: 100%;
      overflow-x: auto;
      padding-bottom: 20px;
    }
    .responsive {
      width: 641px;
    }
    .card {
      margin-right: 20px;
      width: 140px;
      height: 125px;
      flex-direction: column;
      align-items: flex-start;
      padding: 16px;
      img {
        max-width: 26.07px;
        margin-bottom: 13px;
      }
      .txt {
        margin-left: 0;
        span {
          font-size: 13px;
          line-height: 17px;
        }
        strong {
          font-size: 13px;
          line-height: 17px;
          display: inline-block;
        }
      }
    }
  }
`;