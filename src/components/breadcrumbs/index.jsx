import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Fontawesome from 'react-fontawesome';
import Styled from 'styled-components';

import IconeHome from '../../images/icone-home.svg';

const ResultadoBusca = Styled.h3`
    font-weight: 600;
    font-size: 24px;
    color: #363843;
`


class BreadCrumbs  extends Component {
    render() { 
        return ( 
            <section className="s-breadcrumbs">
                <div className="container">
                    <ul className="bread">
                        <li>
                            <Link to="/"><img src={IconeHome} alt=""/></Link>
                        </li>
                        <li>
                            <Link to="/">{this.props.origin}</Link>
                        </li>
                        {this.props.destination1 ? 
                            <li>
                                <span>{this.props.destination1}</span>
                            </li>
                        :
                            null
                        }

                    </ul>
                    {/* <ResultadoBusca>
                        Resultado de busca para "Iphone"
                    </ResultadoBusca> */}
                    {this.props.HaveSocial ?
                        <div className="social">
                            <span>Acompanhe nas redes sociais</span>
                            <ul>
                                <li>
                                    <a href="" target="_blank">
                                        <Fontawesome name="facebook" />
                                    </a>
                                </li>
                                <li>
                                    <a href="" target="_blank">
                                        <Fontawesome name="instagram" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    :
                        null
                    }

                </div>
            </section>
        );
    }
}
 
export default BreadCrumbs;