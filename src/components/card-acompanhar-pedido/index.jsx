import React, { Component } from 'react'
import styled from 'styled-components'
import LogoMaster from '../../images/logo-mastercard.svg'

const Step = styled.div`
    position: absolute;
    display: flex;
    align-items: center;
    padding-top: 36px;
    justify-content: center;
    flex-direction: column;
    top: -7px;
    font: normal ${props => props.active ? `600`: `normal`} 14px/20px 'Inter';
    text-align: center;
    span{
        color: #A0A4B3;
        font: normal 500 12px/20px 'Inter';
    }
    color: ${props => props.active ? '#6BB70B' : '#A0A4B3'};
    &:nth-child(1){
        left: 4%;
    }
    &:nth-child(2){
        left: 28%;
    }
    &:nth-child(3){
        left: 60%;
    }
    &:nth-child(4){
        left: 85%;
    }
    @media(max-width: 1200px) {
        position: relative;
        left: 0px !important;
        top: 0px !important;
        padding: 0px !important;
        align-items: flex-start;
        margin-bottom: 56px;
        &:last-child {
            margin-bottom: 0px;
        }
    }
`

const CardContainer = styled.div`
    margin-bottom: 15px;
    margin-left: 77px;
    width: 872px;
    height: fit-content;
    background: #FFFFFF;
    border-radius: 6px;
    padding: 28px 33px 20px 33px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    .status{
        position: relative;
        width: 782px;
        height: 13px;
        background: #EBEBF0;
        border-radius: 13px;
        .stepsIndicators{
            display: flex;
            position: relative;
            .indicator{
                position: absolute;
                top: -7px;
                &:nth-child(1){
                    left: 80px;
                }
                &:nth-child(2){
                    left: 291px;
                }
                &:nth-child(3){
                    left: 509px;
                }
            }
        }
        .steps{
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative;
        }
        &:before{
            content: '';
            position: absolute;
            width: 519px;
            height: 13px;
            background: linear-gradient(78.18deg, #5C33FF 3.91%, #3399FF 98.18%);
            border-radius: 13px;
        }
    }
    .top{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        width: 100%;
        padding: 0 10px;
        padding-bottom: 26px;
        border-bottom: ${props => props.show ? 'none' : '1px solid #EBEBF0'};
        .nPedido{
            font-family: 'Inter';
            font-style: normal;
            font-weight: normal;
            font-size: 16px;
            line-height: 22px;
            color: #A0A4B3;
            span{
                color: #363843
            }
        }
        .valorTotal{
            font-family: 'Inter';
            font-style: normal;
            font-weight: normal;
            font-size: 16px;
            line-height: 22px;
            color: #A0A4B3;
            span{
                font-family: 'Inter';
                font-style: normal;
                font-weight: 600;
                font-size: 16px;
                line-height: 141.52%;
                color: #363843
            }
        }
    }
    .produto{
        margin-top: ${props => props.show ? '100px' : '0'};
        padding-top: 6px;
        display: flex;
        align-items: center;
        justify-content: flex-start;
        margin-bottom: ${props => props.show ? '19px' : '0'};
        .info{
            font-family: 'Inter';
            font-style: normal;

            padding-left: 17px;
            display: flex;
            flex-direction: column;
            justify-content: center;

            .nome{

                font-weight: normal;
                font-size: 16px;
                line-height: 29px;
                color: #646981;
            }
            .unidades{
                font-weight: 600;
                font-size: 16px;
                line-height: 182.52%;
                color: #646981;
            }
            .vendido{
                font-weight: normal;
                font-size: 16px;
                line-height: 182.52%;
                color: #646981;

                strong{
                    font-weight: 600;
                    color: #646981;
                }
            }
        }
    }
    .showDetalhes{
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        button{
            background-color: transparent;
            cursor: pointer;
            display: flex;
            align-items: center;
            justify-content: center;
            font-family: 'Inter';
            font-style: normal;
            font-weight: normal;
            font-size: 15px;
            line-height: 24px;
            color: #0C64D3;
            svg{
                margin-left: 8px;
            }
        }
    }
    .entrega{
        padding: 0 10px;
        width: 100%;
        font-family: 'Inter';
        font-style: normal;
        padding-top: 22px;
        padding-bottom: 37px;
        border-top: 1px solid #EBEBF0;
        border-bottom: 1px solid #EBEBF0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
        h3{
            font-weight: 600;
            font-size: 18px;
            line-height: 170.02%;
            color: #363843;
            margin-bottom: 26px;
        }
        .nomeCliente{
            font-weight: 600;
            font-size: 16px;
            line-height: 170.02%;
            color: #646981
        }
        .endereco{
            width: 306px;
            font-weight: normal;
            font-size: 16px;
            line-height: 27px;
            color: #646981
        }
    }
    .pagamento{
        padding: 0 10px;
        width: 100%;
        font-family: 'Inter';
        font-style: normal;
        padding-top: 22px;
        padding-bottom: 37px;
        margin-bottom: 19px;
        border-bottom: 1px solid #EBEBF0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
        h3{
            font-weight: 600;
            font-size: 18px;
            line-height: 170.02%;
            color: #363843;
            margin-bottom: 26px;
        }
        .linha{
            width: 100%;
            flex-direction: row;
            display: flex;
            align-items: center;
            justify-content: space-between;
            font-weight: normal;
            font-size: 16px;
            line-height: 27px;
            color: #646981;
        }
        .total{
            margin-top: 24px;
            padding-top: 14px;
            padding-bottom: 14px;
            border-top: 1px solid #EBEBF0;
            border-bottom: 1px solid #EBEBF0;
        }
        .formaPagamento{
            padding-top: 37px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 100%;
            .info{
                .tipo{
                    display: flex;
                    flex-direction: column;
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: 600;
                    font-size: 16px;
                    line-height: 141.52%;
                    color: #A0A4B3;
                    strong{
                        font-weight: 600;
                        color: #646981;
                        margin-bottom: 13px;
                    }
                    .detalhes{
                        display: flex;
                        flex-direction: row;
                        img{
                            margin-right: 16px
                        }
                        span{
                            display: flex;
                            flex-direction: column;
                            font-family: 'Inter';
                            font-style: normal;
                            font-weight: normal;
                            font-size: 16px;
                            line-height: 23px;
                            color: #A0A4B3;
                            strong{
                                font-weight: 600;
                                color: #A0A4B3;
                            }
                        }
                    }
                }
            }
            button{
                width: 199px;
                height: 54px;
                border-radius: 6px;
                border: 1px solid #6BB70B;
                box-sizing: border-box;
                border-radius: 6px;
                background-color: transparent;
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 17px;
                color: #6BB70B;
                background-color: transparent;
                transition: all .3s;
                &:hover{
                    color: #ffffff;
                    background-color: #6BB70B;
                    transition: all .3s;
                }
            }
        }
    }
    @media(max-width: 1200px) {
        width: 100%;
        margin: 20px 0px;
        .top {
            padding: 0;
            flex-direction: column;
            align-items: center;
            padding-bottom: 40px;
            .nPedido {
                margin-bottom: 10px;
            }
        }
        .status {
            width: 100%;
            height: 345px;
            background-color: transparent;
            display: flex;
            align-items: flex-start;
            &:before {
                width: 11px;
                display: inline-block;
                height: 215px;
                z-index: 1;
            }
            .stepsIndicators {
                width: 11px;
                background-color: #EBEBF0;
                height: 100%;
                border-radius: 5px;
                overflow: hidden;
                .indicator {
                    top: 0;
                    z-index: 1;
                    &:first-child {
                        left: 3px;
                    }
                    &:nth-child(2) {
                        left: 3px;
                        top: 95px;
                    }
                    &:nth-child(3) {
                        left: 3px;
                        top: 190px;
                    }
                }
            }
            .steps {
                flex-direction: column;
                width: 100%;
                height: 100%;
                align-items: flex-start;
                justify-content: flex-start;
                padding-left: 40px;
            }
        }
        .produto {
            flex-direction: column;
            width: 100%;
            margin-top: 40px;
            .imagem {
                margin-bottom: 20px;
            }
            .info {
                padding: 0;
                text-align: center;
                .nome {
                    font-size: 14px;
                    line-height: 22px;
                }
                .unidades {
                    font-size: 14px;
                    line-height: 22px;
                }
                .vendido {
                    font-size: 14px;
                    line-height: 22px;
                    margin-bottom: 20px;
                    strong {
                        font-size: 14px;
                        line-height: 22px;
                        display: block;
                    }
                }
            }
        }
        .entrega {
            .endereco {
                width: 100%;
                font-size: 14px;
                line-height: 22px;
            }
        }
        .pagamento {
            .formaPagamento {
                flex-direction: column;
                align-items: flex-start;
                button {
                    width: 100%;
                }
                .info {
                    width: 100%;
                    margin-bottom: 20px;
                    .tipo {
                        strong {
                            margin: 0;
                        }
                        .detalhes {
                            margin-top: 10px;
                        }
                    }
                }
            }
        }
    }
`

export default class CardAcompanharPedido extends Component {
    state = {
        showDetalhes: false
    }
    render() {
        return (
            <CardContainer
                show={this.state.showDetalhes}
            >
                <div className="top">
                    <div className="nPedido">
                        Pedido: <span>{this.props.nPedido}</span>
                    </div>
                    <div className="valorTotal">
                        Valor Total: <span>R$ {((this.props.valor * this.props.unidades) + this.props.precoFrete).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                    </div>
                </div>
                {
                    this.state.showDetalhes ?
                        <div className="status">
                            <div className="stepsIndicators">
                                <div className="indicator">
                                    <svg width="5" height="5" viewBox="0 0 5 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="2.5" cy="2.5" r="2.5" fill="#EBEBF0"/>
                                    </svg>

                                </div>
                                <div className="indicator">
                                    <svg width="5" height="5" viewBox="0 0 5 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="2.5" cy="2.5" r="2.5" fill="#EBEBF0"/>
                                    </svg>

                                </div>
                                <div className="indicator">
                                    <svg width="5" height="5" viewBox="0 0 5 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="2.5" cy="2.5" r="2.5" fill="#EBEBF0"/>
                                    </svg>

                                </div>
                            </div>
                            <div className="steps">
                                <Step>
                                    Pedido efetuado
                                    <span className="time">
                                        22/09/19 00:26
                                    </span>
                                </Step>
                                <Step>
                                    Pagamento autorizado
                                    <span className="time">
                                        22/09/19 00:26
                                    </span>
                                </Step>
                                <Step active>
                                    Em transporte
                                    <span className="time">
                                        22/09/19 00:26
                                    </span>
                                </Step>
                                <Step>
                                    Produto entregue
                                    <span className="time">
                                        22/09/19 00:26
                                    </span>
                                </Step>
                            </div>
                        </div>
                    :
                    null
                }
                <div className="produto">
                    <div className="imagem">
                        <img src={this.props.imagem} alt="Imagem do Produto"/>
                    </div>
                    <div className="info">
                        <span className="nome">{this.props.nome}</span>
                        <span className="unidades">{this.props.unidades == 1 ? `1 unidade` : `${this.props.unidades} unidades`} - R$ {this.props.valor.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                        <span className="vendido">Vendido e entregue por: <strong>{this.props.vendedor}</strong></span>
                    </div>
                </div>

                {
                    this.state.showDetalhes ?
                        <div className="entrega">
                            <h3>Endereço de entrega: </h3>
                            <span className="nomeCliente">{this.props.nomeCliente}</span>
                            <span className="endereco">{this.props.endereco}</span>
                        </div>
                    : null
                }
                
                {
                    this.state.showDetalhes ?
                        <div className="pagamento">
                            <h3>Pagamento</h3>
                            <div className="linha">
                                <span className="titulo">Subtotal</span>
                                <span className="valor">R$ {(this.props.valor * this.props.unidades).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                            </div>
                            <div className="linha">
                                <span className="titulo">Frete</span>
                                <span className="valor">R$ {(this.props.precoFrete).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                            </div>
                            <div className="linha">
                                <span className="titulo">Descontos</span>
                                <span className="valor">R$ {(this.props.descontos).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                            </div>
                            <div className="linha">
                                <span className="titulo">Juros</span>
                                <span className="valor">R$ {(this.props.juros).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                            </div>
                            <div className="linha total">
                                <span className="titulo">Total do pedido:</span>
                                <span className="valor">R$ {(this.props.valor * this.props.unidades + this.props.precoFrete).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>
                            </div>
                            <div className="formaPagamento">
                                <div className="info">
                                    <div className="tipo">
                                        <span>Pagamento</span>
                                        <strong>Cartão de Crédito</strong>
                                        <div className="detalhes">
                                            <img src={LogoMaster} alt="" />
                                            <span>
                                                1234########
                                                <strong>
                                                    R$ 8.013,00 em 12x
                                                </strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <button>
                                    Visualizar nota fiscal
                                </button>
                            </div>
                        </div>
                    : null
                }

                <div className="showDetalhes">
                    <button onClick={()=>this.setState({showDetalhes: !this.state.showDetalhes})}>
                        {this.state.showDetalhes ? 'Fechar detalhes' :'Mostrar detalhes'}
                        {this.state.showDetalhes ? 
                            <svg width="9" height="6" viewBox="0 0 9 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M9 6L4.5 -3.93402e-07L0 6L9 6Z" fill="#0C64D3"/>
                            </svg>
                            : 
                            <svg width="9" height="6" viewBox="0 0 9 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M0 0L4.5 6L9 0L0 0Z" fill="#0C64D3"/>
                            </svg> 
                        }
                        
                    </button>
                </div>
            </CardContainer>
        )
    }
}
