import React, { Component } from 'react';
import { ContQtd } from './style';

class ComponentQuantidade extends Component {
    state = { 
        qtd: 1   
    }
    handleIncrement = () => {
        this.setState({ qtd: this.state.qtd + 1 })
    }
    handleDecrement = () => {
        this.state.qtd == 1 ? this.setState({ qtd: 1 }) : this.setState({ qtd: this.state.qtd - 1 })
    }
    render() { 
        return ( 
            <ContQtd>
                <button onClick={this.handleDecrement}>-</button>
                <span>{this.state.qtd}</span>
                <button onClick={this.handleIncrement}>+</button>
            </ContQtd>
        );
    }
}
 
export default ComponentQuantidade;