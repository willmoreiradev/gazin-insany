import React, { Component } from 'react';

import { ProdutosDoCarrinho} from './style';

import ProdutoListado from './produto-listado';

class ItensCarrinho extends Component {
    render() { 
        return ( 
            <ProdutosDoCarrinho>
                <div className="topo">
                    <div className="left">
                        <span>Produto</span>
                    </div>
                    <div className="right">
                        <ul>
                            <li><span>Qtd.</span></li>
                            <li><span>Entrega</span></li>
                            <li><span>Preço</span></li>
                        </ul>
                    </div>
                </div>
                <div className="all-prods">
                    <ProdutoListado/>
                    <ProdutoListado/>
                </div>
            </ProdutosDoCarrinho>
        );
    }
}
 
export default ItensCarrinho;