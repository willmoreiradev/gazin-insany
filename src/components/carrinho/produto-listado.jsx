import React, { Component } from 'react';
import { ProdutoCarrinho, ValorProdutoListado } from './style';
import ComponentQuantidade from '../carrinho/contador-qtd';

import FotoProduto from '../../images/foto-produto-fornecedor.jpg';
import IconeLixeira from '../../images/icone-lixeira.svg';

class ProdutoListado extends Component {
    render() { 
        return ( 
            <ProdutoCarrinho>
                <div className="left">
                    <div className="foto">
                        <img src={FotoProduto} alt=""/>
                    </div>
                    <div className="info">
                        <h4>iPhone 8 64GB Cinza Espacial Tela 4.7"</h4>
                        <span className="frete-mb">Frete grátis</span>
                    </div>
                </div>
                <div className="right">
                    <ComponentQuantidade/>
                    <span className="frete">Frete grátis</span>
                    <ValorProdutoListado>R$ 3.999,00</ValorProdutoListado>
                    <button className="btn-lixeira">
                        <img src={IconeLixeira} alt=""/>
                    </button>
                </div>
            </ProdutoCarrinho>
        );
    }
}
 
export default ProdutoListado;