import styled from 'styled-components';

export const ProdutosDoCarrinho = styled.div`
    border: 1px solid #DDDDDD;
    border-radius: 6px;
    width: 100%;
    margin-bottom: 29px;
    .topo {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding-right: 153px;
        padding-left: 41px;
        height: 73px;
        border-bottom: 1px solid #DDDDDD;
        span {
            font-weight: 500;
            font-size: 16px;
            line-height: 19px;
            color: #363843;
        }
        ul {
            display: flex;
            align-items: center;
            li {
                margin-left: 99px;
                &:first-child {
                    margin-left: 0px;
                }
            }
        }
    }
    .all-prods {
        padding: 0px 32px;
    }
    @media(max-width: 560px) {
        .topo {
            display: none;
        }
        .all-prods {
            padding: 0;
        }
    }
`;

export const ProdutoCarrinho = styled.div`
    border-bottom: 1px solid #DDDDDD;
    display: flex;
    align-items: center;
    padding: 25px 0px;
    &:last-child {
        border-bottom: none;
    }
    .left {
        display: flex;
        align-items: center;
        width: 100%;
        max-width: 280px;
        margin-right: 18px;
        .foto {
            width: 85px;
            height: 89.32px;
            display: flex;
            align-items: center;
            justify-content: center;
            box-shadow: 0px 1.15254px 2.59322px rgba(54, 56, 67, 0.11);
            img {
                max-width: 75%;
            }
        }
        .info {
            flex: 1;
            margin-left: 21px;
            h4 {
                margin-bottom: 0px;
                color: #646981;
                font-size: 15px;
            }
            .frete-mb {
                display: none;
                font-size: 12px;
                line-height: 15px;
                color: #6BB70B;
            }
        }
    }
    .right {
        flex: 1;
        display: flex;
        align-items: center;
        justify-content: space-between;
        .frete {
            font-size: 12px;
            line-height: 15px;
            color: #6BB70B;
        }
    }
    @media(max-width: 480px) {
        flex-direction: column;
        align-items: flex-start;
        border-bottom: 2px solid #DDDDDD;
        padding: 25px 24px;
        .left {
            margin-right: 0;
            width: 100%;
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
            padding-bottom: 22px;
            margin-bottom: 22px;
            max-width: 100%;
            .info {
                .frete-mb {
                    display: block;
                    margin-top: 5px;
                }
            }
        }
        .right {
            width: 100%;
            align-items: center;
            .frete {
                display: none;
            }
            .btn-lixeira {
                order: 3;
            }
        }
    }
`;

export const ContQtd = styled.div`
    width: 90px;
    height: 48px;
    border: 1px solid #D8D8D8;
    border-radius: 6px;
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    align-items: center;
    button {
        background-color: transparent;
        color: #646981;
        font-weight: bold;
    }
    span {
        display: block;
        text-align: center;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        letter-spacing: -0.1px;
        color: #646981;
    }
    @media(max-width: 480px) {
        order: 2;
    }
`;

export const ValorProdutoListado = styled.h4`
    font-weight: normal;
    font-size: 15px;
    line-height: 18px;
    color: #646981;
    margin-bottom: 0px;
    @media(max-width: 480px) {
        order: 1;
    }
`;