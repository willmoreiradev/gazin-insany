import React, { Component } from 'react';

import {Link} from 'react-router-dom'

import Produto from './prod-cart';

import Bag from '../../images/bag-cart.svg';

//Images

import IconeCarrinho from '../../images/icone-carrinho.svg';

class ComponentCart extends Component {
    activeCart = (e) => {
        const dropCadastro = document.querySelector('header .area-user');
        let botao = e.currentTarget;
        botao.parentElement.classList.toggle('active');
        dropCadastro.classList.remove('active');
    }
    render() { 
        return ( 
            <div className="area-cart">
                <button className="btn-show-cart" type="button" onClick={this.activeCart}>
                    <img src={IconeCarrinho} alt=""/>
                    <span>2</span>
                </button>
                <div className="dropdown-cart">
                    <div className="empty-cart" id="js-cart-empty">
                        <img src={Bag} alt=""/>
                        <h3>Carrinho vazio</h3>
                    </div>
                    <div className="cart-full">
                        <div className="all-prods">
                            <Produto 
                                nome='iPhone 8 64GB Cinza Espacial Tela 4.7"' 
                                qtd='02'
                                valor='3.999,00'
                            />
                            <Produto 
                                nome='iPhone 8 64GB Cinza Espacial Tela 4.7"' 
                                qtd='02'
                                valor='3.999,00'
                            />
                        </div>
                        <div className="total">
                            <span>Valor total</span>   
                            <h3>R$ 3.999,00</h3>
                        </div>
                        <Link to="/carrinho" className="btn-continuar">continuar</Link>
                    </div>
                </div>
            </div>
        );
    }
}
 
export default ComponentCart;