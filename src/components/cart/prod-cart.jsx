import React, { Component } from 'react';

import FotoProduto from '../../images/foto-produto.jpg';
import IconeLixeira from '../../images/icone-lixeira.svg';

class ProdutoCarrinho extends Component {
    ExcluirProduto = (e) => {
        const qtdProduto = document.querySelectorAll('.dropdown-cart .all-prods .item');
        const contCartFull = document.querySelector('.dropdown-cart .cart-full');
        const carrinhoVazio = document.getElementById('js-cart-empty');
        let botao = e.currentTarget;
        let produto = botao.parentElement.parentElement;
        produto.remove();
        if(qtdProduto.length - 1 === 0) {
            carrinhoVazio.style.display = 'block';
            contCartFull.style.display = 'none';
        }
    }
    render() { 
        return ( 
            <div className="item">
                <div className="photo">
                    <img src={FotoProduto} alt=""/>
                </div>
                <div className="info">
                    <h3>{this.props.nome}</h3>
                    <div className="valor-qtd">
                        <span>Quant.: {this.props.qtd}</span>
                        <h4>R$ {this.props.valor}</h4>
                    </div>
                    <button type="button" onClick={this.ExcluirProduto}>
                        <img src={IconeLixeira} alt=""/>
                    </button>
                </div>
            </div>
        );
    }
}
 
export default ProdutoCarrinho;