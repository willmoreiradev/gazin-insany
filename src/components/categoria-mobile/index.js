import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import { ButtonCategoria, StyleSidebar, SidebarMain, SidebarCategorias, SidebarSub } from './style';

import IconeCategoria from '../../images/menu_bar.svg';
import ArrowRightGray from '../../images/arrow-right-gray.svg';
import ArrowRightBlue from '../../images/arrow-right-blue-mb.svg';
import ArrowBack from '../../images/arrow-back-white.svg';
import IconePedidos from '../../images/icon-pedidos-mobile.svg';
import IconeCartao from '../../images/icon-new-card.svg';
import IconeDesconto from '../../images/icon-discount.svg';
import IconeDelivery from '../../images/icon-home-delivery.svg';
import IconPhone from '../../images/icon_phone_blue.svg';

class ComponentCategoriaMobile extends Component {
    state = { 
        show_categorie: false,
        show_menu_main: false,
        show_submenu: false,
        show_menu_categorias: false,
        name_categoria: ''
    }
    componentDidMount = () => {
        const Categorias = document.querySelectorAll('.js-next-sidebar');
        
        Categorias.forEach(cat => {
            cat.addEventListener('click', e => {
                e.preventDefault();
                this.setState({ name_categoria : e.currentTarget.textContent });
                this.setState({ show_menu_main : false });
                this.setState({ show_menu_categorias : false });
                this.setState({ show_submenu : true });
            })
        })
    }
    render() { 
        return ( 
            <div>
                <ButtonCategoria onClick={() => {
                    this.setState({ show_categorie : true });
                    this.setState({ show_menu_main : true });
                }}>
                    <img src={IconeCategoria} />
                </ButtonCategoria>
                <StyleSidebar visibleCategories={this.state.show_categorie}>
                    <SidebarMain visibleMainSidebar={this.state.show_menu_main}>
                        <div className="topo">
                            <button onClick={() => {
                                this.setState({ show_categorie : false });
                                this.setState({ show_menu_main : false });
                            }}>
                                <img src={ArrowBack} alt=""/>
                                Categorias Gazin
                            </button>
                        </div>
                        <Link className="btn-pedidos" to="/conta/pedidos">
                            <img src={IconePedidos} alt=""/>
                            <span>Meus pedidos</span>
                        </Link>
                        <div className="cont-menu">
                            <ul>
                                <li><button className="js-next-sidebar">Compre direto da China <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Celulares e telefone fixa <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Som Automotivo <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Brinquedos e bebês <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Informática e tablets <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Tv, áudio e home theater <img src={ArrowRightGray} alt=""/></button></li>
                                <li>
                                    <button 
                                        className="btn-ver-todos"
                                        onClick={() => {
                                            this.setState({ show_menu_main : false });
                                            this.setState({ show_menu_categorias : true });
                                        }}
                                    >
                                        Ver todos os departamentos 
                                        <img src={ArrowRightBlue} alt=""/>
                                    </button>
                                </li>
                            </ul>
                            <div className="info-site">
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeCartao} alt=""/>
                                    </div>
                                    <p>Pague em até <strong>12x sem juros</strong></p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeDesconto} alt=""/>
                                    </div>
                                    <p><strong>Receba 5% de desconto</strong> no boleto à vista</p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeDelivery} alt=""/>
                                    </div>
                                    <p>Entregamos em <strong>todo Brasil!</strong></p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconPhone} alt=""/>
                                    </div>
                                    <p>Atendimento <strong>(44) 3046-2303</strong><br/> segunda à sexta das 08:30h às 18h</p>
                                </div>
                            </div>
                        </div>
                    </SidebarMain>

                    <SidebarCategorias visibleCategoriaSidebar={this.state.show_menu_categorias}>
                        <div className="topo">
                            <button onClick={() => {
                                this.setState({ show_menu_categorias : false });
                                this.setState({ show_menu_main : true });
                            }}>
                                <img src={ArrowBack} alt=""/>
                                Categorias Gazin
                            </button>
                        </div>
                        <Link className="btn-pedidos" to="/conta/pedidos">
                            <img src={IconePedidos} alt=""/>
                            <span>Meus pedidos</span>
                        </Link>
                        <div className="cont-menu">
                            <ul>
                                <li><button className="js-next-sidebar">Compre direto da China <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Celulares e telefone fixa <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Som Automotivo <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Brinquedos e bebês <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Informática e tablets <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Tv, áudio e home theater <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Eletrodomésticos e split <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Eletroportáteis <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Móveis e decoração <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Moda, beleza e perfumaria <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Games, livros e filmes <img src={ArrowRightGray} alt=""/></button></li>
                                <li><button className="js-next-sidebar">Para sua empresa <img src={ArrowRightGray} alt=""/></button></li>
                            </ul>
                            <div className="info-site">
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeCartao} alt=""/>
                                    </div>
                                    <p>Pague em até <strong>12x sem juros</strong></p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeDesconto} alt=""/>
                                    </div>
                                    <p><strong>Receba 5% de desconto</strong> no boleto à vista</p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeDelivery} alt=""/>
                                    </div>
                                    <p>Entregamos em <strong>todo Brasil!</strong></p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconPhone} alt=""/>
                                    </div>
                                    <p>Atendimento <strong>(44) 3046-2303</strong><br/> segunda à sexta das 08:30h às 18h</p>
                                </div>
                            </div>
                        </div>
                    </SidebarCategorias>
                    
                    <SidebarSub visibleSubSidebar={this.state.show_submenu}>
                        <div className="topo">
                            <button onClick={() => {
                                this.setState({ show_submenu : false });
                                this.setState({ show_menu_main : true });
                            }}>
                                <img src={ArrowBack} alt=""/>
                                {this.state.name_categoria}
                            </button>
                        </div>
                        <Link className="btn-pedidos" to="/conta/pedidos">
                            <img src={IconePedidos} alt=""/>
                            <span>Meus pedidos</span>
                        </Link>
                        <div className="cont-menu">
                            <ul>
                                <li><a href="/departamentos">Compre direto da China</a></li>
                                <li><a href="/departamentos">Celulares e telefone fixa</a></li>
                                <li><a href="/departamentos">Som Automotivo</a></li>
                                <li><a href="/departamentos">Brinquedos e bebês</a></li>
                                <li><a href="/departamentos">Informática e tablets</a></li>
                                <li><a href="/departamentos">Tv, áudio e home theater</a></li>
                                <li><a href="/departamentos">Eletrodomésticos e split</a></li>
                                <li><a href="/departamentos">Eletroportáteis</a></li>
                                <li><a href="/departamentos">Móveis e decoração</a></li>
                                <li><a href="/departamentos">Moda, beleza e perfumaria</a></li>
                                <li><a href="/departamentos">Games, livros e filmes</a></li>
                                <li><a href="/departamentos">Para sua empresa</a></li>
                            </ul>
                            <div className="info-site">
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeCartao} alt=""/>
                                    </div>
                                    <p>Pague em até <strong>12x sem juros</strong></p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeDesconto} alt=""/>
                                    </div>
                                    <p><strong>Receba 5% de desconto</strong> no boleto à vista</p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconeDelivery} alt=""/>
                                    </div>
                                    <p>Entregamos em <strong>todo Brasil!</strong></p>
                                </div>
                                <div className="item">
                                    <div className="icon">
                                        <img src={IconPhone} alt=""/>
                                    </div>
                                    <p>Atendimento <strong>(44) 3046-2303</strong><br/> segunda à sexta das 08:30h às 18h</p>
                                </div>
                            </div>
                        </div>
                    </SidebarSub>
                </StyleSidebar>
            </div>
        );
    }
}
 
export default ComponentCategoriaMobile;