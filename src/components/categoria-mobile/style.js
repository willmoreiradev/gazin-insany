import styled from 'styled-components';

export const ButtonCategoria = styled.button`
    position: absolute;
    bottom: initial;
    left: 20px;
    top: 21px;
    width: 25px;
    height: 25px;
    align-items: center;
    justify-content: center;
    display: none;
    z-index: 4;
    @media(max-width: 1200px) {
        display: flex;
    }
`;

export const StyleSidebar = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 50;
    opacity: ${props => props.visibleCategories ? '1' : '0'};
    pointer-events: ${props => props.visibleCategories ? 'all' : 'none'};
    transition: all .3s;
`;

export const SidebarMain = styled.div`
    background-color: #ffffff;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transform: ${props => props.visibleMainSidebar ? 'translateX(0%)' : 'translateX(-100%)'};
    transition: all .4s;
    .cont-menu {
        overflow: auto;
        height: calc(100vh - 98px);
        padding-bottom: 40px;
    }
    .btn-pedidos {
        display: flex;
        align-items: center;
        width: 100%;
        height: 48px;
        background-color: #F6F6F8;
        padding: 0px 20px;
        img {
            margin-right: 13px;
        }
        span {
            font-size: 14px;
            line-height: 17px;
            color: #363843;
        }
    }
    .topo {
        background-color: #0D71F0;
        width: 100%;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0px 20px;
        button {
            display: flex;
            align-items: center;
            font-weight: 600;
            font-size: 16px;
            line-height: 22px;
            color: #FFFFFF;
            img {
                margin-right: 25px;
            }
        }
    }
    ul {
        padding: 0;
        li {
            button {
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
                line-height: 50px;
                border-bottom: 1px solid #DCDDE3;
                font-size: 14px;
                color: #646981;
                padding: 0px 20px;
            }
            .btn-ver-todos {
                width: 100%;
                display: flex;
                align-items: center;
                justify-content: space-between;
                padding: 0px 20px;
                height: 50px;
                letter-spacing: -0.114286px;
                color: #0D71F0;
                font-size: 14px;
            }
        }
    }
    .info-site {
        padding: 35px 20px;
        .item {
            display: flex;
            align-items: flex-start;
            margin-bottom: 30px;
            &:last-child {
                margin-bottom: 0px;
            }
            .icon {
                width: 24px;
                img {
                    max-width: 100%;
                }
            }
            p {
                flex: 1;
                margin-left: 20px;
                font-size: 14px;
                line-height: 141.52%;
                color: #646981;
                strong {

                    font-size: 14px;
                    color: #646981;
                }
            }
        }
    }
`;

export const SidebarCategorias = styled.div`
    background-color: #ffffff;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    transform: ${props => props.visibleCategoriaSidebar ? 'translateX(0%)' : 'translateX(-100%)'};
    transition: all .4s;
    .cont-menu {
        overflow: auto;
        height: calc(100vh - 98px);
        padding-bottom: 40px;
    }
    .btn-pedidos {
        display: flex;
        align-items: center;
        width: 100%;
        height: 48px;
        background-color: #F6F6F8;
        padding: 0px 20px;
        img {
            margin-right: 13px;
        }
        span {
            font-size: 14px;
            line-height: 17px;
            color: #363843;
        }
    }
    .topo {
        background-color: #0D71F0;
        width: 100%;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0px 20px;
        button {
            display: flex;
            align-items: center;
            font-weight: 600;
            font-size: 16px;
            line-height: 22px;
            color: #FFFFFF;
            img {
                margin-right: 25px;
            }
        }
    }
    ul {
        padding: 0;
        li {
            button {
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
                line-height: 50px;
                border-bottom: 1px solid #DCDDE3;
                font-size: 14px;
                color: #646981;
                padding: 0px 20px;
            }
        }
    }
    .info-site {
        padding: 35px 20px;
        .item {
            display: flex;
            align-items: flex-start;
            margin-bottom: 30px;
            &:last-child {
                margin-bottom: 0px;
            }
            .icon {
                width: 24px;
                img {
                    max-width: 100%;
                }
            }
            p {
                flex: 1;
                margin-left: 20px;
                font-size: 14px;
                line-height: 141.52%;
                color: #646981;
                strong {
                    
                    font-size: 14px;
                    color: #646981;
                }
            }
        }
    }
`;

export const SidebarSub = styled.div`
    background-color: #ffffff;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    transform: ${props => props.visibleSubSidebar ? 'translateX(0%)' : 'translateX(-100%)'};
    transition: all .4s;
    .cont-menu {
        overflow: auto;
        height: calc(100vh - 98px);
        padding-bottom: 40px;
    }
    .btn-pedidos {
        display: flex;
        align-items: center;
        width: 100%;
        height: 48px;
        background-color: #F6F6F8;
        padding: 0px 20px;
        img {
            margin-right: 13px;
        }
        span {
            font-size: 14px;
            line-height: 17px;
            color: #363843;
        }
    }
    .topo {
        background-color: #0D71F0;
        width: 100%;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0px 20px;
        button {
            display: flex;
            align-items: center;
            font-weight: 600;
            font-size: 16px;
            line-height: 22px;
            color: #FFFFFF;
            img {
                margin-right: 25px;
            }
        }
    }
    ul {
        padding: 0;
        li {
            a {
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
                line-height: 50px;
                border-bottom: 1px solid #DCDDE3;
                font-size: 14px;
                color: #646981;
                padding: 0px 20px;
            }
        }
    }
    .info-site {
        padding: 35px 20px;
        .item {
            display: flex;
            align-items: flex-start;
            margin-bottom: 30px;
            &:last-child {
                margin-bottom: 0px;
            }
            .icon {
                width: 24px;
                img {
                    max-width: 100%;
                }
            }
            p {
                flex: 1;
                margin-left: 20px;
                font-size: 14px;
                line-height: 141.52%;
                color: #646981;
                strong {
                    
                    font-size: 14px;
                    color: #646981;
                }
            }
        }
    }
`;

