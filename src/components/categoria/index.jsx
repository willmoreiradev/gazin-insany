import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import IconeMenu from '../../images/icone-bar.svg';
import IconeEstrela from '../../images/star.svg';
import ArrowItemCategoria from '../../images/arrow-item-categoria.svg';
import BannerCategoria from '../../images/banner-categoria.jpg';

class Cont extends Component {
    render() {
        return (
            <div className="cont-categoria">
                <div className="info">
                    <h2>{this.props.title}</h2>
                    <div className="all">
                        <div className="item">
                            <ul>
                                <li><h3>Últimos lançamentos</h3></li>
                                <li><a href="">Moto E6 Plus novo</a></li>
                                <li><a href="">Galaxy Note10 e 10+ </a></li>
                                <li><a href="">Motorola one action</a></li>
                                <li><a href="">Galaxy A80</a></li>
                                <li><a href="">Galaxy M</a></li>
                                <li><a href="">Huawei P30</a></li>
                                <li><a href="">Iphone xr</a></li>
                                <li><a href="">Motorola one vision </a></li>
                                <li><a href="">Lg K12+ </a></li>
                                <li><a href="">Galaxy A 2019</a></li>
                                <li><a href="">Galaxy s10</a></li>
                                <li><a href="">Moto g7</a></li>
                            </ul>
                        </div>
                        <div className="item">
                            <ul>
                                <li><h3>Top modelos</h3></li>
                                <li><a href="">Iphone 8</a></li>
                                <li><a href="">Galaxy J</a></li>
                                <li><a href="">Galaxy s9</a></li>
                                <li><a href="">Lg k</a></li>
                            </ul>
                            <ul>
                                <li><h3>Acessórios</h3></li>
                                <li><a href="">Apple watch 4</a></li>
                                <li><a href="">Acessórios para celular</a></li>
                                <li><a href="">Wearables</a></li>
                                <li><a href="">Smartwatch</a></li>
                                <li><a href="">Capa</a></li>
                                <li><a href="">Película</a></li>
                            </ul>
                        </div>
                        <div className="item">
                            <ul>
                                <li><h3>Veja também</h3></li>
                                <li><a href="">Smartphones usados</a></li>
                                <li><a href="">Celular básico</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="banner-produto">
                    <img src={BannerCategoria} alt=""/>
                </div>
            </div>
        )
    }
}

class ComponentCategoria extends Component {
    state = {
        cont: <Cont title='Direto da China' />
    }
    showCat = (title) => {
        this.setState({cont: <Cont title={title} />})
    }
    activeDropdownCategoria = () => {
        const dropdown = document.querySelector('.dropdown-categoria');
        dropdown.classList.add('active');
    }
    closeDropdownCategoria = () => {
        const dropdown = document.querySelector('.dropdown-categoria');
        dropdown.classList.remove('active');
    }
    render() {
        return (
            <section className="s-categoria">
                <div className="container">
                    <button type='button' className="btn" onMouseOver={this.activeDropdownCategoria}>
                        <img src={IconeMenu} alt="" />
                        <span>Todas categorias</span>
                    </button>
                    <nav>
                        <ul>
                            <li><Link to="/departamentos">Móveis</Link></li>
                            <li><Link to="/departamentos">Eletrodomésticos</Link></li>
                            <li><Link to="/departamentos">Eletrônicos</Link></li>
                            <li><Link to="/departamentos">Portáteis</Link></li>
                            <li><Link to="/departamentos">Celular</Link></li>
                            <li><Link to="/departamentos">Informática</Link></li>
                            <li>
                                <a href="">
                                    <img src={IconeEstrela} alt="" />
                                    Promoções imperdíveis
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div className="dropdown-categoria" onMouseLeave={this.closeDropdownCategoria}>
                        <div className="esq">
                            <ul className="tab">
                                <li onMouseOver={() => this.showCat('Direto da China')}>
                                    <button type='button'>
                                        Compre direto da China
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Celulares')}>
                                    <button type='button'>
                                        Celulares e telefonia fixa
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Sutomotivo')}>
                                    <button type='button'>
                                        Sutomotivo
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Brinquedos')}>
                                    <button type='button'>
                                        Brinquedos e bebês
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Informática')}>
                                    <button type='button'>
                                        Informática e tablets
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Tv e Áudio')}>
                                    <button type='button'>
                                        Tv, áudio e home theater
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Eletrodomésticos')}>
                                    <button type='button'>
                                        Eletrodomésticos e split
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Eletroportáteis')}>
                                    <button type='button'>
                                        Eletroportáteis
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Móveis e Decoração')}>
                                    <button type='button'>
                                        Móveis e decoração
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Moda, Beleza e Perfumaria')}>
                                    <button type='button'>
                                        Moda, beleza e perfumaria
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Games, Livros e Filmes')}>
                                    <button type='button'>
                                        Games, livros e filmes
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                                <li onMouseOver={() => this.showCat('Para sua Empresa')}>
                                    <button type='button'>
                                        Para sua empresa
                                        <img src={ArrowItemCategoria} alt="" />
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div className="dir">
                            {this.state.cont}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ComponentCategoria;