import React, { Component } from 'react';
import InputMask from "react-input-mask";
import styled from 'styled-components';

const Content = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const AreaInput = styled.div`
    display: flex;
    align-items: center;
    border: 1px solid #DCDDE3;
    box-sizing: border-box;
    border-radius: 6px;
    width: 235px;
    height: 48px;
    padding: 0px 10px;
    input[type=text] {
        flex: 1 1 auto;
        padding-right: 20px;
        font-size: 14px;
        line-height: 17px;
        letter-spacing: -0.1px;
        color: #A0A4B3;
    }
    button {
        font-weight: 600;
        font-size: 14px;
        line-height: 17px;
        letter-spacing: -0.1px;
        text-transform: uppercase;
        color: #2A89FF;
        background-color: transparent;
    }
    @media(max-width: 480px) {
        width: 100%;
        margin-right: 0;
    }
`;

const BotaoBuscarCep = styled.button`
    font-size: 14px;
    line-height: 17px;
    color: #646981;
    background-color: transparent;
    text-align: left;
    transition: all .3s;
    display: ${props => props.visualizar ? "block" : "none"};

    &:hover {
        color: #2A89FF;
        transition: all .3s;
    }
`;

class ComponentBuscaCep extends Component {
    render() { 
        return ( 
            <Content>
                <AreaInput className="input">
                    <InputMask 
                        type="text"
                        mask="99999-999"
                        maskPlaceholder=""
                        placeholder="Digite aqui o seu CEP"
                    />
                    <button type="button">ok</button>
                </AreaInput>
                <BotaoBuscarCep visualizar={this.props.mostrar_botao}>Não sei meu CEP</BotaoBuscarCep>
            </Content>
        );
    }
}

ComponentBuscaCep.defaultProps = {
    visualizar: 'block'
}
 
export default ComponentBuscaCep;