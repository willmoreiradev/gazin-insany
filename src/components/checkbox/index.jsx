import React, { Component } from 'react';

class Checkbox extends Component {
    checked = (e) => {
        let check = e.currentTarget.parentElement.parentElement;
        let allChecks = check.querySelectorAll('.check');
        
        allChecks.forEach(item => {
            item.classList.remove('active');
        })

        e.currentTarget.classList.add('active');
    }
    render() { 
        return ( 
                <div className={`check ${this.props.status}`} onClick={this.checked}>
                    <div className="square"></div>
                    {this.props.label}
                </div>
        );
    }
}
 
export default Checkbox;