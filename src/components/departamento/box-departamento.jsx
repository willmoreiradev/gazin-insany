import React, { Component } from 'react';

import { Link } from 'react-router-dom';

class BoxDepartamento extends Component {
    render() { 
        return ( 
            <Link to="./departamentos" className="box-departamento">
                <div className="foto">
                    <img src={this.props.img_departamento} alt=""/>
                </div>
                <span>{this.props.nome_departamento}</span>
            </Link>
        );
    }
}
 
export default BoxDepartamento;