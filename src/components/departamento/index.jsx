import React, { Component } from 'react';

import Title from '../title-section';

import SlideDepartamentos from '../slides/departamentos';

class CoponentAreaDepartamento extends Component {
    render() { 
        return ( 
            <section className="s-departamento">
                <div className="container">
                    <Title 
                        titulo="Compre por departamento com os melhores descontos" 
                        subtitulo="Conheça os departamentos mais acessadas na Gazin"
                    />
                    <SlideDepartamentos/>
                </div>
            </section>
        );
    }
}
 
export default CoponentAreaDepartamento;