import React, { Component } from 'react';

import Visa from '../../images/bandeiras/visa.svg';
import MasterCard from '../../images/bandeiras/mastercard.svg';
import HiperCard from '../../images/bandeiras/hipercard.svg';
import Diners from '../../images/bandeiras/diners-club.svg';
import Hiper from '../../images/bandeiras/hiper.svg';
import Boleto from '../../images/bandeiras/boleto.svg';

import Google from '../../images/google.svg';
import CompraSegura from '../../images/compra-segura-new.svg';
import ReclameAqui from '../../images/reclame-aqui.svg';
import IconPhone from '../../images/icone-phone.svg';


class AreaCompraFooter extends Component {
    render() {
        return (
            <div className="area-compra">
                <div className="cont-geral">
                    <div className="esq">
                        <div className="item">
                            <div className="telefone-contato">
                                <img src={IconPhone} alt=""/>
                                <div className="telefone">
                                    <div className="topo">
                                        <h2>Atendimento Gazin</h2>
                                    </div>
                                    <div className="info">
                                        <a href="#">(44) 3046-2303</a>
                                        <p>de Seg à Sex das 8:30 às 18:00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <span>Formas de pagamentos</span>
                            <ul>
                                <li>
                                    <img src={Visa} alt="" />
                                </li>
                                <li>
                                    <img src={MasterCard} alt="" />
                                </li>
                                <li>
                                    <img src={HiperCard} alt="" />
                                </li>
                                <li>
                                    <img src={Diners} alt="" />
                                </li>
                                <li>
                                    <img src={Hiper} alt="" />
                                </li>
                                <li>
                                    <img src={Boleto} alt="" />
                                </li>
                            </ul>
                        </div>
                        <div className="item">
                            <ul>
                                <li>
                                    <img src={CompraSegura} alt="" />
                                </li>
                                <li>
                                    <img src={ReclameAqui} alt="" />
                                </li>
                            </ul>
                        </div>
                        <div className="item">
                            <span>Avaliação dos consumidores</span>
                            <ul>
                                <li>
                                    <img src={Google} alt="" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AreaCompraFooter;