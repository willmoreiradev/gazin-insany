import React, { Component } from 'react';
import { FacebookProvider, Page } from 'react-facebook';

import IconePhone from '../../images/icone-phone.svg';

class AreaPrimariaFooter extends Component {
    render() {
        return (
            <div className="area-primaria">
                <div className="esq">
                    <div className="atendimento">
                        <img src={IconePhone} alt=""/>
                        <div className="info">
                            <span>Atendimento</span>
                            <a href="">(44) 3046-2303</a>
                            <span>segunda à sexta das 08:30h às 18h</span>
                        </div>
                    </div>
                    <div className="facebook">
                        <FacebookProvider appId="394160471533518">
                            <Page href="https://www.facebook.com/grupogazin/" width="407" height="132" tabs="" />
                        </FacebookProvider>
                    </div>

                </div>
                <nav>
                    <div className="item">
                        <h3>Institucional</h3>
                        <ul>
                            <li><a href="">Institucional</a></li>
                            <li><a href="">A Empresa</a></li>
                            <li><a href="">Nossas Lojas</a></li>
                            <li><a href="">Trabalhe Conosco</a></li>
                            <li><a href="">Mapa do Site</a></li>
                            <li><a href="">Glossário</a></li>
                            <li><a href="">Contato</a></li>
                        </ul>
                    </div>
                    <div className="item">
                        <h3>Dúvidas</h3>
                        <ul>
                            <li><a href="">Dúvidas</a></li>
                            <li><a href="">Como Comprar</a></li>
                            <li><a href="">Esqueci Minha Senha</a></li>
                            <li><a href="">Acompanhar Pedido</a></li>
                            <li><a href="">Reimpressão de Boleto</a></li>
                            <li><a href="">Vale-presente e Cupons</a></li>
                            <li><a href="">Segurança</a></li>
                        </ul>
                    </div>
                    <div className="item">
                        <ul>
                            <li><a href="">Forma Pagamento</a></li>
                            <li><a href="">Troca e Devolução</a></li>
                            <li><a href="">Prazo de Entrega</a></li>
                            <li><a href="">Entrega</a></li>
                            <li><a href="">Privacidade</a></li>
                            <li><a href="">Dúvidas Frequentes</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default AreaPrimariaFooter;