import React, { Component } from 'react';

import Visa from '../../images/visa.jpg';
import MasterCard from '../../images/mastercard.jpg';
import HiperCard from '../../images/hipercard.jpg';
import Diners from '../../images/diners.jpg';
import Hiper from '../../images/hiper.jpg';
import Boleto from '../../images/boleto.jpg';
import CompraSegura from '../../images/compra-segura-new.svg';
import ReclameAqui from '../../images/reclame-aqui.svg';
import Google from '../../images/google.svg';
import Ecode from '../../images/e-code.svg';
import Insany from '../../images/insany.svg';

class AreaSecundariaFooter extends Component {
    render() {
        return (
            <div className="area-secundaria">
                <div className="cont-geral">
                    <div className="esq">
                        <div className="item">
                            <span>Formas de pagamentos</span>
                            <ul>
                                <li>
                                    <img src={Visa} alt="" />
                                </li>
                                <li>
                                    <img src={MasterCard} alt="" />
                                </li>
                                <li>
                                    <img src={HiperCard} alt="" />
                                </li>
                                <li>
                                    <img src={Diners} alt="" />
                                </li>
                                <li>
                                    <img src={Hiper} alt="" />
                                </li>
                                <li>
                                    <img src={Boleto} alt="" />
                                </li>
                            </ul>
                        </div>
                        <div className="item">
                            <ul>
                                <li>
                                    <img src={CompraSegura} alt="" />
                                </li>
                                <li>
                                    <img src={ReclameAqui} alt=""/>
                                </li>
                            </ul>
                        </div>
                        <div className="item">
                            <span>Avaliação dos consumidores</span>
                            <ul>
                                <li>
                                    <img src={Google} alt="" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="dev">
                        <ul>
                            <li>
                                <a href="https://www.agenciaecode.com.br" target="_blank">
                                    <img src={Ecode} alt="" />
                                </a>
                            </li>
                            <li>
                                <a href="https://insanydesign.com/" target="_blank">
                                    <img src={Insany} alt="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <p>
                    Preços e condições de pagamento exclusivos para compras via Internet. Vendas sujeitas à analise e confirmação de dados. Fica garantida a empresa a eventual retificação das ofertas e erros de digitação que possam ter sido veiculados, podendo ser estornado a compra.  77.941.490/0225-58 - Gazin Industria e Comercio De Móveis e Eletrodomesticos LTDA - ROD PR 082 SN - Centro - CEP: 87485000 - Douradina-PR
                </p>
            </div>
        );
    }
}

export default AreaSecundariaFooter;