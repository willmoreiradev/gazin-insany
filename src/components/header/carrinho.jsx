import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import styled from 'styled-components'

import Logo from '../../images/logo.svg';

import Cart from '../../images/cart.svg';
import Identificacao from '../../images/identificacao.svg';
import Obrigado from '../../images/obrigado.svg';
import Pagamento from '../../images/pagamento.svg';
import Entre from '../../images/entre.svg';
import Seguro from "../../images/seguro.svg";


const Header = styled.header`
    position: relative;
    top: 0;
    left: 0;
    width: 100%;
    height: 80px;
    z-index: 20;
    @include gradient(#5C33FF, #3399FF);
    display: flex;
    align-items: center;
    .container {
        display: flex;
        align-items: center;
    }
    @media(max-width: 1200px) {
        position: fixed;
        height: 137px;
        .logo {
            position: absolute;
            left: 15px;
            top: 25px;
        }
    } 
`
const Steps = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin-left: 102px;
    margin-right: 159px;
    @media(max-width: 1200px) {
        margin: 0;
        position: absolute;
        bottom: 26px;
        left: 50%;
        margin-left: -198.5px;
    }
    @media(max-width: 480px) {
        width: 100%;
        left: 0;
        margin: 0;
        justify-content: space-between;
        padding: 0px 15px;
    }

`

const Step = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    opacity: ${props => props.active ? "1" : "0.5"};
    img{
        margin-right: 14px;
    }
    span{     
        font-family: 'Inter';
        font-style: normal;
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        letter-spacing: -0.1px;
        color: #ffffff;
    }
    @media(max-width: 1200px) {
        span {
            display: none;
        }
    }
`

const EntreStep = styled.div`
    opacity: ${props => props.active ? "1" : "0.5"};
    padding: 0 42px;
    @media(max-width: 480px) {
        padding: 0 20px;
    }
`

const AmbienteSeguro = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    img{
        margin-right: 15px;
    }
    p{
        color: #ffffff;
        width: 65px;
    }
    @media(max-width: 1200px) {
        position: absolute;
        right: 15px;
        top: 23px;
        p {
            width: auto;
        }
    }
    @media(max-width: 480px) {
        p {
            font-size: 13px;
            strong {
                font-size: 13px;
            }
        }
    }
`

class HeaderCarrinho extends Component {
    render() { 
        return ( 
            <Header>
                <div className="container">
                    <Link to="/" className="logo">
                        <img src={Logo} alt=""/>
                    </Link>
                    <Steps>
                        <Step active={true}>
                            <img src={Cart} alt="" />
                            <span>
                                Meu carrinho
                            </span>
                        </Step>
                        <EntreStep active={true}>
                            <img src={Entre} alt="" />
                        </EntreStep>
                        <Step active={this.props.identificacao}>
                            <img src={Identificacao} alt="" />
                            <span>
                                Identificação
                            </span>
                        </Step>
                        <EntreStep active={this.props.identificacao}>
                            <img src={Entre} alt="" />
                        </EntreStep>
                        <Step active={this.props.pagamento}>
                            <img src={Pagamento} alt="" />
                            <span>
                                Pagamento
                            </span>
                        </Step>
                        <EntreStep active={this.props.obrigado}>
                            <img src={Entre} alt="" />
                        </EntreStep >
                        <Step active={this.props.obrigado}>
                            <img src={Obrigado} alt="" />
                            <span>
                                Obrigado !
                            </span>
                        </Step>
                    </Steps>
                    <AmbienteSeguro>
                        <img src={Seguro} alt="" />
                        <p>
                            Ambiente <br />
                            <strong>
                                Seguro
                            </strong>!
                        </p>
                    </AmbienteSeguro>
                </div>
            </Header>
        );
    }
}
 
export default HeaderCarrinho;