import React, { Component } from 'react';

import styled from 'styled-components'

import ComponentSearch from '../search/index';
import ComponentCart from '../cart/index'; 

import { Link } from 'react-router-dom';

import { StyleMenuMobile, StyleNewHeader } from './style';
import CategoriaMobile from '../categoria-mobile';

//Images

import Logo from '../../images/logo.svg';
import NewLogo from '../../images/new-logo.svg';
import IconeUser from '../../images/icone-user.svg'
import IconeHeart from '../../images/heart.svg';
import Cart from '../../images/cart.svg';
import Identificacao from '../../images/identificacao.svg';
import Obrigado from '../../images/obrigado.svg';
import Erro from '../../images/icon-header-erro.svg';
import Pagamento from '../../images/pagamento.svg';
import Entre from '../../images/entre.svg';
import Seguro from "../../images/seguro.svg";
import CloseIcon from '../../images/close.svg';
import Facebook from '../../images/facebook.svg';
import IconUser from '../../images/icon/user.svg';
import ImageTheo from '../../images/theo.png';
import IconNewPhone from '../../images/icon-new-phone.svg';
import IconNewPacote from '../../images/icon-new-pacote.svg';
import IconCookies from '../../images/icon-cookies.svg';


const HeaderC = styled.header`
    position: relative;
    top: 0;
    left: 0;
    width: 100%;
    height: 80px;
    z-index: 20;
    @include gradient(#5C33FF, #3399FF);
    display: flex;
    align-items: center;
    .container {
        display: flex;
        align-items: center;
    }
    @media(max-width: 1200px) {
        position: fixed;
        height: 137px;
        .logo {
            position: absolute;
            left: 15px;
            top: 25px;
        }
    } 
`

const Steps = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin-left: 102px;
    margin-right: 159px;
    @media(max-width: 1200px) {
        margin: 0;
        position: absolute;
        bottom: 26px;
        left: 50%;
        margin-left: -198.5px;
    }
    @media(max-width: 480px) {
        width: 100%;
        left: 0;
        margin: 0;
        justify-content: space-between;
        padding: 0px 15px;
    }
`

const Step = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    opacity: ${props => props.active ? "1" : "0.5"};
    img{
        margin-right: 14px;
    }
    span{     
        font-family: 'Inter';
        font-style: normal;
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        letter-spacing: -0.1px;
        color: #ffffff;
    }
    @media(max-width: 1200px) {
        span {
            display: none;
        }
    }
`

const EntreStep = styled.div`
    opacity: ${props => props.active ? "1" : "0.5"};
    padding: 0 42px;
    @media(max-width: 480px) {
        padding: 0 20px;
    }
`

const AmbienteSeguro = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    img{
        margin-right: 15px;
    }
    p{
        color: #ffffff;
        width: 65px;
    }
    @media(max-width: 1200px) {
        position: absolute;
        right: 15px;
        top: 23px;
        p {
            width: auto;
        }
    }
    @media(max-width: 480px) {
        p {
            font-size: 13px;
            strong {
                font-size: 13px;
            }
        }
    }
`

const CookiesWarning = styled.div`
    display: ${props => props.active ? "flex" : "none"};
    opacity: ${props => props.active ? "1" : "0"};
    pointer-events: ${props => props.active ? "all" : "none"};
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 75px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #ffffff;
    box-shadow: 4px 0px 10px rgba(11, 87, 183, 0.17);
    z-index: 100;
    .texto {
        display: flex;
        align-items: center;
    }
    .icon{
        display: flex;
        justify-content: center;
        align-items: center;
        width: 44px;
        height: 43.5px;
        margin-right: 26px;
    }
    p{
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 23px;
        color: #646981;
        margin-right: 81px;
        margin-bottom: 0px;
    }
    .btns{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        button{
            display: flex;
            justify-content: center;
            align-items: center;
            width: 149px;
            height: 43px;
            border-radius: 6px;
            &.fechar{
                font-style: normal;
                font-weight: 500;
                font-size: 14px;
                line-height: 17px;
                text-align: center;
                letter-spacing: -0.114286px;
                text-transform: uppercase;
                color: #646981;
            }
            &.entendi{
                font-style: normal;
                font-weight: 600;
                font-size: 14px;
                line-height: 17px;
                text-align: center;
                letter-spacing: -0.114286px;
                text-transform: uppercase;
                background: #0C64D3;
                color: #FFFFFF;
            }
        }
    }
    @media (max-width: 900px) {
        height: auto;
        flex-direction: column;
        padding: 20px;
        align-items: flex-start;
        .texto {
            align-items: flex-start;
            margin-bottom: 20px;
        }
        .icon{
            margin-bottom: 0;
            margin-right: 10px;
        }
        p{
            margin: 0;
            flex: 1;
            font-size: 14px;
            line-height: 22px;
        }
        .btns {
            width: 100%;
            justify-content: space-between;
        }
        
    }
`

class Header extends Component {
    state = {
        sidebar : false,
        cookies: true
    }
    activeDropdownUser = (e) => {
        const dropCarrinho = document.querySelector('header .area-cart');
        let botao = e.currentTarget;
        botao.parentElement.classList.toggle('active');
        dropCarrinho.classList.remove('active');
    }
    activeMenu = () => {
        document.documentElement.classList.toggle('menu-opened');
    }
    toggleCookies = () =>{
        this.setState({cookies: !this.state.cookies})
    }
    render() { 
        if(this.props.from == "/carrinho"){
            return ( 
                <HeaderC>
                    <div className="container">
                        <Link to="/" className="logo">
                            <img src={Logo} alt=""/>
                        </Link>
                        <Steps>
                            <Step active={true}>
                                <img src={Cart} alt="" />
                                <span>
                                    Meu carrinho
                                </span>
                            </Step>
                            <EntreStep active={true}>
                                <img src={Entre} alt="" />
                            </EntreStep>
                            <Step active={this.props.identificacao}>
                                <img src={Identificacao} alt="" />
                                <span>
                                    Identificação
                                </span>
                            </Step>
                            <EntreStep active={this.props.pagamento}>
                                <img src={Entre} alt="" />
                            </EntreStep>
                            <Step active={this.props.pagamento}>
                                <img src={Pagamento} alt="" />
                                <span>
                                    Pagamento
                                </span>
                            </Step>
                            <EntreStep active={this.props.obrigado}>
                                <img src={Entre} alt="" />
                            </EntreStep >
                            {this.props.obrigado && !this.props.erro 
                            ?
                                <Step active={this.props.obrigado}>
                                    <img src={Obrigado} alt="" />
                                    <span>
                                        Obrigado !
                                    </span>
                                </Step>
                            :
                                <Step active={this.props.obrigado}>
                                    <img src={Erro} alt="" />
                                    <span>
                                        Erro!
                                    </span>
                                </Step>
                            }
                        
                        </Steps>
                        <AmbienteSeguro>
                            <img src={Seguro} alt="" />
                            <p>
                                Ambiente <br />
                                <strong>
                                    Seguro
                                </strong>!
                            </p>
                        </AmbienteSeguro>
                    </div>
                </HeaderC>
            );
        }else{
            return ( 
                <>
                <StyleNewHeader>
                    <CategoriaMobile/>
                    <StyleMenuMobile sidebar_open={this.state.sidebar}>
                        <div className="cont">
                            <button type="button" className="close" onClick={() => {
                                this.setState({ sidebar : false })
                            }}>Menu <img src={CloseIcon}/></button>
                            <div className="item-menu">
                                <div className="title">
                                    <img src={IconeUser}/>
                                    <h2>Minha conta</h2>
                                </div>
                                <ul>
                                    <li><Link to="/conta/dados">Meus dados</Link></li>
                                    <li><Link to="/conta/alterar/email">Alterar meu e-mail</Link></li>
                                    <li><Link to="/conta/alterar/senha">Alterar senha</Link></li>
                                </ul>
                            </div>
                            <div className="item-menu">
                                <div className="title">
                                    <img src={IconUser}/>
                                    <h2>Meus pedidos</h2>
                                </div>
                                <ul>
                                    <li><Link to="/conta/pedidos">Acompanhar pedidos</Link></li>
                                    <li><Link to="/conta/segunda-via">Imprimir 2ª via do boleto</Link></li>
                                </ul>
                            </div>
                            <div className="social">
                                <span>Acompanhe nas redes sociais</span>
                                <ul>
                                    <li><a href="https://www.facebook.com/grupogazin" target="_blank"><img src={Facebook} alt=""/></a></li>
                                </ul>
                            </div>
                        </div>
                    </StyleMenuMobile>
                    <div className="container">
                        <img src={ImageTheo} className="theo" alt=""/>
                        <div className="right">
                            <div className="top">
                                <p className="welcome">Olá! <strong>Sou o Téo da Gazin, seja bem-vindo!</strong></p>
                                <ul>
                                    <li>
                                        <span>Atendimento</span>
                                        <div className="phone">
                                            <p><img src={IconNewPhone} alt=""/> <a href="#">(44) 3046-2303</a> de Seg à Sex das 8h30 às 18h</p>
                                        </div>
                                    </li>
                                    <li>
                                        <Link to="/conta/pedidos">
                                            <img src={IconNewPacote} alt=""/>
                                            <span>Meus pedidos</span>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                            <div className="geral">
                                <Link to="/" className="logo">
                                    <img src={NewLogo} alt=""/>
                                </Link>
                                <ComponentSearch/>
                                <div className="area-user">
                                    <button className="btn-user" onClick={this.activeDropdownUser}>
                                        <img src={IconeUser} alt=""/>
                                        <div className="info-user">
                                            <span>Olá! 😄</span>
                                            <p>Entre ou cadastre-se</p>
                                        </div>
                                    </button>
                                    <div className="dropdown-cadastro">
                                        <Link 
                                            to={{
                                                pathname: "/login",
                                                state: {
                                                    from: this.props.from
                                                }
                                            }}
                                            className="btn-entrar" 
                                        >
                                            entrar
                                        </Link>
                                        <Link to="/cadastro" className="btn-cadastro">Cliente novo? Cadastrar</Link>
                                        <ul>
                                            <li><Link to="/conta">Minha conta</Link></li>
                                            <li><Link to="/conta/pedidos">Meus pedidos</Link></li>
                                        </ul>
                                    </div>
                                </div>
                                <Link to="/favoritos" className="favoritos">
                                    <img src={IconeHeart} alt=""/>
                                    <span className="alert">1</span>
                                </Link>
                                <ComponentCart/>
                            </div>
                        </div>
                    </div>
                </StyleNewHeader>

                <CookiesWarning active={this.state.cookies}>
                    <div className="texto">
                        <div className="icon">
                            <img src={IconCookies} alt=""/>
                        </div>
                        <p>
                            Os cookies nos permitem oferecer nossos serviços. Ao utilizá-los, você aceita o uso que fazemos dos cookies.
                        </p>
                    </div>
                    <div className="btns">
                        <button className="fechar" onClick={()=>this.toggleCookies()}>
                            Fechar
                        </button>
                        <button className="entendi" onClick={()=>this.toggleCookies()}>
                            Entendi
                        </button>
                    </div>
                </CookiesWarning>
            </>
            )
        }
    }
}
 
export default Header;