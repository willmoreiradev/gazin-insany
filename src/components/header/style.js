import styled from 'styled-components';


export const StyleMenuMobile = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    width: 474px;
    height: 100vh;
    background-color: #ffffff;
    z-index: 10;
    padding-left: 74px;
    padding-top: 33px;
    box-shadow: 0 0 30px 0 rgba(0,0,0,0.2);
    transform: ${ props => props.sidebar_open ? 'translateX(0%)' : 'translateX(100%)' };
    transition: all .3s;
    .close {
        display: flex;
        align-items: center;
        background-color: transparent;
        font-size: 15px;
        color: #2A89FF;
        margin-bottom: 68px;
        img {
            margin-left: 19px;
        }
    }
    .item-menu {
        margin-bottom: 67px;
        .title {
            display: flex;
            align-items: center;
            margin-bottom: 31px;
            img {
                margin-right: 15px;
            }
            h2 {
                font-size: 18px;
                color: #363843;
                font-weight: 600;
            }
        }
        ul {
            li {
                margin-bottom: 25px;
                &:last-child {
                    margin-bottom: 0px;
                }
                a {
                    font-size: 16px;
                    line-height: 17px;
                    color: #646981;
                    transition: all .3s;
                    &:hover {
                        color: #0C64D3;
                        transition: all .3s;
                    }
                }
            }
        }
    }
    .social {
        span {
            font-size: 14px;
            line-height: 17px;
            color: #515568;
            display: block;
            margin-bottom: 23px;
        }
        ul {
            display: flex;
            align-items: center;
            li {
                margin-left: 9px;
                &:first-child {
                    margin-left: 0px;
                }
                a {
                    width: 42px;
                    height: 42px;
                    background-color: #E8EDF4;
                    border-radius: 6px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }
            }
        }
    }
    @media(max-width: 1100px) {
        width: 100%;
        padding: 30px;
        .close {
            margin-bottom: 40px;
        }
        .item-menu {
            .title {
                margin-bottom: 20px;
            }
            ul {
                li {
                    margin-bottom: 20px;
                    a {
                        font-size: 14px;
                    }
                }
            }
        }
    }
`;

export const StyleNewHeader = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 114px;
    z-index: 1001;
    display: flex;
    align-items: center;
    background: linear-gradient(87.38deg, #5C33FF -4.51%, #3399FF 104.58%);
    p,ul {
        margin-bottom: 0px;
    }
    .theo {
        margin-left: -24px;
    }
    .container {
        display: flex;
        align-items: center;
        justify-content: space-between;
        position: relative;
    }
    .right {
        flex: 1;
        .top {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 14px;
            .welcome {
                font-size: 15px;
                line-height: 18px;
                color: #FFFFFF;
                strong {
                    font-size: 15px;
                    line-height: 18px;
                    color: #FFFFFF;
                }
            }
            ul {
                display: flex;
                align-items: center;
                li {
                    display: flex;
                    align-items: center;
                    margin-left: 13px;
                    &:first-child {
                        margin-left: 0px;
                        padding-right: 13px;
                        border-right: 1px solid rgba(255, 255, 255, 0.5);
                        span {
                            font-size: 15px;
                            line-height: 18px;
                            color: #FFFFFF;
                            margin-right: 11px;
                        }
                    }
                    &:last-child {
                        a {
                            span {
                                margin-left: 8px;
                                font-size: 15px;
                                line-height: 18px;
                                color: #FFFFFF;
                            }
                        }
                    }
                    a {
                        display: flex;
                        align-items: center;
                    }
                    .phone {
                        display: flex;
                        align-items: center;
                        p {
                            display: flex;
                            align-items: center;
                            font-size: 15px;
                            line-height: 18px;
                            color: #FFFFFF;
                            img {
                                margin-right: 10px;
                            }
                            a {
                                font-weight: 600;
                                font-size: 15px;
                                line-height: 18px;
                                color: #FFFFFF;
                                margin-right: 5px;
                            }
                        }
                    }
                }
            }
        }
        .geral {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-between;
            .area-user {
                position: relative;
                display: flex;
                align-items: center;
                .btn-user {
                    display: flex;
                    align-items: center;
                    img {
                        width: 22px;
                    }
                    .info-user {
                        margin-left: 13px;
                        span {
                            text-align: left;
                            display: block;
                            font-size: 14px;
                            color: #fff;
                            line-height: 1;
                        }
                        p {
                            font-size: 14px;
                            color: #fff;
                        }
                    }
                }
                &.active {
                    .dropdown-cadastro {
                        opacity: 1;
                        top: 55px;
                        pointer-events: all;
                        transition: all .3s;
                    }
                }
                
                .dropdown-cadastro {
                    position: relative;
                    background-color: #FFFFFF;
                    box-shadow: 0px 8px 25px rgba(166, 166, 166, 0.2);
                    border-radius: 6px;
                    position: absolute;
                    top: 35px;
                    left: 50%;
                    width: 312px;
                    height: 216px;
                    margin-left: -177px;
                    padding: 18px 15px;
                    opacity: 0;
                    pointer-events: none;
                    transition: all .3s;
                    &:before {
                        content: "";
                        width: 14px;
                        height: 14px;
                        background-color: #ffffff;
                        border-radius: 2px;
                        transform: rotate(-45deg);
                        position: absolute;
                        top: -5px;
                        left: 50%;
                        margin-left: -7px;
                    }
                    .btn-entrar {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        background-color: #FF9D2E;
                        border: 2px solid #FF9D2E;
                        border-radius: 8px;
                        width: 100%;
                        height: 58px;
                        text-transform: uppercase;
                        color: #FFFFFF;
                        font-weight: 600;
                        margin-bottom: 25px;
                        transition: all .3s;
                        &:hover {
                            color: #FF9D2E;
                            background-color: transparent;
                            transition: all .3s;
                        }
                    }
                    .btn-cadastro {
                        display: block;
                        text-align: center;
                        margin-bottom: 22px;
                        color: #646981;
                        line-height: 23px;
                        transition: all .3s;
                        &:hover {
                            transition: all .3s;
                            color: $azul;
                        }
                    }
                    ul {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        border-top: 1px solid #DCDDE3;
                        margin-top: 22px;
                        padding-top: 22px;
                        li {
                            &:first-child {
                                padding-right: 31px;
                                margin-right: 27px;
                                border-right: 1px solid #DCDDE3;
                            }
                            a {
                                color: #646981;
                                display: flex;
                                align-items: center;
                                height: 30px;
                                transition: all .3s;
                                &:hover {
                                    transition: all .3s;
                                    color: $azul;
                                }
                            }
                        }
                    }
                }
            }
            .favoritos {
                position: relative;
            }
        }
    }
    .search {
        width: 528px;
        height: 48px;
        display: flex;
        align-items: center;
        background-color: #F6F6F6;
        border-radius: 6px;
        padding: 4px 6px 4px 16px;
        input {
            flex: 1 1 auto;
            padding-right: 16px;
            color: #A0A4B3;
            font-size: 15px;
            line-height: 18px;
            background-color: transparent;
            &::placeholder {
                color: #A0A4B3;
            }
        }
        button {
            width: 40px;
            height: 40px;
            background-color: #FF9D2E;
            border-radius: 6px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    }
    .favoritos {
        position: relative;
        a {
            display: block;
        }
        .alert {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 14px;
            height: 14px;
            background-color: #FF7100;
            color: #ffffff;
            border-radius: 50%;
            position: absolute;
            right: -2px;
            bottom: -3px;
            font-weight: 500;
            font-size: 12px;
        }
    }
    .area-cart {
        position: relative;
        &.active {
            transition: all .3s;
            .btn-show-cart {
                background-color: #0C64D3;
                transition: all .3s;
            }
            .dropdown-cart {
                opacity: 1;
                pointer-events: all;
                top: 65px;
                transition: all .3s;
            }
        }
        .btn-show-cart {
            background: rgba(93, 168, 255, 0.34);
            border-radius: 6px;
            width: 98px;
            height: 48px;
            display: flex;
            align-items: center;
            justify-content: center;
            transition: all .3s;
            img {
                margin-right: 22px;
            }
            span {
                color: #FFFFFF;
                font-size: 20px;
                font-weight: 500;
            }
        }
        .dropdown-cart {
            position: absolute;
            background-color: #FFFFFF;
            box-shadow: 0px 8px 25px rgba(166, 166, 166, 0.2);
            border-radius: 6px;
            width: 328px;
            top: 60px;
            left: -215px;
            padding: 38px 24px 26px 24px;
            opacity: 0;
            pointer-events: none;
            transition: all .3s;
            &:before {
                content: "";
                width: 14px;
                height: 14px;
                border-radius: 2px;
                transform: rotate(-45deg);
                position: absolute;
                top: -6px;
                right: 50px;
                margin-left: -7px;
                background-color: #ffffff;
            }
            .empty-cart {
                padding: 50px 0px;
                display: none;
                img {
                    margin: 0 auto;
                    margin-bottom: 34px;
                }
                h3 {
                    text-align: center;
                    color: #646981;
                    font-size: 18px;
                    font-weight: bold;
                }
            }
            .all-prods {
                .item {
                    display: flex;
                    align-items: center;
                    padding-bottom: 25px;
                    margin-bottom: 25px;
                    border-bottom: 1px solid #DCDDE3;
                    .photo {
                        box-shadow: 0px 4px 8px rgba(71, 75, 91, 0.13);
                        border-radius: 4px;
                        width: 60px;
                        height: 60px;
                        @include elemento_no_centro(column);
                        margin-right: 18px;
                        img {
                            max-width: 44px;
                        }
                    }
                    .info {
                        position: relative;
                        flex: 1 1 auto;
                        max-width: 201px;
                        h3 {
                            max-width: 163px;
                            font-size: 14px;
                            line-height: 17px;
                            color: #363843;
                            margin-bottom: 12px;
                        }
                        .valor-qtd {
                            display: flex;
                            align-items: center;
                            justify-content: space-between;
                            span {
                                font-size: 13px;
                                line-height: 16px;
                                color: #A0A4B3;
                            }
                            h4 {
                                font-weight: 500;
                                font-size: 13px;
                                line-height: 16px;
                                color: #363843;
                            }
                        }
                        button {
                            position: absolute;
                            top: 5px;
                            right: 0;
                            background-color: transparent;
                            z-index: 2;
                        }
                    }
                }
            }
            .total {
                display: flex;
                align-items: center;
                justify-content: flex-end;
                margin-bottom: 29px;
                span {
                    margin-right: 55px;
                    color: #646981;
                    font-size: 13px;
                }
                h3 {
                    color: #0D71F0;
                    font-weight: 600;
                    font-size: 14px;
                }
            }
            .btn-continuar {
                width: 100%;
                height: 58px;
                display: flex;
                align-items: center;
                justify-content: center;
                background-color: #6BB70B;
                border: 2px solid #6BB70B;
                border-radius: 8px;
                font-weight: 600;
                letter-spacing: -0.114286px;
                text-transform: uppercase;
                color: #FFFFFF;
                transition: all .3s;
                &:hover {
                    background-color: transparent;
                    color: #6BB70B;
                    transition: all .3s;
                }
            }
        }
    }
    .area-menu {
        display: flex;
        align-items: center;
        cursor: pointer;
        span {
            font-size: 15px;
            color: #FFFFFF;
            margin-right: 18px;
        }
    }
    @media(max-width: 1200px) {
        height: 127px;
        padding: 0px 20px;
        .container {
            height: 100%;
            padding: 0;
        }
        .theo {
            display: none;
        }
        .right {
            width: 100%;
            .top {
                display: none;
            }
            .geral {
                .logo {
                    position: absolute;
                    top: 20px;
                    max-width: 105px;
                    left: 37px;
                    img {
                        max-width: 100%;
                    }
                }
                .area-user {
                    position: absolute;
                    top: 22px;
                    right: 77px;
                    &.active {
                        .dropdown-cadastro {
                            top: 45px;
                        }
                    }
                    .info-user {
                        display: none;
                    }
                    .dropdown-cadastro {
                        left: -38px;
                        &:before {
                            margin-left: 64px;
                        }
                    }
                }
                .favoritos {
                    position: absolute;
                    top: 23px;
                    right: 125px;
                }
            }
        }
        .area-cart {
            position: absolute;
            right: 0;
            top: 17px;
            .dropdown-cart {
                left: initial;
                right: 0;
            }
            .btn-show-cart {
                width: 55px;
                height: 34px;
                img {
                    margin-right: 9px;
                    max-width: 15px;
                }
                span {
                    font-size: 14px;
                }
            }
        }
        .search {
            position: absolute;
            left: 0;
            bottom: 20px;
            width: 100%;
            height: 40px;
            button {
                width: 32px;
                height: 32px;
                img {
                    max-width: 14px;
                }
            }
            input {
                font-size: 14px;
            }
        }
    }
`;