import React, { useState } from 'react';

import { SectionNewsletterPromocoes } from '../box-info-compra/style';

export default function ComponentInformacoesEntrega() {
    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    return (
        <SectionNewsletterPromocoes bg={true}>
            <div className="container">
                <div className="texto">
                    <h2>Aproveite nossas promoções!</h2>
                    <p>Cadastre seu e-mail e receba ofertas exclusivas</p>
                </div>
                <form action="">
                    <input 
                        type="text" 
                        placeholder="Digite seu nome"
                        value={nome}
                        onChange={e => setNome(e.target.value)}
                    />
                    <input 
                        type="email" 
                        placeholder="Digite seu e-mail"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <button type="button">quero receber</button>
                </form>
            </div>
        </SectionNewsletterPromocoes>
    );
}
