import React, { Component } from 'react';

class ComponentBoxLoja extends Component {
    render() { 
        return ( 
            <a href="" className="box-lojas">
                <div className="foto">
                    <img src={this.props.img_loja} className="img-loja" alt=""/>
                    <div className="logo">
                        <img src={this.props.logo} alt=""/>
                    </div>
                </div>
                <span>{this.props.nome_loja}</span>
            </a>
        );
    }
}
 
export default ComponentBoxLoja;