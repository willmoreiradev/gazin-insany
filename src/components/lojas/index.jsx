import React, { Component } from 'react';

import Title from  '../title-section';

import CarrouselLojas from '../slides/carrousel-lojas';

class SessaoLojas extends Component {
    render() { 
        return ( 
            <section className="s-loja">
                <div className="container">
                    <Title titulo="Conheça as lojas parceiras" subtitulo="Juntos somos mais, conheça as lojas parceiras Gazin"/>
                    <CarrouselLojas/>
                </div>
            </section>
        );
    }
}
 
export default SessaoLojas;