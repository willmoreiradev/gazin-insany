import React, { Component } from 'react';

import { SectionInfoPagamento } from './style';


import IconHomeCard from '../../images/icon-home-card.svg';
import IconDisconto from '../../images/icon-discount.svg';
import IconDelivery from '../../images/icon-home-delivery.svg';


export default function ComponentInfoPagamentoOferta() {
    return ( 
        <SectionInfoPagamento>
            <div className="container">
                <ul>
                    <li>
                        <img src={IconHomeCard} alt=""/>
                        <p>Pague em até <strong>12x sem juros</strong></p>
                    </li>
                    <li>
                        <img src={IconDisconto} alt=""/>
                        <p>Receba 5% de <strong>desconto no boleto</strong></p>
                    </li>
                    <li>
                        <img src={IconDelivery} alt=""/>
                        <p>Frete grátis a partir de R$ 100,00</p>
                    </li>
                </ul>
            </div>
        </SectionInfoPagamento>
    );
}
 