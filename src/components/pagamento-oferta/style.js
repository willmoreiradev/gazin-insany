import styled from 'styled-components';

export const SectionInfoPagamento = styled.section`
    border-bottom: 1px solid #E8E8E8;
    height: 82px;
    display: flex;
    align-items: center;
    ul {
        margin-bottom: 0px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        li {
            display: flex;
            align-items: center;
            img {
                margin-right: 13px;
            }
            p {
                margin-bottom: 0px;
                font-size: 16px;
                line-height: 19px;
                color: #363843;
                strong {
                    font-weight: 600;
                    font-size: 16px;
                    line-height: 19px;
                    color: #363843;
                }
            }
        }
    }
    @media(max-width: 480px) {
        height: auto;
        padding: 40px 0px;
        .container {
            display: flex;
            justify-content: center;
        }
        ul {
            flex-direction: column;
            align-items: flex-start;
            max-width: 290px;
            li {
                margin-bottom: 37px;
                &:last-child {
                    margin-bottom: 0px;
                }
                &:nth-child(2) {
                    p {
                        strong {
                            display: block;
                        }
                    }
                }
                p {
                    flex: 1;
                    font-size: 14px;
                    strong {
                        font-size: 14px;
                    }
                }
            }
        }
    }
`;