import React, { Component } from 'react'

import {Link} from 'react-router-dom'

import styled from 'styled-components'

import IconCartaoActive from '../../images/icon-credito.svg'
import IconBoleto from '../../images/icon-boleto.svg'
import IconBoletoActive from '../../images/boleto-active.svg'
import IconCartao from '../../images/icon-debito.svg'

import Visa from '../../images/bandeiras/visa.svg';
import MasterCard from '../../images/bandeiras/mastercard.svg';
import HiperCard from '../../images/bandeiras/hipercard.svg';
import Diners from '../../images/bandeiras/diners-club.svg';
import Boleto from '../../images/bandeiras/boleto.svg';
import Printer from '../../images/print.svg';
import Computer from '../../images/computer.svg';
import Calendar from '../../images/calendar.svg';


import Cvv from '../../images/cvv.svg';


import { StyleInput, MultiInput } from '../../Style/global';

const Left = styled.div`
    width: 296px;
    ul {
        display: flex;
        flex-direction: column;
        li {
            position: relative;
            display: flex;
            align-items: center;
            height: 69px;
            background-color: #F6F6F6;
            border: 1px solid #DDDDDD;
            border-right: none;
            border-top: none;
            padding-left: 15px;
            cursor: pointer;
            transition: all .3s;
            &:after {
                content: "";
                position: absolute;
                top: 0;
                right: -1px;
                width: 1px;
                height: 100%;
                background-color: #ffffff;
                opacity: 0;
                transition: all .3s;
            }
            &.selected {
                background-color: #ffffff;
                transition: all .3s;
                &:after {
                    opacity: 1;
                    transition: all .3s;
                }
                .circle {
                    &:before {
                        transform: scale(1);
                        transition: all .3s;
                    }
                }
                .info{
                    span{
                        font-weight: 600;
                        color: #363843;
                    }
                }
            }
            &:hover {
                background-color: #FFFFFF;
                transition: all .3s;
            }
            &:first-child {
                border-radius: 6px 0px 0px 0px;
                border-top: 1px solid #DDDDDD;
            }
            &:last-child {
                border-radius: 0px 0px 0px 6px;
                border-top: none;
            }
            .info {
                display: flex;
                align-items: center;
                img{
                    margin-right: 15px;
                    max-width: 29px;
                }
                span {
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: normal;
                    font-size: 16px;
                    line-height: 20px;

                    color: #646981;
                }
                h5 {
                    font-size: 15px;
                    line-height: 24px;
                    color: #363843;
                }
                p {
                    margin: 0;
                    font-size: 13px;
                    line-height: 21px;
                    color: #A0A4B3;
                    strong {
                        font-size: 13px;
                        line-height: 21px;
                        color: #646981;
                    }
                }
            }
        }
    }
    @media(max-width: 1200px) {
        width: 100%;
        ul {
            flex-direction: row;
            justify-content: space-between;
            li {
                padding: 0;
                width: 33%;
                justify-content: center;
                border: 1px solid #DDDDDD !important;
            }
        }
    }
    @media(max-width: 480px) {
        ul {
            li {
                .info {
                    flex-direction: column;
                    align-items: center;
                    img {
                        margin-right: 0;
                        margin-bottom: 5px;
                    }
                    span {
                        text-align: center;
                        font-size: 14px;
                        line-height: 15px;
                    }
                }
            }
        }
    }
`;

const Right = styled.div`
    width: 762px;
    border-radius: 0px 6px 6px 6px;
    border: 1px solid #DDDDDD;
    background-color: #ffffff;
    padding: 40px 29px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    @media(max-width: 1200px) {
        width: 100%;
        height: auto;
    }
    @media(max-width: 480px) {
        padding: 20px;
    }
`;

export const PagamentoContainer = styled.section`
    .container{
        width: 100%;
        padding: 0px;
    }
    p{
        padding-top: 64px;
        strong{
            font: normal bold 14px/24px 'Inter';
            color: #7C7D80;
        }
        font: normal normal 14px/24px 'Inter';
        color: #7C7D80;

    }
    @media(max-width: 1200px) {
        padding: 40px 0px;
        .container {
            width: 100%;
        }
        .title {
            h2 {
                text-align: center;
                margin-bottom: 30px;
            }
        }
        p {
            padding-top: 30px;
            text-align: center;
        }
    }
    @media(max-width: 480px) {
        padding-top: 0;
    }
`

const Opcoes = styled.div`
    display: flex;
    align-items: flex-start;
    @media(max-width: 1200px) {
        flex-direction: column;
        align-items: center;
    }

`;

const PagamentoForm = styled.form`
    width: 467px;
    .topInput{
        .pagamento{
            display: flex;
            align-items: center;
            width: 284px;
            height: 18px;
            ul{
                width: 284px;
                display: flex;
                justify-content: flex-end;
                align-items: center;
                margin-bottom: 11px;
                filter: grayscale(1);
                li{
                    margin-right: 20px;
                    &:last-child{
                        margin-right: 0;
                    }
                    img{
                        cursor: default;
                        position: relative;
                        top: 0;
                        left: 0;
                    }
                }
            }
        }
    }
    div${StyleInput}{
        .checkbox-label {
            position: relative;
            cursor: pointer;
            font-size: 22px;
            line-height: 24px;
            clear: both;
            padding-left: 35px;
            display: flex;
            align-items: center;
            font: normal normal 16px/19px 'Inter';
            color: #363843;
            height: 20px;
            margin-bottom: 44px;
            margin-top: 8px;

        }
        .checkbox-label input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        .checkbox-label .checkbox-custom {
            position: absolute;
            top: 0px;
            left: 0px;
            height: 20px;
            width: 20px;
            background-color: transparent;
            border-radius: 5px;
            border: 1px solid #DCDDE3;
        }


        .checkbox-label input:checked ~ .checkbox-custom {
            background-color: #0D71F0;
            border-radius: 5px;
            -webkit-transform: rotate(0deg) scale(1);
            -ms-transform: rotate(0deg) scale(1);
            transform: rotate(0deg) scale(1);
            opacity:1;
            border: 1px solid #0D71F0;
        }


        .checkbox-label .checkbox-custom::after {
            position: absolute;
            content: "";
            left: 12px;
            top: 12px;
            height: 0px;
            width: 0px;
            border-radius: 5px;
            border: solid #ffffff;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(0deg) scale(0);
            -ms-transform: rotate(0deg) scale(0);
            transform: rotate(0deg) scale(0);
            opacity:1;
        }


        .checkbox-label input:checked ~ .checkbox-custom::after {
            -webkit-transform: rotate(45deg) scale(1);
            -ms-transform: rotate(45deg) scale(1);
            transform: rotate(45deg) scale(1);
            opacity:1;
            left: 5px;
            top: 1px;
            width: 6px;
            height: 10px;
            border: solid #ffffff;
            border-width: 0 2px 2px 0;
            background-color: transparent;
            border-radius: 0;
        }

        .checkbox-label input:checked ~ .checkbox-custom::before {
            left: -3px;
            top: -3px;
            width: 24px;
            height: 24px;
            border-radius: 5px;
            -webkit-transform: scale(3);
            -ms-transform: scale(3);
            transform: scale(3);
            opacity:0;
            z-index: 999;
        }


    }
    div${MultiInput}{
        div{
            width: 50%;
        }
        select{
            width: 100%;
        }
        .total{
            font: normal normal 16px/22px 'Inter';
            color: #5E606B;
            display: flex;
            align-items: center;
            justify-content: center;
            strong{
                font: normal bold 16px/19px 'Inter';

            }
        }
    }
    button{
        width: 467px;
        height: 56px;
        background: #6BB70B;
        border-radius: 6px;
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        font-size: 16px;
        line-height: 19px;
        text-align: center;
        letter-spacing: -0.114286px;
        text-transform: uppercase;
        transform: scale(1);
        transition: all.3s;
        color: #FFFFFF;

        &:hover{
            transform: scale(1.05);
             transition: all.3s;
        }
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
    @media(max-width: 480px) {
        .topInput {
            flex-direction: column;
            margin-bottom: 15px;
            .pagamento {
                width: 100%;
                height: auto;
                ul {
                    width: 100%;
                }
            }
        }
        div${MultiInput}{
            div{
                margin-right: 15px;
                &:last-child {
                    margin-right: 0px;
                }
            }
            .total {
                flex-direction: column;
                align-items: flex-start;
                margin-top: 12px;
            }
        }
        button {
            width: 100%;
            font-size: 14px;
        }
    }
`

const BoletoContainer = styled.div`
    h2{
        font:normal 600 18px/26px 'Inter';
        color: #363843;
        margin-bottom: 30px;
    }
    .infos{
    margin-bottom: 40px;
    ul{
            max-width: 630px;
            margin-bottom: 79px;
            li{
                display: flex;
                justify-content: flex-start;
                align-items: flex-start;
                margin-bottom: 31px;
                &:last-child{
                    margin-bottom: 0;
                }
                .icon{
                    margin-right: 21px;
                }
                span{
                    font:normal normal 15px/20px 'Inter';
                    color: #363843;
                    strong{
                        font:normal bold 15px/20px 'Inter';
                    }
                }
            }
        }
    }
    .btn-container{
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 30px;
        span{
            font: normal normal 16px/23px 'Inter';
            color: #363843;
            margin-right: 32px;
            strong{
                font: normal bold 16px/19px 'Inter';
            }
        }
        button{
            width: 333px;
            height: 56px;
            background-color: #6BB70B;
            border-radius: 8px;
            font:normal 600 18px/19px 'Inter';
            letter-spacing: -0.114286px;
            text-transform: uppercase;
            color: #FFFFFF;
            transform: scale(1);
            transition: all .3s;
            &:hover{
                transform: scale(1.02);
                transition: all .3s;
            }
        }
    }
    .importante{
        h3{
            font:normal bold 15px/20px 'Inter';
            color: #363843;
            margin-bottom: 20px;
        }
        ul{
            max-width: 649px;
            li{
                list-style: initial;
                font:normal normal 14px/24px 'Inter';
            }
        }
    }
    @media(max-width: 480px) {
        .infos {
            ul {
                max-width: 100%;
                margin-bottom: 40px;
                li {
                    margin-bottom: 20px;
                    &:last-child {
                        margin-bottom: 0px;
                    }
                    .icon {
                        margin-right: 15px;
                        margin-top: 5px;
                    }
                    span {
                        max-width: 250px;
                        font-size: 14px;
                        line-height: 22px;
                    }
                }
            }
        }
        .btn-container {
            flex-direction: column;
            margin-bottom: 30px;
            button {
                width: 100%;
            }
            span {
                margin-right: 0;
                margin-bottom: 10px;
            }
        }
        .importante {
            h3 {
                margin-bottom: 10px;
                text-align: center;
            }
            ul {
                li {
                    margin: 15px;
                    font-size: 13px;
                    line-height: 19px;
                    &:last-child {
                        margin-bottom: 0px;
                    }
                }
            }
        }
    }
`


export default class PagamentoAction extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            activePaymentForm: 'credito'
        }
        this.lojaSelecionada = this.lojaSelecionada.bind(this);
    }

    lojaSelecionada(event) {
        const allStores = document.querySelectorAll('.stores li');
        allStores.forEach(index => index.classList.remove('selected'));
        let loja = event.currentTarget;
        loja.classList.add('selected');
        this.setState({activePaymentForm: loja.id})
    }
    render() {
        return (
            <PagamentoContainer>
                <div className="container">
                <Opcoes>
                    <Left>
                        <ul className="stores">
                            <li onClick={this.lojaSelecionada} id='credito' className="selected">
                                <div className="info">
                                    <img src={this.state.activePaymentForm == 'credito' ? IconCartaoActive : IconCartao} alt=""/>
                                    <span>Cartão de crédito</span>
                                </div>
                            </li>
                            <li onClick={this.lojaSelecionada} id='boleto' >
                                <div className="info">
                                    <img src={this.state.activePaymentForm == 'boleto' ? IconBoletoActive : IconBoleto} alt=""/>
                                    <span>Boleto</span>
                                </div>
                            </li>
                            <li onClick={this.lojaSelecionada} id='debito'>
                                <div className="info">
                                    <img src={this.state.activePaymentForm == 'debito' ? IconCartaoActive : IconCartao} alt=""/>
                                    <span>Débito</span>
                                </div>
                            </li>
                        </ul>
                    </Left>
                    <Right>
                        {
                            this.state.activePaymentForm === 'credito' || this.state.activePaymentForm === 'debito' ?

                                <PagamentoForm>
                                    <StyleInput>
                                        <div className="topInput">
                                            <label htmlFor="cartao">Número do cartão</label>
                                            <div className="pagamento">
                                                <ul>
                                                <li>
                                                    <img src={Visa} alt=""/>
                                                    </li>
                                                <li>
                                                    <img src={MasterCard} alt=""/>
                                                    </li>
                                                <li>
                                                    <img src={HiperCard} alt=""/>
                                                    </li>
                                                <li>
                                                    <img src={Diners} alt=""/>
                                                    </li>
                                                <li>
                                                    <img src={HiperCard} alt=""/>
                                                    </li>
                                                <li>
                                                    <img src={Boleto} alt=""/>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <input type="text" name="cartao" placeholder="Número impresso no cartão" required />
                                    </StyleInput>
                                    <StyleInput>
                                        <label htmlFor="mes">Nome</label>
                                        <input type="text" name="nome" placeholder="Nome" required />
                                    </StyleInput>
                                    <MultiInput>
                                        <StyleInput>
                                            <label htmlFor="mes">Validade mês</label>
                                            <select name="mes" required >
                                                <option value="01">Janeiro</option>
                                                <option value="02">Favereiro </option>
                                                <option value="03">Março</option>
                                                <option value="04">Abril</option>
                                                <option value="05">Maio</option>
                                                <option value="06">Junho</option>
                                                <option value="07">Julho</option>
                                                <option value="08">Agosto</option>
                                                <option value="09">Setembro</option>
                                                <option value="10">Outubro</option>
                                                <option value="11">Novembro</option>
                                                <option value="12">Dezembro</option>
                                            </select>
                                        </StyleInput>
                                        <StyleInput>
                                            <label htmlFor="ano">Validade ano</label>
                                            <select name="ano" required >
                                                <option value="19"> 2019</option>
                                                <option value="20"> 2020</option>
                                                <option value="21"> 2021</option>
                                                <option value="22"> 2022</option>
                                                <option value="23"> 2023</option>
                                                <option value="24"> 2024</option>
                                                <option value="25"> 2025</option>
                                                <option value="26"> 2026</option>
                                                <option value="27"> 2027</option>
                                                <option value="28"> 2028</option>
                                                <option value="29"> 2029</option>
                                                <option value="30"> 2030</option>
                                                <option value="31"> 2031</option>
                                                <option value="32"> 2032</option>
                                                <option value="33"> 2033</option>
                                                <option value="34"> 2034</option>
                                                <option value="35"> 2035</option>
                                                <option value="36"> 2036</option>
                                                <option value="37"> 2037</option>
                                                <option value="38"> 2038</option>
                                                <option value="39"> 2039</option>
                                            </select>
                                        </StyleInput>
                                    </MultiInput>
                                    <MultiInput>
                                        <StyleInput>
                                            <label htmlFor="cvv">CVV</label>
                                            <input type="text" name="cvv" placeholder="CVV" />
                                        </StyleInput>
                                            <img src={Cvv} alt=""/>
                                    </MultiInput>
                                    <MultiInput>
                                        <StyleInput>
                                            <label htmlFor="parcelas">Parcelas em </label>             
                                            <select name="parcelas">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </StyleInput>
                                        <span className="total">Total:&nbsp;<strong>R$ 3.200,00</strong></span>
                                    </MultiInput>
                                    <StyleInput>
                                        <label className="checkbox-label checkPreferencias">
                                            <input name="salvar" type="checkbox" />
                                            <span className="checkbox-custom rectangular"></span>
                                            Salvar dados para compras futuras
                                        </label>
                                    </StyleInput>
                                </PagamentoForm>

                            :

                                <BoletoContainer>
                                    <h2>Pagar com boleto</h2>
                                    <div className="infos">
                                        <ul>
                                            <li>
                                                <div className="icon">
                                                    <img src={Printer} alt=""/>
                                                </div>
                                                <span>Imprima o boleto e pague nas lotéricas ou em qualquer agência bancária</span>
                                            </li>
                                            <li>
                                                <div className="icon">
                                                    <img src={Computer} alt=""/>
                                                </div>
                                                <span>Ou pague pela internet utilizando o código de barras do boleto</span>
                                            </li>
                                            <li>
                                                <div className="icon">
                                                    <img src={Calendar} alt=""/>
                                                </div>
                                                <span>Fique atendo a data de vencimento do boleto bancário, pois, o boleto tem <strong>vence em 4 dias</strong>, após este período não será possível mais o pagamento do boleto</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="btn-container">
                                        <span>Total: <strong>R$ 3.200,00</strong></span>
                                        <button>Pagar com boleto</button>
                                    </div>
                                    <div className="importante">
                                        <h3>Importante</h3>
                                        <ul>
                                            <li>Caso o seu computador tenha um programa anti pop-up, será preciso desativá-lo antes de finalizar sua compra e imprimir o boleto ou pagar pelo internet banking;</li>
                                            <li>Não faça depósito ou transferência entre contas. O boleto não é enviado pelos Correios;</li>
                                            <li>Imprima-o e pague-o no banco ou pela internet;</li>
                                            <li>Se o boleto não for pago até a data de vencimento, o pedido será automaticamente cancelado;</li>
                                            <li>O prazo de entrega dos pedidos pagos com boleto bancário começa a contar três dias depois do pagamento do boleto, tempo necessário para que a instituição bancária confirme o pagamento.</li>
                                        </ul>
                                    </div>
                                </BoletoContainer>
                        }

                    </Right>
                </Opcoes>
                <p>
                    <strong>Termos:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer hendrerit neque augue, vel blandit quam imperdiet nec. Mauris posuere orci ac elit placerat, ac vehicula orci rhoncus. Praesent lobortis fermentum lectus eu mattis. Sed eu faucibus nibh. Sed vulputate gravida tincidunt. Integer interdum efficitur turpis, vel dignissim sapien pulvinar ornare. Sed vel purus et elit placerat pulvinar vel nec nisl. Donec condimentum, sapien ut accumsan mattis, ex arcu ultricies tellus, non pretium purus augue sed nulla.
                </p>
                </div>

            </PagamentoContainer>
        )
    }
}
