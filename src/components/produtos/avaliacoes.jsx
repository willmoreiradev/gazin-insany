import React, { Component } from 'react';
import { SessaoAvaliacao, Left, Right, AvaliacaoGeral, Nota } from './style.js';

// Others Components
import Icone from '../fontawesome'
import SlideAvaliacoes from '../slides/carrousel-avaliacoes';


class Avaliacao extends Component {
    render() { 
        return ( 
            <div className="container">
                <SessaoAvaliacao>
                    <div className="topo">
                        <Left>
                            <AvaliacaoGeral>
                                <h3>Avaliações</h3>
                                <ul>
                                    <li><Icone nome_icone="star" /></li>
                                    <li><Icone nome_icone="star" /></li>
                                    <li><Icone nome_icone="star" /></li>
                                    <li><Icone nome_icone="star" /></li>
                                    <li><Icone nome_icone="star" /></li>
                                </ul>
                            </AvaliacaoGeral>
                            <Nota>
                                <h3>Nota 4,9</h3>
                                <a href="#">(21) Avaliar produto</a>
                            </Nota>
                        </Left>
                        <Right>
                            <button type="button">Avalie este produto</button>
                            <select>
                                <option value="">Mais recentes</option>
                                <option value="">Mais vistos</option>
                            </select>
                        </Right>
                    </div>
                    <SlideAvaliacoes/>
                </SessaoAvaliacao>
            </div>
        );
    }
}
 
export default Avaliacao;