import React, { Component } from 'react';
import {    
        BoxProduto, 
        ProdutoIndisponivel, 
        ValorProduto, 
        Oferta, 
        BotaoFavorito, 
        QtdCores, 
        ImgProduto,
        InfoProduto 
    } from './style.js';

import Icone from '../fontawesome';

import Cores from '../../images/cores.svg';

class Disponivel extends Component {
    render() {
        if(this.props.indisponivel){
            return(
                <ProdutoIndisponivel className="prod-indisponivel">
                    <span>Produto indisponível</span>
                    <a href="">Avise-me quando chegar</a>
                </ProdutoIndisponivel>

            )
        } else{
            return (
                <ValorProduto className="valores">
                    <div className="valor-total-parcelado">
                        <strong>R$ 3.999,00</strong>
                        <p>ou <strong>12x</strong> de <strong>R$ 333,25</strong></p>
                    </div>
                    <div className="valor-vista">
                        <h2><span>ou</span> R$ 3.799,00 </h2>
                        <span>no boleto à vista</span>
                    </div>
                </ValorProduto>
            )
        }

    }
}

class ComponentBoxProduto extends Component {
    funcaoFavoritar = (e) => {
        e.preventDefault();
        let botao = e.currentTarget;
        let spanText = botao.firstChild;
        if(spanText.classList.contains('fa-heart-o')) {
            spanText.classList.remove('fa-heart-o');
            spanText.classList.add('fa-heart');
        } else {
            spanText.classList.add('fa-heart-o');
            spanText.classList.remove('fa-heart');
        }
    }
    render() { 
        return ( 
            <BoxProduto to="/detalhes-produto">
                <Oferta>{this.props.porcetagem_oferta} off</Oferta>
                <BotaoFavorito type='button' onClick={this.funcaoFavoritar}>
                    <Icone nome_icone="heart-o"/>
                </BotaoFavorito>
                <QtdCores src={Cores} alt=""/>
                <ImgProduto>
                    <img src={this.props.caminho_img} alt=""/>
                </ImgProduto>
                <InfoProduto>
                    <ul>
                        <li><Icone nome_icone="star"/></li>
                        <li><Icone nome_icone="star"/></li>
                        <li><Icone nome_icone="star"/></li>
                        <li><Icone nome_icone="star"/></li>
                        <li><Icone nome_icone="star disabled"/></li>
                    </ul>
                    <h3>{this.props.nome_produto}</h3>
                </InfoProduto>
                <Disponivel 
                    indisponivel={this.props.indisponivel} 
                    valor_de={this.props.valor_de}
                    valor_produto={this.props.valor_produto}
                    valor_parcelado={this.props.valor_parcelado}
                    parcelas={this.props.parcelas}
                />
            </BoxProduto>
        );
    }
}
 
export default ComponentBoxProduto;