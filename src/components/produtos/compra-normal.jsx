import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Cores, CorDisponivel } from './style';
import BuscarCep from '../cep/buscar-cep';

//Images

import IconeCarrinho from '../../images/icone-carrinho-white.svg';
import CartaoAzul from '../../images/cartao-azul.svg';
import IconeFrete from '../../images/frete-azul.svg';


class MetodoCompraNormal extends Component {
    selectColor(event) {
        const TodasCores = document.querySelectorAll('.cores li');
        let color = event.currentTarget.parentElement;
        TodasCores.forEach(cor => cor.classList.remove('selected'));
        color.classList.add('selected');
    }
    render() {
        return (
            <div className="tipo-compra-normal">
                <div className="valores-gerais">
                    <div className="valor-parcela">
                        <strong>R$ 1.583,12</strong>
                        <p>ou <strong>12x</strong> de <strong>R$ 131,92</strong> sem juros</p>
                    </div>
                    <div className="valor-total">
                        <h3>ou <strong>R$ 1.503,00</strong> à vista no boleto </h3>
                    </div>
                </div>
                <Cores>
                    <span>Cor: Azul Safira</span>
                    <ul className="cores">
                        <li className="selected">
                            <CorDisponivel
                                onClick={this.selectColor}
                                color="#DDDFE0"
                            />
                        </li>
                        <li>
                            <CorDisponivel
                                onClick={this.selectColor}
                                color="#2C5290"
                            />
                        </li>
                    </ul>
                </Cores>
                <Link to="/detalhes-garantia" className="btn-continuar">
                    <img src={IconeCarrinho} alt="" />
                    Comprar
                </Link>
                <button type="button" className="botao-parcelamento">
                    <img src={CartaoAzul} alt="" />
                    <span>Formas de parcelamento</span>
                </button>
                <div className="frete">
                    <div className="title">
                        <img src={IconeFrete} alt="" />
                        <span>Calcular frete e prazo</span>
                    </div>
                    <BuscarCep mostrar_botao={true}/>
                </div>
            </div>
        );
    }
}

export default MetodoCompraNormal;