import React, { Component } from 'react';
import styled from 'styled-components';
import { Content, Opcoes } from './style.js';

import {Link} from 'react-router-dom';

import MetodoCompraNormal from './compra-normal';


const Left = styled.div`
    width: 220px;
    ul {
        display: flex;
        flex-direction: column;
        li {
            position: relative;
            display: flex;
            align-items: center;
            padding: 16px 24px;
            padding-right: 10px;
            background-color: #F6F6F6;
            border: 1px solid #DDDDDD;
            border-right: none;
            border-top: none;
            cursor: pointer;
            transition: all .3s;
            &:after {
                content: "";
                position: absolute;
                top: 0;
                right: -1px;
                width: 1px;
                height: 100%;
                background-color: #ffffff;
                opacity: 0;
                transition: all .3s;
            }
            &.selected {
                background-color: #ffffff;
                transition: all .3s;
                &:after {
                    opacity: 1;
                    transition: all .3s;
                }
                .circle {
                    &:before {
                        transform: scale(1);
                        transition: all .3s;
                    }
                }
            }
            &:hover {
                background-color: #FFFFFF;
                transition: all .3s;
            }
            &:first-child {
                border-radius: 6px 0px 0px 0px;
                border-top: 1px solid #DDDDDD;
            }
            &:last-child {
                border-radius: 0px 0px 0px 6px;
                border-top: none;
            }
            .circle {
                width: 20px;
                height: 20px;
                border-radius: 50%;
                margin-right: 18px;
                background: #FFFFFF;
                border: 1px solid #CACACA;
                width: 20px;
                height: 20px;
                display: flex;
                align-items: center;
                justify-content: center;
                &:before {
                    content: "";
                    width: 10px;
                    height: 10px;
                    background: #0D71F0;
                    border-radius: 50%;
                    transform: scale(0);
                    transition: all .3s;
                }
            }
            .info {
                span {
                    font-size: 14px;
                    line-height: 24px;
                    color: #646981;
                    &:last-child {
                        font-size: 12px;
                    }
                }
                h5 {
                    font-size: 15px;
                    line-height: 24px;
                    color: #363843;
                }
                p {
                    margin: 0;
                    font-size: 13px;
                    line-height: 21px;
                    color: #A0A4B3;
                    strong {
                        font-size: 13px;
                        line-height: 21px;
                        color: #646981;
                    }
                }
            }
        }
    }
    @media(max-width: 480px) {
        width: 100%;
        margin-bottom: 20px;
        ul {
            li {
                padding: 15px;
                height: auto;
                border-radius: 6px !important;
                border: 1px solid #DDDDDD !important;
                margin-bottom: 10px;
                &:last-child {
                    margin-bottom: 0px;
                }
                &:after {
                    display: none;
                }
            }
        }
    }
`;

const Right = styled.div`
    width: 437px;
    border-radius: 0px 6px 6px 6px;
    border: 1px solid #DDDDDD;
    background-color: #ffffff;
    padding: 40px 29px;
    @media(max-width: 1200px) {
        width: 100%;
    }
    @media(max-width: 480px) {
        padding: 20px;
    }
`;

class ComportamentoCompraPorLoja extends Component {
    lojaSelecionada(event) {
        const allStores = document.querySelectorAll('.stores li');
        allStores.forEach(index => index.classList.remove('selected'));
        let loja = event.currentTarget;
        loja.classList.add('selected');
    }
    render() {
        return (
            <Content>
                <h4>Escolha uma loja e compre</h4>
                <Opcoes>
                    <Left>
                        <ul className="stores">
                            <li onClick={this.lojaSelecionada} className="selected">
                                <div className="circle"></div>
                                <div className="info">
                                    <span>Gazin</span>
                                    <h5>R$ 3.999,00</h5>
                                    <span>Frete: -</span>
                                </div>
                            </li>
                            <li onClick={this.lojaSelecionada}>
                                <div className="circle"></div>
                                <div className="info">
                                    <span>Loja Oficial Apple</span>
                                    <h5>R$ 3.999,00</h5>
                                    <span>Frete: -</span>
                                </div>
                            </li>
                            <li onClick={this.lojaSelecionada}>
                                <div className="circle"></div>
                                <div className="info">
                                    <span>Loja Oficial Apple</span>
                                    <h5>R$ 3.999,00</h5>
                                    <span>Frete: -</span>
                                </div>
                            </li>
                            <li>
                                <Link to="/detalhes-lista-fornecedores">
                                    <div className="info">
                                        <p>
                                            Mais opções de produtos a partir de <strong>R$ 390,00</strong>
                                        </p>
                                    </div>
                                </Link>
                            </li>
                        </ul>
                    </Left>
                    <Right>
                        <MetodoCompraNormal/>
                    </Right>
                </Opcoes>
            </Content>
        );
    }
}

export default ComportamentoCompraPorLoja;