import React, { Component } from 'react';
import ReactPlayer from 'react-player'

class DescricaoProduto extends Component {
    render() { 
        return ( 
            <div className="container">
                <div className="descricao-produto">
                    <div className="texto">
                        <h3>Descrição do produto</h3>
                        <div className="desc">
                            <p>
                                Experiência cinematográfica com a primeira tela CinemaVision de 21:9
                                Tenha uma experiência imersiva onde você estiver com o motorolaone vison e sua superfície completamente envolvida pela tela de 6.3” Full HD+ e proporção 21:9.
                            </p>
                            <p>
                                Sensor 48 MP Quad Pixel com Night Vision1
                                Câmera traseira dupla com sensor de 48 MP1 com modo Night Vision e estabilização óptica, mais 25 MP para selfies incríveis. Tudo isso com a tecnologia Quad Pixel que proporciona quatro vezes mais sensibilidade a luz, para você e suas fotos ficarem sempre em destaque de dia ou de noite.
                            </p>
                            <p>
                                O poder do exclusivo TurboPowerTM nas suas mãos
                                Em apenas 15 minutos de carga você garante 7 horas de uso para tudo que o seu dia precisa. Você não para, o seu smartphone também não2.
                            </p>
                        </div>
                        <button type="button">Ver descricão completa</button>
                    </div>
                    <ReactPlayer 
                        url='https://www.youtube.com/watch?v=5TRTlOFsn0I'
                        light="true"
                        width={576}
                        height={324}
                        playing
                        className="video-descricao"
                    />
                </div>
            </div>
        );
    }
}
 
export default DescricaoProduto;