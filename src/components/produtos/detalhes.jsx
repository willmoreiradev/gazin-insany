import React, { Component } from 'react';
import DescricaoProduto from './descricao-produto';
import FichaTecnica from './ficha-tecnica';
import Avaliacoes from './avaliacoes';
import InformacoesGerais from './foto-informacoes';



class ComponentInformacoesCompra extends Component {
    render() {
        return (
            <section className="s-info-detalhes-prod">
                <InformacoesGerais  
                    nome_produto={`Smartphone Motorola One Vision 128GB 
                    Dual Chip Android Pie 9.0 Tela 6,3" 4G + 
                    Câmera 48+5MP - Azul Safira`}
                />
                <DescricaoProduto/>
                <FichaTecnica/>
                <Avaliacoes/>
            </section>
        );
    }
}

export default ComponentInformacoesCompra;