import React, { Component } from 'react';
import { Ficha } from './style.js';

import IconeInfoBasicas from '../../images/icone-info-basica.svg';
import IconeImportante from '../../images/icone-importante.svg';



class FichaTecnica extends Component {
    render() { 
        return ( 
            <div className="container">
                <Ficha>
                    <h3>Ficha técnica</h3>
                    <div className="dados">
                        <div className="item">
                            <div className="box-title">
                                <img src={IconeInfoBasicas} alt=""/>
                                <h4>Informações<br/> Básicas</h4>
                            </div>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Tipo</td>
                                        <td>Smartphone</td>
                                    </tr>
                                    <tr>
                                        <td>Marca</td>
                                        <td>Motorola</td>
                                    </tr>
                                    <tr>
                                        <td>Linha</td>
                                        <td>Motorola One</td>
                                    </tr>
                                    <tr>
                                        <td>Modelo</td>
                                        <td>iPhone 8 64GB Cinza Espacial Tela 4.7" IOS 4G Câmera 12MP </td>
                                    </tr>
                                    <tr>
                                        <td>Cores</td>
                                        <td>Cinza Espacial, Dourado, Prata</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="item">
                            <div className="box-title">
                                <img src={IconeImportante} alt=""/>
                                <h4>Importante</h4>
                            </div>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Garantia do fornecedor</td>
                                        <td>12 meses</td>
                                    </tr>
                                    <tr>
                                        <td>Itens inclusos</td>
                                        <td>01 iPhone com iOS 11, EarPods com Conector Lightning, Adaptador de Lightning para Conector de fones de ouvido de 3,5 mm, Cabo de Lightning para USB, Carregador USB, Documentação.</td>
                                    </tr>
                                    <tr>
                                        <td>Observações</td>
                                        <td>Todas as informações divulgadas são de responsabilidade do fabricante/fornecedor  </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="button">Ver ficha completa</button>
                </Ficha>
            </div>
        );
    }
}
 
export default FichaTecnica;