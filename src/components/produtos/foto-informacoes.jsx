import React, { Component } from 'react';
import { Btns, ButtonAcaoProduto, BoxRetirarLoja } from './style.js';
import Galeria from './galeria';
import Voltagem from './voltagem';
import MetodoCompraPorLoja from './compra-por-loja';
import MetodoCompraNormal from './compra-normal';
import Icone from '../fontawesome';

// images

import IconeRetirarLoja from '../../images/icone-retirar-loja.svg';

class GaleriaValores extends Component {
    render() {
        return (
            <div className="container">
                <div className="area-slide-prod">
                    <div className="box">
                        <Galeria/>
                        <Btns>
                            <ButtonAcaoProduto>
                                <Icone nome_icone="heart-o" />
                            </ButtonAcaoProduto>
                            <ButtonAcaoProduto>
                                <Icone nome_icone="share-alt" />
                            </ButtonAcaoProduto>
                        </Btns>
                    </div>
                </div>
                <div className="info-geral">
                    <div className="avaliacao">
                        <ul>
                            <li>
                                <Icone nome_icone="star yellow" />
                            </li>
                            <li>
                                <Icone nome_icone="star yellow" />
                            </li>
                            <li>
                                <Icone nome_icone="star yellow" />
                            </li>
                            <li>
                                <Icone nome_icone="star" />
                            </li>
                            <li>
                                <Icone nome_icone="star" />
                            </li>
                        </ul>
                        <span>4,9 (21) Avaliar produto</span>
                    </div>
                    <h2>{this.props.nome_produto}</h2>
                    {/* <BoxRetirarLoja>
                        <div className="icone">
                            <img src={IconeRetirarLoja} alt=""/>
                        </div>
                        <div className="info">
                            <h4>Retire na loja!</h4>
                            <p>Verifique a disponibilidade na página de opções de entrega e aproveite. </p>
                        </div>
                    </BoxRetirarLoja> */}
                    <Voltagem/>
                    <MetodoCompraPorLoja/>
                    {/* <MetodoCompraNormal/> */}
                </div>
            </div>
        );
    }
}

export default GaleriaValores;