import React, { Component } from 'react';
import { EspacoFoto, FotoModal, ModalSlider, ModalContainer, ThumbContainer, FotoThumb } from './style.js';
import Slider from "react-slick";

import FotoProduto from '../../images/imagem-produto-lg.jpg';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import CloseModalDetalhes from "../../images/close-modal-detalhes.svg"

import Share from  "../../images/share-normal.svg"


import HeartNormal from  "../../images/heart-normal.svg"
import HeartActive from  "../../images/heart-active.svg"


class GaleriaFotos extends Component {
    constructor(props) {
        super(props);
        this.state = {
          nav1: null,
          nav2: null,
          openModal: false,
            liked: false
        };
      }
    
      componentDidMount() {
        this.setState({
          nav1: this.slider1,
          nav2: this.slider2
        });
      }
    
    render() {
        const settings = {
            infinite: true,
            dots: false,
            className: "slider-prod",
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        const settingsModal = {
            className: "slideshow",
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
        };
        return (
            <div>
                <ModalContainer className={this.state.openModal ? "open" : null}>
                    <div className="overlay"></div>
                    <ModalSlider>
                        <div className="close"  onClick={()=>this.setState({openModal: false})}>
                            <img src={CloseModalDetalhes} alt=""/>
                        </div>
                        <div className="actions">
                            <ul>
                                <li className="like" onClick={()=>this.setState({liked: !this.state.liked})}>
                                    <img src={this.state.liked ? HeartActive : HeartNormal} alt=""/>
                                </li>
                                <li className="share">
                                    <img src={Share} alt=""/>
                                </li>
                            </ul>
                        </div>
                        <Slider {...settingsModal}>
                            <div>
                                <FotoModal>
                                    <img src={FotoProduto} />
                                </FotoModal>
                            </div>
                            <div>
                                <FotoModal>
                                    <img src={FotoProduto} />
                                </FotoModal>
                            </div>
                            <div>
                                <FotoModal>
                                    <img src={FotoProduto} />
                                </FotoModal>
                            </div>
                            <div>
                                <FotoModal>
                                    <img src={FotoProduto} />
                                </FotoModal>
                            </div>
                            <div>
                                <FotoModal>
                                    <img src={FotoProduto} />
                                </FotoModal>
                            </div>
                            <div>
                                <FotoModal>
                                    <img src={FotoProduto} />
                                </FotoModal>
                            </div>
                        </Slider>
                    </ModalSlider>
                </ModalContainer>
                <Slider {...settings} asNavFor={this.state.nav2} ref={slider => (this.slider1 = slider)}>
                    <div>
                        <EspacoFoto onClick={()=>this.setState({openModal: true})}>
                            <img src={FotoProduto} />
                        </EspacoFoto>
                    </div>
                    <div>
                        <EspacoFoto onClick={()=>this.setState({openModal: true})}>
                            <img src={FotoProduto} />
                        </EspacoFoto>
                    </div>
                    <div>
                        <EspacoFoto onClick={()=>this.setState({openModal: true})}>
                            <img src={FotoProduto} />
                        </EspacoFoto>
                    </div>
                    <div>
                        <EspacoFoto onClick={()=>this.setState({openModal: true})}>
                            <img src={FotoProduto} />
                        </EspacoFoto>
                    </div>
                    <div>
                        <EspacoFoto onClick={()=>this.setState({openModal: true})}>
                            <img src={FotoProduto} />
                        </EspacoFoto>
                    </div>
                    <div>
                        <EspacoFoto onClick={()=>this.setState({openModal: true})}>
                            <img src={FotoProduto} />
                        </EspacoFoto>
                    </div>
                </Slider>
                <ThumbContainer>
                    <Slider  
                        asNavFor={this.state.nav1}
                        ref={slider => (this.slider2 = slider)}
                        slidesToShow={5.1}
                        swipeToSlide={true}
                        focusOnSelect={true}
                        infinite={true}
                        responsive= {[
                            {
                              breakpoint: 781,
                              settings: {
                                slidesToShow: 5.1,
                              }
                            },
                            {
                              breakpoint: 480,
                              settings: {
                                slidesToShow: 4,
                                slidesToScroll: 1
                              }
                            }
                        ]}
                    >
                        <FotoThumb>
                            <img src={FotoProduto} />
                        </FotoThumb>
                        <FotoThumb>
                            <img src={FotoProduto} />
                        </FotoThumb>
                        <FotoThumb>
                            <img src={FotoProduto} />
                        </FotoThumb>
                        <FotoThumb>
                            <img src={FotoProduto} />
                        </FotoThumb>
                        <FotoThumb>
                            <img src={FotoProduto} />
                        </FotoThumb>
                        <FotoThumb>
                            <img src={FotoProduto} />
                        </FotoThumb>
                    </Slider>
                </ThumbContainer>
            </div>
        );
    }
}

export default GaleriaFotos;