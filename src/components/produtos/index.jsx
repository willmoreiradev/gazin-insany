import React, { Component } from 'react';

import CarrouselProdutos from '../slides/carrousel-produtos'

class ComponentSessaoProdutos extends Component {
    render() {
        return (
            <section className="s-produtos">
                <div className="container">
                    <div className="title">
                        <h2>{this.props.titulo}</h2>
                        <p>{this.props.subtitulo}</p>
                    </div>
                    <CarrouselProdutos/>
                </div>
            </section>
        );
    }
}

export default ComponentSessaoProdutos;