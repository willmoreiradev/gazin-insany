import React, { Component, useState } from 'react';
import { Link } from 'react-router-dom';

import BoxProduto from '../produtos/box-produto';
import Icone from '../fontawesome';
import Checkbox from '../checkbox';
import {ListaProd, StyleFiltroProdutosMobile} from './style'
import ArrowNextPaginador from '../../images/arrow-white-next.svg';
import IconFilter from '../../images/icon-filter.svg';
import CloseFilter from '../../images/close-filter.svg';



class ListaProdutos extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             direction: 'col',
             filter: false,
             selected: 'relevancia'
        }
    }

    handleChange(event) {
        this.setState({selected: event.target.value});
    }


    
    activeAvaliacao = (e) => {
        const itemPai = e.currentTarget.parentElement.parentElement;
        const avaliacao = itemPai.querySelectorAll('.nota');
        avaliacao.forEach(i => {
            i.classList.remove('active');
        })
        let item = e.currentTarget;
        item.classList.add('active');
    }
    changeGrid = (e) => {
        let elementPai = e.currentTarget.parentElement.parentElement.parentElement.parentElement;
        let grid = elementPai.querySelector('.all');
        let botao = e.currentTarget;
        grid.classList.toggle('column-three');
        botao.classList.toggle('active');
    }
    changeGridDirectionRow = (e) => {
        this.setState({direction: 'row'})
    }
    changeGridDirectionCol = (e) => {
        this.setState({direction: 'col'})
    }
    render() {

        return (
            <section className="s-lista-produtos">
                <StyleFiltroProdutosMobile visibleFilter={this.state.filter}>
                    <div className="topo">
                        <div className="left">
                            <button type="button" onClick={() => this.setState({ filter : false })}>
                                <img src={CloseFilter} alt=""/>
                            </button>
                            <h3>Filtros</h3>
                        </div>
                        <button type="button" className="btn-clear">Limpar tudo</button>
                    </div>

                    <div className="cont-filter">
                        <div className="item-filter">
                            <h3>Ordernar por</h3>
                            <select value={this.state.selected} onChange={this.handleChange}>
                                <option disabled value="relevancia">Relevância</option>
                                <option value="filtro-01">Filtro 01</option>
                                <option value="filtro-02">Filtro 02</option>
                                <option value="filtro-03">Filtro 03</option>
                            </select>
                        </div>
                        <div className="item-filter">
                            <h3>Ordernar por</h3>
                            <ul>
                                <li>
                                    <Checkbox label="Iphone" status="active"/>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label="Samsung Galaxy"/>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label="Motorola"/>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label="Xiaomi"/>
                                    <span>(120)</span>
                                </li>
                            </ul>
                        </div>
                        <div className="item-filter">
                            <h3>Descontos</h3>
                            <ul>
                                <li>
                                    <Checkbox label="até 10%" status="active"/>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label="de 10% à 30%"/>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label="de 30% à 60%"/>
                                    <span>(120)</span>
                                </li>
                            </ul>
                        </div>
                        <div className="item-filter">
                            <h3>Avaliação</h3>
                            <ul>
                                <li>
                                    <Checkbox label="" status="active"/>
                                    <div className="nota">
                                        <Icone nome_icone="star feat" />
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star feat"/>
                                    </div>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label=""/>
                                    <div className="nota">
                                        <Icone nome_icone="star feat" />
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star"/>
                                    </div>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label=""/>
                                    <div className="nota">
                                        <Icone nome_icone="star feat" />
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star"/>
                                        <Icone nome_icone="star"/>
                                    </div>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label=""/>
                                    <div className="nota">
                                        <Icone nome_icone="star feat" />
                                        <Icone nome_icone="star feat"/>
                                        <Icone nome_icone="star"/>
                                        <Icone nome_icone="star"/>
                                        <Icone nome_icone="star"/>
                                    </div>
                                    <span>(120)</span>
                                </li>
                                <li>
                                    <Checkbox label=""/>
                                    <div className="nota">
                                        <Icone nome_icone="star feat" />
                                        <Icone nome_icone="star"/>
                                        <Icone nome_icone="star"/>
                                        <Icone nome_icone="star"/>
                                        <Icone nome_icone="star"/>
                                    </div>
                                    <span>(120)</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="area-btn">
                        <button>Ver 75 Resultados</button>
                    </div>
                </StyleFiltroProdutosMobile>
                <div className="container">
                    {
                        this.props.isFavoritos 
                        ?
                            <div className="box-categorias-fav">
                                <div className="top-border"></div>
                                <h4>Categorias</h4>
                                <ul>
                                    <li>Áudio</li>
                                    <li className="active-categoria">Celulares e smartphones</li>
                                    <li>Eletroportáteis</li>
                                </ul>
                            </div>
                        :
                        <div className="filtros">
                            <div className="item">
                                <h3>Top Marcas</h3>
                                <ul>
                                    <li>
                                        <Link to="/departamentos">
                                            Iphone
                                            <span>(120)</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/departamentos">
                                            Samsung Galaxy
                                            <span>(120)</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/departamentos">
                                            Motorola
                                            <span>(120)</span>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/departamentos">
                                            Xiaomi
                                            <span>(120)</span>
                                        </Link>
                                    </li>
                                </ul>
                                <button>mostrar mais</button>
                            </div>
                            <div className="item">
                                <h3>Descontos</h3>
                                <ul>
                                    <li>
                                        <Checkbox label="até 10%" status="active"/>
                                        <span>(120)</span>
                                    </li>
                                    <li>
                                        <Checkbox label="de 10% à 30%"/>
                                        <span>(120)</span>
                                    </li>
                                    <li>
                                        <Checkbox label="de 30% à 60%"/>
                                        <span>(120)</span>
                                    </li>
                                </ul>
                            </div>
                            <div className="item">
                                <h3>Avaliação</h3>
                                <ul>
                                    <li>
                                        <div className="nota" onClick={this.activeAvaliacao}>
                                            <Icone nome_icone="star feat" />
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star feat"/>
                                        </div>
                                        <span>(120)</span>
                                    </li>
                                    <li>
                                        <div className="nota" onClick={this.activeAvaliacao}>
                                            <Icone nome_icone="star feat" />
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star"/>
                                        </div>
                                        <span>(120)</span>
                                    </li>
                                    <li>
                                        <div className="nota" onClick={this.activeAvaliacao}>
                                            <Icone nome_icone="star feat" />
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star"/>
                                            <Icone nome_icone="star"/>
                                        </div>
                                        <span>(120)</span>
                                    </li>
                                    <li>
                                        <div className="nota" onClick={this.activeAvaliacao}>
                                            <Icone nome_icone="star feat" />
                                            <Icone nome_icone="star feat"/>
                                            <Icone nome_icone="star"/>
                                            <Icone nome_icone="star"/>
                                            <Icone nome_icone="star"/>
                                        </div>
                                        <span>(120)</span>
                                    </li>
                                    <li>
                                        <div className="nota" onClick={this.activeAvaliacao}>
                                            <Icone nome_icone="star feat" />
                                            <Icone nome_icone="star"/>
                                            <Icone nome_icone="star"/>
                                            <Icone nome_icone="star"/>
                                            <Icone nome_icone="star"/>
                                        </div>
                                        <span>(120)</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    }
                    

                    <div className="all-prods">
                    {
                        this.props.isFavoritos
                        ?
                            <div className="topo" style={{paddingLeft: 150}}>
                                <div className="ordernar">
                                        <span>Ordernar por:</span>
                                        <select>
                                            <option defaultValue="">Maior destaque</option>
                                            <option defaultValue="">Menor destaque</option>
                                        </select>
                                </div>
                                <div className="exibicao">
                                    <div className="view">
                                        <span>Visualizar</span>
                                        <button style={{opacity: this.state.direction === 'col' ? 1 : 0.5}} className="dir-col" onClick={this.changeGridDirectionCol}>
                                            <i></i>
                                            <i></i>
                                            <i></i>
                                        </button>
                                    </div>
                                    <div className="view">
                                        <button style={{opacity: this.state.direction === 'col' ? 0.5 : 1}} className="dir-row" onClick={this.changeGridDirectionRow}>
                                            <i></i>
                                            <i></i>
                                            <i></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        :
                            <div className="topo" >
                                <span>Exibindo 1 - 42 de 100 resultado</span>
                                <button type="button" className="btn-filtro-mobile" onClick={() => this.setState({ filter : true })}>
                                    <img src={IconFilter} alt=""/>
                                    <span>Filtros</span>
                                </button>
                                <div className="exibicao">
                                    <div className="view">
                                        <span>Visualizar</span>
                                        <button onClick={this.changeGrid}>
                                            <i></i>
                                            <i></i>
                                            <i></i>
                                            <i></i>
                                        </button>
                                    </div>
                                    <div className="ordernar">
                                        <span>Ordernar por:</span>
                                        <select>
                                            <option defaultValue="">Maior destaque</option>
                                            <option defaultValue="">Menor destaque</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    }
                        <ListaProd direction={this.props.isFavoritos? this.state.direction : 'col'} className={this.props.isFavoritos ? "all fav-prod" : "all"}>
                            {
                                this.props.produtos.map((item, i) => (
                                    item ? (
                                    <BoxProduto
                                        key={i}
                                        indisponivel={item.indisponivel}
                                        porcetagem_oferta={item.porcetagem_oferta ? item.porcetagem_oferta : null}
                                        caminho_img={item.caminho_img}
                                        nome_produto={item.nome_produto}
                                        valor_de={item.valor_de ? parseFloat(item.valor_de).toFixed(2).replace('.', ',') : null}
                                        valor_produto={item.valor_produto ? parseFloat(item.valor_produto).toFixed(2).replace('.', ',') : null}
                                        valor_parcelado={item.valor_parcelado ? parseFloat(item.valor_parcelado).toFixed(2).replace('.', ',') : null}
                                        parcelas={item.parcelas}
                                    />
                                    ) : null
                                ))
                            }
                        </ListaProd>
                        {
                            this.props.isFavoritos 
                            ?
                                null
                            :
                                <div className="paginador">
                                    <div className="contador">
                                        <span>Página</span>
                                        <select>
                                            <option value="">01</option>
                                            <option value="">02</option>
                                            <option value="">03</option>
                                        </select>
                                        <span>de 24</span>
                                    </div>
                                    <Link to="#">
                                        <span>Próxima página</span>
                                        <div className="square">
                                            <img src={ArrowNextPaginador} alt=""/>
                                        </div>
                                    </Link>
                                </div>
                        }

                    </div>
                </div>
            </section>
        );
    }
}

export default ListaProdutos;