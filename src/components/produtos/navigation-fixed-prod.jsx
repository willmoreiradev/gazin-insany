import React, { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

import { StyleNavigation } from './style';

import IconeCarrinho from '../../images/icone-carrinho-white.svg';

export default function NavigationFixedProduct() {
  const [scroll, setScroll] = useState(false);

  useEffect(() => {
    function onScroll() {
      let currentPosition = window.pageYOffset;
      if (currentPosition > 640) {
        setScroll(true);
      } else {
        setScroll(false);
      }
    }

    window.addEventListener("scroll", onScroll);
    return window.addEventListener("scroll", onScroll);
  }, []);
  return (
    <StyleNavigation visible={scroll}>
      <div className="nome-valor-prod">
        <div className="container">
          <h3>iPhone 8 64GB Cinza Espacial Tela 4.7’'IOS 4G Câmera 12MP - Apple</h3>
          <div className="right">
            <div className="valor-condicao">
              <h2>R$ 3.999,00 <span>à vista no boleto </span></h2>
            </div>
            <Link className="btn" to="/detalhes-garantia">
              <img src={IconeCarrinho} alt="" />
            comprar
          </Link>
          </div>
        </div>
      </div>
    </StyleNavigation>
  )
}