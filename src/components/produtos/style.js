import styled from 'styled-components';

import { Link } from 'react-router-dom';

import rightArrowModalDetalhes from "../../images/right-arrow-modal-detalhes.svg"
import leftArrowModalDetalhes from "../../images/left-arrow-modal-detalhes.svg"


export const Cores = styled.div`
    margin-bottom: 27px;
    span {
        margin-bottom: 10px;
        display: block;
    }
    ul {
        display: flex;
        align-items: center;
        li {
            border: 1px solid transparent;
            margin-left: 16px;
            border-radius: 50%;
            overflow: hidden;
            &:first-child {
                margin-left: 0px;
            }
            &.selected {
                border: 1px solid rgba(100, 105, 129, 0.69);
                transition: all .3s;
                div {
                    border: 4px solid #ffffff;
                }
            }
        }
    }
`;

export const CorDisponivel = styled.div`
    width: 30px;
    height: 30px;
    background-color: ${props => props.color};
    border-radius: 50%;
    cursor: pointer;
    border: 1px solid transparent;
    transition: all .3s;
`;

export const SessaoAvaliacao = styled.div`
    width: 100%;
    padding-top: 54px;
    .topo {
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 42px;
    }
    .slide-avaliacoes {
        position: relative;
        overflow: hidden;
    }
    @media(max-width: 1200px) {
        .topo {
            flex-direction: column;
        }
    }
    @media(max-width: 480px) {
        padding-top: 40px;
        .topo {
            padding: 0px 15px;
        }
        .slide-avaliacoes {
            padding: 0px 15px;
        }
    }
`

export const Left = styled.div`
    display: flex;
    align-items: center;
    @media(max-width: 1200px) {
        margin-bottom: 30px;
    }
    @media(max-width: 480px) {
        flex-direction: column;
        align-items: center;
        width: 100%;
    }
`

export const Right = styled.div`
    display: flex;
    align-items: center;
    flex: 1 1 auto;
    justify-content: flex-end;
    button {
        width: 203px;
        height: 40px;
        background-color: #5DA8FF;
        border-radius: 6px;
        font: normal 500 14px/17px 'Inter';
        letter-spacing: -0.1px;
        color: #FFFFFF;
        transition: all .3s;
        &:hover {
            background-color: #3490ff;
            transition: all .3s;
        }
    }
    select {
        max-width: 186px;
        margin-left: 48px;
    }
    @media(max-width: 480px) {
        flex-direction: column;
        align-items: center;
        width: 100%;
        button {
            width: 100%;
            height: 55px;
            margin-bottom: 10px;
            font-size: 16px;
        }
        select {
            max-width: 100%;
            margin: 0px;
            height: 55px;
        }
    }
`

export const AvaliacaoGeral = styled.div`
    display: flex;
    align-items: center;
    margin-right: 96px;
    h3 {
        margin-right: 96px;
        font: normal 600 24px/36px 'Inter';
        color: #363843;
    }
    ul {
        display: flex;
        li {
            margin-left: 10px;
            &:first-child {
                margin-left: 0px;
            }
            span {
                font-size: 26px;
                color: #FFA82E;
            }
        }
    }
    @media(max-width: 480px) {
        margin-right: 0;
        margin-bottom: 20px;
        width: 100%;
        justify-content: space-between;
        flex-direction: column;
        h3 {
            margin-right: 0px;
        }
    }
`

export const Nota = styled.div`
    display: flex;
    align-items: center;
    h3 {
        margin-right: 12px;
        font: normal 600 24px/36px 'Inter';
        color: #646981;
    }
    a {
        font-size: 16px;
        line-height: 19px;
        color: #646981;
    }

`

export const BoxProduto = styled(Link)`
    position: relative;
    top: 0;
    display: block;
    width: 100%;
    max-width: 294px;
    height: auto;
    background-color: #ffffff;
    border-radius: 8px;
    transition: all .3s;
    padding: 30px 24px;
    &:hover {
        top: -10px;
        box-shadow: 4px 5px 15px rgba(20, 21, 24, 0.07);
        transition: all .3s;
    }
    @media(max-width: 1200px) {
        max-width: 100% !important;
    }
    @media(max-width: 480px) {
        padding: 20px;
        &:hover {
            top: 0px;
            box-shadow: none;
            transition: all .3s;
        }
    }
`

export const Oferta = styled.span`
    position: absolute;
    top: 24px;
    left: 24px;
    z-index: 1;
    width: 64px;
    height: 26px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #80C826;
    color: #ffffff;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: -0.3px;
    border-radius: 6px;
`

export const BotaoFavorito = styled.button`
    position: absolute;
    top: 24px;
    right: 24px;
    color: #A0A4B3;
    span {
        transition: all .3s;
        &.fa-heart {
            color: red;
            transition: all .3s;
        }
    }
`

export const QtdCores = styled.img`
    position: absolute;
    left: 24px;
    top: 60px;
`

export const ImgProduto = styled.div`
    width: 178px;
    height: 200px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    margin-bottom: 27px;
    img {
        max-width: 100%;
    }
    @media(max-width: 480px) {
        width: 120px;
    }
`

export const InfoProduto = styled.div`
    ul {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
        li {
            margin-left: 6px;
            &:first-child {
                margin-left: 0px;
            }
            span {
                color: #FFA82E;
                &.disabled {
                    color: #DCDDE3;
                }
            }
        }
    }
    h3 {
        max-width: 234px;   
        font: normal normal 15px/20px 'Inter';
        color: #363843;
        margin-bottom: 14px;
    }
    @media(max-width: 480px) {
        ul {
            margin-bottom: 10px;
        }
        h3 {
            font-size: 14px;
            line-height: 23px;
        }
        h2 {
            font-size: 22px;
            line-height: 27px;
        }
        p {
            font-size: 10px;
        }
    }
`

export const ProdutoIndisponivel = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    span {
        width: 100%;
        line-height: 39px;
        background-color: #EBEBF0;
        border-radius: 5px;
        display: block;
        text-align: center;
        margin-bottom: 19px;
        font-size: 13px;
        color: #646981
    }
    a {
        display: block;
        font-size: 13px;
        line-height: 16px;
        text-align: center;
        color: #0D71F0;
    }
`

export const ValorProduto = styled.div`
    .valor-total-parcelado {
        margin-bottom: 11px;
        strong {
            font-weight: bold;
            font-size: 14px;
            line-height: 17px;
            color: #646981;
        }
        p {
            font-size: 14px;
            line-height: 17px;
            color: #646981;
            strong {
                font-weight: bold;
                font-size: 14px;
                line-height: 17px;
                color: #FF9D2E;
            }   
        }
    }
    .valor-vista {
        line-height: 1;
        h2 {
            line-height: 1;
            font-weight: bold;
            font-size: 24px;
            color: #0D71F0;
            margin-bottom: 0px;
        }
        span {
            font-size: 14px;
            line-height: 17px;
            color: #646981;
            font-weight: 400;
        }
    }
    @media(max-width: 1200px) {
        .valor-vista {
            h2 {
                font-size: 20px;
            }
        }
    }
`

export const Content = styled.div`
    h4 {
        font: normal 600 18px/37px 'Inter';
        color: #646981;
        margin-bottom: 19px;
    }
    @media(max-width: 1200px) {
        width: 100%;
        h4 {
            line-height: 1;
            text-align: center;
        }
    }
`;

export const Opcoes = styled.div`
    display: flex;
    align-items: flex-start;
    @media(max-width: 1200px) {
        display: grid;
        grid-template-columns: 220px 1fr;
        align-items: flex-start;
    }
    @media(max-width: 480px) {
        grid-template-columns: 1fr;
        width: 100%;
    }
`;

export const Ficha = styled.div`
    width: 100%;
    border-top: 1px solid #F0F0F0;
    border-bottom: 1px solid #F0F0F0;
    margin-top: 49px;
    padding: 49px 0px;
    h3 {
        font-weight: bold;
        font-size: 24px;
        line-height: 37px;
        color: #363843;
        margin-bottom: 40px;
    }
    .dados {
        .item {
            display: flex;
            justify-content: space-between;
            &:last-child {
                margin-top: 32px;
            }
            .box-title {
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                background: #F2F8FF;
                border-radius: 6px;
                width: 296px;
                img {
                    max-width: 58px;
                    margin-bottom: 17px;
                }
                h4 {
                    font-weight: 600;
                    font-size: 16px;
                    line-height: 19px;
                    text-align: center;
                    color: #094E99;
                }
            }
            table {
                width: 952px;
                tr {
                    border-radius: 6px;
                    height: 48px;
                    margin: 0;
                    &:nth-child(odd) {
                        background: rgba(93, 168, 255, 0.08);
                    }
                    td {
                        border: none;
                        max-width: 596px;
                        padding: 12px 27px 12px 32px;
                        font-size: 15px;
                        line-height: 18px;
                        color: #6E6E6E;
                        &:first-child {
                            border-radius: 8px 0px 0px 8px;
                            font-weight: 600;
                            width: 261px;
                        }
                        &:last-child {
                            border-radius: 0px 8px 8px 0px;
                        }
                    }
                }
            }
        }
    }
    button {
        font-weight: 600;
        font-size: 15px;
        line-height: 18px;
        color: #0D71F0;
        background-color: transparent;
        margin-top: 32px;
    }
    @media(max-width: 1200px) {
        h3 {
            text-align: center;
        }
        button {
            margin: 0 auto;
            display: block;
            margin-top: 50px;
        }
    }
    @media(max-width: 480px) {
        margin-top: 40px;
        padding: 40px 0px;
        h3 {
            font-size: 22px;
            margin-bottom: 20px;
        }
        .dados {
            .item {
                flex-direction: column;
                align-items: center;
                .box-title {
                    width: 100%;
                    height: auto;
                    padding: 20px 0px;
                    margin-bottom: 20px;
                    flex-direction: row;
                    align-items: center;
                    img {
                        margin-bottom: 0;
                        margin-right: 24px;
                        max-width: 49px;
                    }
                    h4 {
                        text-align: left;
                    }
                }
                table {
                    width: 100%;
                    tr {
                        td {
                            padding: 10px 15px;
                            font-size: 13px;
                            &:first-child {
                                width: 147px;
                            }
                        }
                    }
                }
            }
        }
    }
`

export const Btns = styled.div`
  position: absolute;
  top: 17px;
  right: 23px;
  display: flex;
  flex-direction: column;
`;

export const ButtonAcaoProduto = styled.button`
    width: 39px;
    height: 39px;
    background: #FFFFFF;
    box-shadow: 1.07955px 2.69886px 6.47727px rgba(22, 24, 41, 0.13);
    border-radius: 50%;
    margin-bottom: 16px;
    &:last-child {
        margin-bottom: 0px;
    }
    span {
        font-size: 18px;
        color: #A0A4B3;
    }
`;

export const EspacoFoto = styled.div`
    cursor: pointer;
    width: 428px;
    height: 428px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    img {
        max-width: 100%;
    }
    @media(max-width: 480px) {
        width: 100%;
        height: auto;
    }
`;

export const FotoModal = styled.div`
    width: 472px;
    height: 472px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    img {
        max-width: 100%;
    }
    @media(max-width: 480px) {
        width: 100%;
        height: auto;
    }
`;

export const FotoThumb = styled.div`
    width: 77px !important;
    height: 77px;
    box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.1);
    padding: 10px;
    border-radius: 6px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    img{
        width: 100%
    }
    @media (max-width: 480px) {
        display: flex !important;
        img{
            width: 100% !important;
        }
    }
`

export const ThumbContainer = styled.div`
    margin-top: 11px;
    .slick-list{
        background-color: transparent !important;
        padding: 1rem 0 !important;
        width: 100% !important;
        height: auto !important;
        box-shadow: none !important;
        border-radius: 0 !important;
    }
    .slick-slide{
        img{
            width: 100% !important;
        }
    }
`

export const ModalContainer = styled.div`
    opacity: 0;
    pointer-events: none;
    transition: all .3s;
    position: fixed;
    left: 0;
    top: 0;
    width: 100vw;
    height: 100vh;
    z-index: 100;
    &.open{
        opacity: 1;
        pointer-events: all;
        transition: all .3s;
    }
    .overlay{
        width: 100%;
        height: 100%;
        pointer-events: none;
        z-index: 100;
        background-color: rgba(0,0,0, 0.85);
    }
`;

export const ModalSlider = styled.div`
    position: absolute;
    top: 50%;
    margin-top: -307px;
    left: 50%;
    margin-left: -484px;
    width: 968px;
    height: 600px;
    z-index: 101;
    display: flex;
    align-items: center;
    justify-content: center;
    .close{
        cursor: pointer;
        position: absolute;
        right: 85px;
        top: -15px;
        z-index: 99;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
        background: #0D71F0;
        border-radius: 50%;
    }
    .actions{
        position: absolute;
        right: 92px;
        top: 38px;
        z-index: 99;
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
        flex-direction: column;
        li{
            width: 20px;
            height: 20px;
            cursor: pointer;
            margin-bottom: 24px;
            display: flex;
            align-items: center;
            justify-content: center;
            &:last-child{
                margin-bottom: 0;
            }
        }
    }
    .slick-slider{
        display: flex;
        align-items: center;
        justify-content: center;
        .slick-list{
            width: 100% !important;
            height: auto !important;
            box-shadow: none !important;
        }
        .slick-next{
            right: -67px;
            width: 42px;
            height: 42px;
            background-color: #ffffff;
            border-radius: 50%;
            &:before{
                content: "";
                position: absolute;
                left: 1px;
                top: 0;
                background-image: url(${rightArrowModalDetalhes});
                background-position: center;
                background-repeat: no-repeat;
                width: 42px;
                height: 42px;
                border-radius: 50%;
            }
            display: block !important;
        }
        .slick-prev {
            left: -67px;
            width: 42px;
            height: 42px;
            background-color: #ffffff;
            border-radius: 50%;
            &:before{
                content: "";
                position: absolute;
                left: -1px;
                top: 0;
                background-image: url(${leftArrowModalDetalhes});
                background-position: center;
                background-repeat: no-repeat;
                width: 42px;
                height: 42px;
                border-radius: 50%;
            }
            display: block !important;
        }
        width: 836px;
        height: 600px;
        background: #FFFFFF;
        border-radius: 8px;
    }
    @media (max-width: 768px) {
        top: 50%;
        margin-top: -70%;
        left: 50%;
        margin-left: -40%;
        width: 80%;
        height: 70%;
        .close{
            right: 15px;
        }
        .actions{
            right: 22px;
        }
        .slick-slider{
            width: 100%;
            height: 100%;
            .slick-next{
                top: unset;
                bottom: -80px;
                right: 30%;
            }
            .slick-prev {
                top: unset;
                bottom: -80px;
                left: 30%;
            }
        }
    }
`;

export const BoxRetirarLoja = styled.div`
    width: 100%;
    background: #F6F6F6;
    border-radius: 6px;
    display: flex;
    align-items: center;
    padding: 15px 0px;
    padding-left: 22px;
    margin-bottom: 29px;
    .icone {
        width: 52px;
        height: 52px;
        background: linear-gradient(89.75deg, #5C33FF 3.91%, #3399FF 98.18%);
        box-shadow: 2px 5px 12px rgba(22, 24, 41, 0.13);
        border-radius: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-right: 17px;
    }
    .info {
        p {
            margin: 0 !important;
        }
        h4 {
            font-weight: bold;
            font-size: 20px;
            line-height: 37px;
            color: #2A89FF;
        }
        p {
            font-size: 14px;
            line-height: 24px;
            color: #646981;
        }
    }
    @media(max-width: 1200px) {
        justify-content: center;
    }
    @media(max-width: 480px) {
        padding: 20px;
        height: auto;
        flex-direction: column;
        align-items: center;
        .icone {
            margin-right: 0;
            margin-bottom: 10px;
        }
        .info {
            h4 {
                text-align: center;
            }
            p {
                text-align: center;
                line-height: 22px;
            }
        }
    }
`;

export const VoltagemProduto = styled.div`
    margin-bottom: 35px;
    div {
        display: flex;
        align-items: center;
        margin-top: 13px;
    }
    span {
    font-size: 16px;
    line-height: 23px;
    color: #646981;
    strong {
        color: #0D71F0;
    }
    @media(max-width: 480px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        margin-bottom: 20px;
    }
}
`;

export const BotaoVoltagem = styled.button`
    width: 96px;
    height: 47px;
    border: ${props => props.active ?  '2px solid #0D71F0' : '1px solid #DCDDE3'} ;
    border-radius: 2px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: 14px;
    font-size: 16px;
    color: ${props => props.active ?  '#0D71F0' : '#A0A4B3'};
    background-color: transparent;
    &:first-child {
        margin-left: 0px;
    }
`;

export const ListaProd = styled.div`
    grid-template-columns: ${props => props.direction === 'col' ? 'repeat(3, 1fr)' : 'repeat(1, 1fr) !important'} ;
    ${BoxProduto}{
        display: ${props => props.direction === 'row' ? 'flex' : 'block'};
        max-width: ${props => props.direction === 'row' ? '100%' : '294px'};
        height: auto;
        align-items: center;
        justify-content: flex-start;
        ${ImgProduto}{
            justify-content: ${props => props.direction === 'row' ? 'flex-start' : 'none'};
            width: ${props => props.direction === 'row' ? '200px' : '178px'};
            margin: ${props => props.direction === 'row' ? '0' : '0 auto'};
            padding-left: ${props => props.direction === 'row' ? '35px' : '0'};
        }
        ${InfoProduto}{
            padding: ${props => props.direction === 'row' ? '0px 24px' : '0'} ;
            width: ${props => props.direction === 'row' ? '40%' : 'auto'} ;
            display: ${props => props.direction === 'row' ? 'flex' : 'block'};
            flex-direction: ${props => props.direction === 'row' ? 'column' : 'auto'};
            ul{
                order: 2;
            }
            h3{
                order: 1;
            }
        }
        ${ValorProduto }{
            width: ${props => props.direction === 'row' ? '40%' : 'auto'} ;
        }
        ${BotaoFavorito}{
            top: ${props => props.direction === 'row' ? '50%' : '24px'} ;
            margin-top: ${props => props.direction === 'row' ? '-12px' : '0'} ;
        }
    }
    @media(max-width: 1200px) {
        ${BoxProduto} {
            ${ImgProduto} {
                width: 100%;
                margin-bottom: 19px;
            }
        }
    }
`

export const StyleNavigation = styled.div`
    position: fixed;
    top: 114px;
    z-index: 1000;
    background-color: #fff;
    width: 100%;
    height: 60px;
    box-shadow: 0px 6px 15px rgba(67, 76, 108, 0.08);
    opacity: ${props => props.visible ? '1' : '0'};
    pointer-events: ${props => props.visible ? 'all' : 'none'};
    transform: ${props => props.visible ? 'translateY(0px);' : 'translateY(-50px);'};
    transition: all .3s;
    @media(max-width: 1200px) {
        display: none;
    }
    .container {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .nome-valor-prod {
        height: 60px;
        display: flex;
        align-items: center;
        h3 {
            margin-bottom: 0px;
            font-weight: 600;
            font-size: 18px;
            line-height: 36px;
            color: #363843;
        }
        .right {
            display: flex;
            align-items: center;
            .valor-condicao {
                margin-right: 33px;
                h2 {
                    margin-bottom: 0px;
                    font-weight: bold;
                    font-size: 24px;
                    line-height: 29px;
                    color: #0D71F0;
                    span {
                        font-size: 18px;
                        line-height: 22px;
                        color: #646981;
                        font-weight: 400;
                    }
                }
            }
            .btn {
                display: flex;
                align-items: center;
                justify-content: center;
                width: 295px;
                height: 40px;
                background: #6BB70B;
                border-radius: 6px;
                font-weight: 600;
                font-size: 15px;
                letter-spacing: -0.107143px;
                text-transform: uppercase;
                color: #FFFFFF;
                transition: all .3s;
                img {
                    margin-right: 10px;
                }
                &:hover {
                    background-color: #60a00e;
                    transition: all .3s;
                }
            }
        }
    }
`;

export const StyleFiltroProdutosMobile = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #fff;
    z-index: 2001;
    transform: ${props => props.visibleFilter ? 'translateY(0%)' : 'translateY(100%)'};
    pointer-events: ${props => props.visibleFilter ? 'all' : 'none'};
    transition: all .4s;
    .topo {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        background-color: #0D71F0;
        width: 100%;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0px 16px;
        .left {
            display: flex;
            align-items: center;
            button {
                margin-right: 20px;
            }
            h3 {
                font-weight: 600;
                font-size: 16px;
                color: #FFFFFF;
                margin-bottom: 0px;
            }
        }
        .btn-clear {
            font-size: 12px;
            line-height: 15px;
            color: #FFFFFF;
        }
    }
    .cont-filter {
        margin-top: 50px;
        height: calc(100% - 128px);
        overflow-y: auto;
        padding: 20px 16px;
        padding-bottom: 50px;
        .item-filter {
            margin-bottom: 20px;
            &:last-child {
                margin-bottom: 0px;
            }
            h3 {
                font-weight: bold;
                font-size: 14px;
                line-height: 17px;
                color: #363843;
                margin-bottom: 16px;
            }
            .nota {
                .fa-star {
                    font-size: 17px;
                    color: #DCDDE3;
                    &.feat {
                        color: #FF9D2E;
                    }
                }
            }
            ul {
                li {
                    display: flex;
                    align-items: center;
                    margin-bottom: 20px;
                    &:last-child {
                        margin-bottom: 0px;
                    }
                    span {
                        font-weight: 500;
                        font-size: 14px;
                        line-height: 17px;
                        color: #A0A4B3;
                        margin-left: 8px;
                    }
                }
            }
        }
    }
    .area-btn {
        position: fixed;
        bottom: 0;
        left: 0;
        width: 100%;
        background: #FFFFFF;
        box-shadow: 4px -5px 15px rgba(20, 21, 24, 0.07);
        padding: 16px;
        button {
            width: 100%;
            height: 46px;
            background-color: #0D71F0;
            border-radius: 6px;
            color: #FFFFFF;
            font-weight: 600;
            font-size: 14px;
            text-transform: uppercase;
        }
    }
`;