import React, { Component } from 'react';

import { VoltagemProduto, BotaoVoltagem  } from './style.js';

class ComponentVoltagem extends Component {
    constructor() {
        super();
        this.state = {
            volts: "-",
            active1: false,
            active2: false
        }
    }
    HandleVoltagem110 = (e) => {
        this.setState({ volts: "110v" })
        this.setState({ active1: true })
        this.setState({ active2: false })
    }
    HandleVoltagem220 = (e) => {
        this.setState({ volts: "220v" })
        this.setState({ active2: true })
        this.setState({ active1: false })
    }
    render() { 
        return ( 
            <VoltagemProduto>
                <span>Voltagem: <strong>{this.state.volts} </strong></span>

                <div>
                    <BotaoVoltagem active={this.state.active1} onClick={this.HandleVoltagem110}>110v</BotaoVoltagem>
                    <BotaoVoltagem active={this.state.active2} onClick={this.HandleVoltagem220}>220v</BotaoVoltagem>
                </div>
            </VoltagemProduto>
        );
    }
}
 
export default ComponentVoltagem;