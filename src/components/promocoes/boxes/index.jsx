import React, { Component } from 'react';

import Title from '../../title-section';

import Promo_01 from '../../../images/promocao-banner-03.jpg';
import Promo_02 from '../../../images/promo-01.jpg';
import Promo_03 from '../../../images/promo-02.jpg';

class ComponentPromocoesBoxes extends Component {
    render() {
        return (
            <section className="s-promocoes-boxes">
                <div className="container">
                    <div className="title">
                        <h2>{this.props.titulo}</h2>
                        <p>{this.props.subtitulo}</p>
                    </div>
                    <div className="responsive">
                        <div className="all">
                            <div className="box" style={{
                                backgroundImage: `url(${Promo_01})`,
                                backgroundPosition: 'center',
                                backgroundSize: 'cover',
                                backgroundRepeat: 'no-repeat'
                            }}>
                                <a href=""></a>
                            </div>
                            <div className="box" style={{
                                backgroundImage: `url(${Promo_02})`,
                                backgroundPosition: 'center',
                                backgroundSize: 'cover',
                                backgroundRepeat: 'no-repeat'
                            }} >
                                <a href=""></a>
                            </div>
                            <div className="box" style={{
                                backgroundImage: `url(${Promo_03})`,
                                backgroundPosition: 'center',
                                backgroundSize: 'cover',
                                backgroundRepeat: 'no-repeat'
                            }} >
                                <a href=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ComponentPromocoesBoxes;