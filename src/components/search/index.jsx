import React, { Component } from 'react';

//Images

import IconeLupa from '../../images/icone-lupa.svg'

class ComponentSearch extends Component {
    render() { 
        return ( 
            <div className="search">
                <input type="text" placeholder="O que você procura"/>
                <button type="button">
                    <img src={IconeLupa} alt=""/>
                </button>
            </div>
        );
    }
}
 
export default ComponentSearch;