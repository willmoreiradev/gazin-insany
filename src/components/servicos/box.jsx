import React, { Component } from 'react';

class ComponentBoxServicos extends Component {
    render() { 
        return ( 
            <a href="" className="box-servicos">
                <div className="foto">
                    <img src={this.props.img_servico} alt=""/>
                </div>
                <div className="info">
                    <div className="icone">
                        <img src={this.props.icone} alt=""/>
                    </div>
                    <div className="text">
                        <h3>{this.props.nome_servico}</h3>
                        <p>{this.props.desc_servico}</p>
                    </div>
                </div>
            </a>
        );
    }
}
 
export default ComponentBoxServicos;