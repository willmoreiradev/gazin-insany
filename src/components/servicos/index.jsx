import React, { Component } from 'react';

import BoxServico from './box';

import Casamento from '../../images/casamento.jpg';
import Consorcio from '../../images/consorcio.jpg';
import Seguros from '../../images/img-seguros.jpg';
import Creditos from '../../images/credito.jpg';
import Atacado from '../../images/atacado.jpg';

import IconeCasamento from '../../images/icone-casamento.svg';
import IconeConsorcio from '../../images/icone-consorcio.svg';
import IconeSeguros from '../../images/icon-seguros.svg';
import IconeCredito from '../../images/icone-credito.svg';
import IconeAtacado from '../../images/icone-atacado.svg';

class SessaoServicos extends Component {
    render() {
        return (
            <section className="s-servicos">
                <div className="txt-mobile">
                    <h2>Conheça as lojas parceiras </h2>
                    <p>Juntos somos mais, conheça as lojas parceiras Gazin</p>
                </div>
                <div className="responsive">
                    <div className="container">
                        <div className="texto">
                            <h2>Gazin tem muito mais</h2>
                            <p>Nós queremos estar com você em todos os momentos especiais da sua vida. Conheça nossos outros serviços.</p>
                            <BoxServico
                                img_servico={Casamento}
                                icone={IconeCasamento}
                                nome_servico="Lista de Casamento"
                                desc_servico="Lista interativa de presentes para ocasiões mais que especiais."
                            />
                        </div>
                        <BoxServico
                            img_servico={Consorcio}
                            icone={IconeConsorcio}
                            nome_servico="Consórcios"
                            desc_servico="Trabalhando para que grandes sonhos se tornem realidade."
                        />
                        <BoxServico
                            img_servico={Seguros}
                            icone={IconeSeguros}
                            nome_servico="Seguros"
                            desc_servico="Uma seguradora voltada para demandas  de móveis e eletrodomésticos."
                        />
                        <BoxServico
                            img_servico={Creditos}
                            icone={IconeCredito}
                            nome_servico="Crédito"
                            desc_servico="Créditos concedidos através de produtos financeiros"
                        />
                        <BoxServico
                            img_servico={Atacado}
                            icone={IconeAtacado}
                            nome_servico="Atacado"
                            desc_servico="Produtos em atacado com descontos inacreditáveis."
                        />
                    </div>
                </div>
            </section>
        )
    }
}

export default SessaoServicos;