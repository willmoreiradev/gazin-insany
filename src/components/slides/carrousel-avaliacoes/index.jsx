import React, { useState } from 'react';
import Swiper from 'react-id-swiper';
import BoxAvaliacao from '../../avaliacao/box';
import styled from 'styled-components';

import ArrowSlideProduto from '../../../images/arrow-slide-produtos.svg';

const ContentSlider = styled.div`
    position: relative;
    .swiper-pagination {
        position: relative;
        bottom: 0px;
        margin-top: 42px;
        .swiper-pagination-bullet {
            background-color: #A0A4B3;
            opacity: 0.3;
            transition: all .3s;
            &.swiper-pagination-bullet-active {
                background-color: #1B4E8D;
                opacity: 1;
                transition: all .3s;
            }
        }
    }
`;

const Button = styled.button`
    position: absolute;
    left: -58px;
    top: 150px;
    transform: rotate(180deg);
    z-index: 4;
    cursor: pointer;
    &:last-child {
        transform: rotate(0deg);
        right: -58px;
        left: initial;
    }
    @media(max-width: 1200px) {
        display: none;
    }
`;

const CarrouselAvaliacoes = () => {

    const [swiper, updateSwiper] = useState(null);

    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
        }
    };

    const params = {
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 32,
        speed: 800,
        containerClass: 'slide-avaliacoes',
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 1.8,
                slidesPerGroup: 1.5,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 3,
                slidesPerGroup: 3,
            }
        }
    }

    return (
        <ContentSlider>
            <Swiper {...params} getSwiper={updateSwiper}>
                <div>
                    <BoxAvaliacao
                        titulo_avaliacao="Achei incrível"
                        descricao_avaliacao="Design maravilhoso, câmera muito melhor que a do 7, carregamento rápido e sem fio tb, bateria apesar de pequena rende bastante. só faltou vir uma base de tomada decente, pq essa eh bem fraca, fora isso 5 estrelas"
                        nome_cliente="José da Silva"
                        dia_avaliacao={8}
                    />
                </div>
                <div>
                    <BoxAvaliacao
                        titulo_avaliacao="Achei incrível"
                        descricao_avaliacao="Design maravilhoso, câmera muito melhor que a do 7, carregamento rápido e sem fio tb, bateria apesar de pequena rende bastante. só faltou vir uma base de tomada decente, pq essa eh bem fraca, fora isso 5 estrelas"
                        nome_cliente="José da Silva"
                        dia_avaliacao={8}
                    />
                </div>
                <div>
                    <BoxAvaliacao
                        titulo_avaliacao="Achei incrível"
                        descricao_avaliacao="Design maravilhoso, câmera muito melhor que a do 7, carregamento rápido e sem fio tb, bateria apesar de pequena rende bastante. só faltou vir uma base de tomada decente, pq essa eh bem fraca, fora isso 5 estrelas"
                        nome_cliente="José da Silva"
                        dia_avaliacao={8}
                    />
                </div>
                <div>
                    <BoxAvaliacao
                        titulo_avaliacao="Achei incrível"
                        descricao_avaliacao="Design maravilhoso, câmera muito melhor que a do 7, carregamento rápido e sem fio tb, bateria apesar de pequena rende bastante. só faltou vir uma base de tomada decente, pq essa eh bem fraca, fora isso 5 estrelas"
                        nome_cliente="José da Silva"
                        dia_avaliacao={8}
                    />
                </div>
                <div><BoxAvaliacao
                    titulo_avaliacao="Achei incrível"
                    descricao_avaliacao="Design maravilhoso, câmera muito melhor que a do 7, carregamento rápido e sem fio tb, bateria apesar de pequena rende bastante. só faltou vir uma base de tomada decente, pq essa eh bem fraca, fora isso 5 estrelas"
                    nome_cliente="José da Silva"
                    dia_avaliacao={8}
                /></div>
            </Swiper>
            <Button onClick={goPrev}><img src={ArrowSlideProduto} alt="" /></Button>
            <Button onClick={goNext}><img src={ArrowSlideProduto} alt="" /></Button>
        </ContentSlider>
    )
}

export default CarrouselAvaliacoes;
