import React, { useState } from 'react';
import Swiper from 'react-id-swiper';

import ArrowSlideProduto from '../../../images/arrow-slide-produtos.svg';

import BoxLoja from '../../lojas/box';

import ImgEletroluz from '../../../images/img-eletroluz.jpg';
import ImgLg from '../../../images/img-lg.jpg';
import ImgEpson from '../../../images/img-epson.jpg';
import ImgMotorola from '../../../images/img-motorola.jpg';
import ImgAOC from '../../../images/img-aoc.jpg';
import ImgAcer from '../../../images/img-acer.jpg';


import LogoEletroluz from '../../../images/logo-eletroluz.jpg';
import LogoLg from '../../../images/logo-lg.jpg';
import LogoEpson from '../../../images/logo-epson.jpg';
import LogoMotorola from '../../../images/logo-motorola.jpg';
import LogoAoc from '../../../images/logo-aoc.jpg';
import LogoAcer from '../../../images/logo-acer.jpg';

const CarrouselLojas = () => {

    const [swiper, updateSwiper] = useState(null);

    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
        }
    };

    const params = {
        slidesPerView: 6,
        slidesPerGroup: 6,
        spaceBetween: 32,
        speed: 800,
        containerClass: 'slide-lojas',
        breakpoints: {
            320: {
                slidesPerView: 2.3,
                slidesPerGroup: 2.3,
                spaceBetween: 22,
            },
            768: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 6,
                spaceBetween: 32,
            }
        }
    }

    return (
        <div className="area-slide">
            <Swiper {...params} getSwiper={updateSwiper}>
                <div>
                    <BoxLoja
                        img_loja={ImgEletroluz}
                        logo={LogoEletroluz}
                        nome_loja="Electrolux"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgLg}
                        logo={LogoLg}
                        nome_loja="LG"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgEpson}
                        logo={LogoEpson}
                        nome_loja="Epson"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgMotorola}
                        logo={LogoMotorola}
                        nome_loja="Motorola"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgAOC}
                        logo={LogoAoc}
                        nome_loja="AOC"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgAcer}
                        logo={LogoAcer}
                        nome_loja="Acer"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgEletroluz}
                        logo={LogoEletroluz}
                        nome_loja="Electrolux"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgLg}
                        logo={LogoLg}
                        nome_loja="LG"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgEpson}
                        logo={LogoEpson}
                        nome_loja="Epson"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgMotorola}
                        logo={LogoMotorola}
                        nome_loja="Motorola"
                    />
                </div>
                <div>
                    <BoxLoja
                        img_loja={ImgAOC}
                        logo={LogoAoc}
                        nome_loja="AOC"
                    />
                </div>
            </Swiper>
            <button onClick={goPrev} className="btn btn-prev"><img src={ArrowSlideProduto} alt="" /></button>
            <button onClick={goNext} className="btn btn-next"><img src={ArrowSlideProduto} alt="" /></button>
        </div>
    )
}

export default CarrouselLojas;
