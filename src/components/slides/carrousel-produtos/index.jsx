import React, { useState } from 'react';
import Swiper from 'react-id-swiper';

import BoxProduto from '../../produtos/box-produto';
import ImgProduto01 from '../../../images/produto-01.jpg';
import ImgProduto02 from '../../../images/produto-02.jpg';
import ImgProduto03 from '../../../images/produto-03.jpg';
import ImgProduto04 from '../../../images/produto-04.jpg';

import ArrowSlideProduto from '../../../images/arrow-slide-produtos.svg';

const CarrouselProdutos = () => {
    const [swiper, updateSwiper] = useState(null);
 
    const goNext = () => {
      if (swiper !== null) {
        swiper.slideNext();
      }
    };
   
    const goPrev = () => {
      if (swiper !== null) {
        swiper.slidePrev();
      }
    };

    const params = {
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        // autoplay: {
        //     delay: 5000
        // },
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 30,
        speed: 800,
        containerClass: 'carrousel-produtos',
        breakpoints: {
            320: {
                slidesPerView: 1.2,
                slidesPerGroup: 1.2,
                spaceBetween: 18,
            },
            768: {
                slidesPerView: 2.5,
                slidesPerGroup: 2,
            },
            1200: {
                slidesPerView: 4,
                slidesPerGroup: 4,
            }
        }
    }

    return (
        <div className="area-slide">
            <Swiper {...params} getSwiper={updateSwiper}>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto01}
                        nome_produto="Smartphone Apple Iphone 6S 32GB"
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto02}
                        nome_produto="Monitor Gamer 23'' 1 ms 75Hz Ultra Fino SA0 Series..."
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto03}
                        nome_produto="Cadeira Charles Eames Wood Base Madeira - Design..."
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto04}
                        nome_produto="Micro-ondas Consul Espelhado - 20L - Cinza 127V - Cm020"
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto01}
                        nome_produto="Smartphone Apple Iphone 6S 32GB"
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto02}
                        nome_produto="Monitor Gamer 23'' 1 ms 75Hz Ultra Fino SA0 Series..."
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto03}
                        nome_produto="Cadeira Charles Eames Wood Base Madeira - Design..."
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
                <div>
                    <BoxProduto
                        porcetagem_oferta="30%"
                        caminho_img={ImgProduto04}
                        nome_produto="Micro-ondas Consul Espelhado - 20L - Cinza 127V - Cm020"
                        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
                        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
                        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
                    />
                </div>
            </Swiper>
            <button onClick={goPrev} className="btn btn-prev"><img src={ArrowSlideProduto} alt=""/></button>
            <button onClick={goNext} className="btn btn-next"><img src={ArrowSlideProduto} alt=""/></button>
        </div>
    )
}

export default CarrouselProdutos;
