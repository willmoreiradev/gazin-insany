import React, { useState } from 'react';
import Swiper from 'react-id-swiper';

import ArrowSlideProduto from '../../../images/arrow-slide-produtos.svg';

import BoxDepartamento from '../../departamento/box-departamento';

import ImgDepCasaBanho from '../../../images/casa-banho.jpg';
import ImgDepDecoracao from '../../../images/img-decoracao.jpg';
import ImgDepTelefonia from '../../../images/telefonia.jpg';
import ImgDepInformatica from '../../../images/informatica.jpg';
import ImgDepCasaSeguranca from '../../../images/casa-seguranca.jpg';
import ImgDepBelezaSaude from '../../../images/beleza-saude.jpg';

const CarrouselDepartamentos = () => {

    const [swiper, updateSwiper] = useState(null);

    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
        }
    };

    const params = {
        slidesPerView: 6,
        spaceBetween: 35,
        speed: 800,
        containerClass: 'slide-departamentos',
        breakpoints: {
            320: {
                slidesPerView: 1.6,
                spaceBetween: 33,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 6,
                spaceBetween: 35,
            }
        }
    }

    return (
        <div className="area-slide">
            <Swiper {...params} getSwiper={updateSwiper}>
                <div>
                    <BoxDepartamento
                        nome_departamento="Casa e banho"
                        img_departamento={ImgDepCasaBanho}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Decoração"
                        img_departamento={ImgDepDecoracao}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Telefonia"
                        img_departamento={ImgDepTelefonia}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Informática"
                        img_departamento={ImgDepInformatica}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Casa e segurança"
                        img_departamento={ImgDepCasaSeguranca}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Beleza e saude"
                        img_departamento={ImgDepBelezaSaude}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Telefonia"
                        img_departamento={ImgDepTelefonia}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Informática"
                        img_departamento={ImgDepInformatica}
                    />
                </div>
                <div>
                    <BoxDepartamento
                        nome_departamento="Casa e segurança"
                        img_departamento={ImgDepCasaSeguranca}
                    />
                </div>
            </Swiper>
            <button onClick={goPrev} className="btn btn-prev"><img src={ArrowSlideProduto} alt="" /></button>
            <button onClick={goNext} className="btn btn-next"><img src={ArrowSlideProduto} alt="" /></button>
        </div>
    )
}

export default CarrouselDepartamentos;
