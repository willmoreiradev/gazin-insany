import React, { Component } from 'react';
import Swiper from 'react-id-swiper';

import { Link } from 'react-router-dom';

import '../swiper.min.css';

//Images

import ArrowPrev from '../../../images/arrow-prev-slide.svg';
import ArrowNext from '../../../images/arrow-next-slide.svg';

class slideFullBanner extends Component {
    render() { 
        const params = {
            pagination: {
                el: '.s-banner .ctrl-slide .swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            speed: 800,
            navigation: {
                nextEl: '#btn-next-banner',
                prevEl: '#btn-prev-banner'
            }
        }
        return ( 
            <section className="s-banner">
                <img src={ArrowPrev} className="btn" id="btn-prev-banner" alt=""/>
                <img src={ArrowNext} className="btn" id="btn-next-banner" alt=""/>
                <Swiper {...params} containerClass="slide-fullbanner">
                    <div className="slide-01">
                        <Link to="/detalhes-produto"/>
                    </div>
                    <div className="slide-01">
                        <Link to="/detalhes-produto"/>
                    </div>
                    <div className="slide-01">
                        <Link to="/detalhes-produto"/>
                    </div>
                </Swiper>
                <div className="ctrl-slide">
                    <div className="container">
                        <div className="swiper-pagination"></div>
                    </div>
                </div>
            </section>
        );
    }
}
 
export default slideFullBanner;
 