import React, { Component } from 'react'

import styled from 'styled-components'

import Facebook from '../../images/facebook.svg'
import Google from '../../images/googleLogo.svg'

const SocialLoginContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    span{
        font-family: 'Inter';
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        color: #646981;
        margin-bottom: 22px;
    }
    .btnContainer{
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-direction: row;
        margin-bottom: 37px;
    }
    .cadastrar{
        span{
            font-family: 'Inter';
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 17px;
            color: #363843;
        }
        a{
            font-weight: bold;
            cursor: pointer;
            color: #0D71F0;
        }
    }
    @media(max-width: 480px) {
        .btnContainer {
            width: 100%;
            flex-direction: column;
            margin-bottom: 20px;
        }
    }
`
const SocialLoginBtn = styled.a`
    cursor: pointer;
    width: 224px;
    height: 75px;
    background: rgba(220, 221, 227,0.3);
    border-radius: 6px;
    display: flex;
    align-items: center;
    flex-direction: row;
    transform: scale(1);
    transition: all .3s;
    padding-left: 18px;
    &:hover{
        transform: scale(1.05);
        transition: all .3s;
    }

    &:first-child{
        margin-right: 19px;
    }
    
    div{
        
        width: 46px;
        height: 46px;
        border-radius: 6px;
        background-color: #ffffff;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    span{
        padding-left: 21px;
        text-align: left;
        margin-left: 21px;
        width: 118px;
        font-family: 'Inter';
        font-style: normal;
        font-weight: normal;
        line-height: 17px;
        font-size: 14px;
        color: #363843;
        margin: 0;
    }
    @media(max-width: 480px) {
        width: 100%;
        margin-bottom: 20px;
        padding: 0;
        justify-content: center;
        &:last-child {
            margin-bottom: 0px;
        }
        &:first-child {
            margin-right: 0;
        }
    }
`

export default class SocialLogin extends Component {
    render() {
        return (
        <SocialLoginContainer>
            <span>
                {this.props.texto}
            </span>
            <div className="btnContainer">
                <SocialLoginBtn>
                    <div>
                        <img src={Facebook} />
                    </div>
                    <span>
                        Acessar com o <strong>Facebook</strong>
                    </span>
                </SocialLoginBtn>
                <SocialLoginBtn>
                    <div>
                        <img src={Google} />
                    </div>
                    <span>
                        Acessar com o <strong>Google</strong>
                    </span>
                </SocialLoginBtn>
            </div>
        </SocialLoginContainer>
        )
    }
}
