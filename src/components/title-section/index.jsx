import React, { Component } from 'react';

class ComponentSection extends Component {
    render() { 
        return ( 
            <div className="title">
                <h2 className="text-title">{this.props.titulo}</h2>
                <p className="text-subtitle">{this.props.subtitulo}</p>
            </div>
        );
    }
}

ComponentSection.defaultProps = {
    titulo : 'Titulo Padrão',
    subtitulo: 'Subtitulo Padrão'
}
 
export default ComponentSection;