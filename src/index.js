import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Departamentos from './Departamentos';
import DetalhesProduto from './DetalhesProduto';
import DetalhesListaFornecedores from './DetalhesListaFornecedores';
import DetalhesGarantia from './DetalhesGarantia';
import Carrinho from './Carrinho';
import Login from './Login';
import Cadastro from './Cadastro';
import CadastroEndereco from './CadastroEndereco';
import Page404 from './Page404';

import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ScrollToTop from './components/effect';

import './scss/main.scss';
import MinhaContaPage from './MinhaConta';
import Favoritos from './Favoritos';
import Checkout from './pages/checkout';
import CheckoutAntigo from './pages/checkout-antigo';
import Sucesso from './pages/checkout/sucesso';
import Erro from './pages/checkout/erro';
import SucessoCartao from './pages/checkout/cartao/sucesso';
import ErroCartao from './pages/checkout/cartao/erro';

ReactDOM.render(
    <BrowserRouter>
        <ScrollToTop /> 
        <Switch>
            <Route path="/" exact={true} component={App} />
            <Route path="/departamentos" component={Departamentos} />
            <Route path="/detalhes-produto" component={DetalhesProduto} />
            <Route path="/detalhes-lista-fornecedores" component={DetalhesListaFornecedores} />
            <Route path="/detalhes-garantia" component={DetalhesGarantia} />
            <Route path="/carrinho" component={Carrinho} />
            <Route path="/login" component={Login} />
            <Route path="/cadastro" exact={true} component={Cadastro} />
            <Route path="/cadastro/endereco" exact={true} component={CadastroEndereco} />
            <Route exact={false} path="/conta" component={MinhaContaPage} />
            <Route path="/favoritos" component={Favoritos} />
            <Route path="/checkout" exact={true} component={Checkout} />
            <Route path="/checkout-antigo" exact={true} component={CheckoutAntigo} />
            <Route path="/checkout/sucesso" exact={true} component={Sucesso} />
            <Route path="/checkout/erro" exact={true} component={Erro} />
            <Route path="/checkout/cartao/sucesso" exact={true} component={SucessoCartao} />
            <Route path="/checkout/cartao/erro" exact={true} component={ErroCartao} />
            <Route path='*' component={Page404} />
        </Switch>
    </ BrowserRouter>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
