import React, { Component } from 'react';

import styled from 'styled-components'

import {Helmet} from "react-helmet";

import Header from '../../components/header';

import Categorias from '../../components/categoria';

import {Link} from 'react-router-dom'

import InformacoesPagamentoOferta from '../../components/pagamento-oferta';

import FooterAreaPrimaria from '../../components/footer/area-primaria';
import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import Ilustra404 from '../../images/ilustra-404.svg'

const Container404 = styled.section`
    padding: 122px 0 150px 0;
    .container{
        display: flex;
        align-items: center;
        justify-content: space-between;
        .esq{
            width: 413px;
            h1{
                font-family: 'Inter';
                font-style: normal;
                font-weight: bold;
                font-size: 58px;
                line-height: 69px;
                color: #0B57B7;
                margin-bottom: 16px;
            }
            p{
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 15px;
                line-height: 24px;
                color: #646981;
                margin-bottom: 45px;
            }
            .btn-voltar{
                display: flex;
                align-items: center;
                justify-content: center;
                width: 279px;
                height: 57px;
                left: 320px;
                top: 593px;
                background: #FFA82E;
                border-radius: 8px;
                font-family: 'Inter';
                font-style: normal;
                font-weight: 600;
                font-size: 16px;
                line-height: 19px;
                text-align: center;
                letter-spacing: -0.114286px;
                text-transform: uppercase;
                color: #FFFFFF;
                transform: scale(1);
                transition: all .3s;
                &:hover{
                    transform: scale(1.05);
                    transition: all .3s;
                }
            }
        }
        .dir{
            width: 755px;
            height: 512px;
            img{
                max-width: 100%;
            }
        }
    }
    @media (max-width: 768px) {
        padding: 172px 0 50px 0;
        .container{
            flex-direction: column;
            .esq{
                width: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                h1{
                    text-align: center;
                }
                p{
                    text-align: center;
                    margin-bottom: 35px
                }
                .btn-voltar{
                    margin-bottom: 40px;
                }
            }
            .dir{
                width: 100%;
                height: auto
            }
        }
    }
`


class Pagina404 extends Component {
  render() { 
    return ( 
      <div>
        <Helmet>
            <link rel="canonical" href="http://www.gazin.com.br"/>
            <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
            <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
            <meta name="author" content="Luis Alfredo Rosar" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <meta charSet="utf-8" />
            <title>Gazin - Detalhes do Produto</title>
        </Helmet>
        <Header/>
        <Categorias/>
        <Container404>
            <div className="container">
                <div className="esq">
                    <h1>
                        Página não encontrada
                    </h1>
                    <p>
                        Não conseguimos encontrar a pagina que você procura em nossos servidores, tente novamente mais tarde
                    </p>
                    <Link to="/" className="btn-voltar">
                        Voltar para a home
                    </Link>
                </div>
                <div className="dir">
                    <div className="ilustra">
                        <img src={Ilustra404} alt=""/>
                    </div>
                </div>
            </div>

        </Container404>
        <InformacoesPagamentoOferta/>
        <footer>
          <div className="container">
            <FooterAreaPrimaria/>
            <FooterAreaSecundaria/>
          </div>
        </footer>
      </div>
    );
  }
}
 
export default Pagina404;