import React, { Component } from 'react';

import './PasswordStrengthMeter.css'

import zxcvbn from 'zxcvbn';


class PasswordStrengthMeter extends Component {
    createPasswordLabel = (result) => {
        switch (result.score) {
          case 0:
            return 'fraca';
          case 1:
            return 'fraca';
          case 2:
            return 'justa';
          case 3:
            return 'boa';
          case 4:
            return 'forte';
          default:
            return 'fraca';
        }
      }
      render() {
        const { password } = this.props;
        const testedResult = zxcvbn(password);
        return (
          <div className="password-strength-meter">
            <progress
                className={`password-strength-meter-progress strength-${this.createPasswordLabel(testedResult)}`}
                value={testedResult.score}
                max="4"
            />
            <br />
            <label
              className="password-strength-meter-label"
            >
              {password && (
                <>
                  Senha {this.createPasswordLabel(testedResult)}
                </>
              )}
            </label>
          </div>
        );
      }
}

export default PasswordStrengthMeter;