import React, { Component } from 'react';

import { Helmet } from "react-helmet";

import { Link } from 'react-router-dom';

import PasswordStrengthMeter from './PasswordStrengthMeter';

import InputMask from "react-input-mask";

import { StyleInput, GroupCheckBox, RadioButton, DoubleInput } from '../../Style/global';

import { ContentCadastro, ButtonSubmit, Cadastrar } from './style';

import EyeOff from '../../images/eye-off.svg'
import EyeOn from '../../images/eye-on.svg'
import SocialLogin from '../../components/socialLogin';

import HeaderCarrinho from '../../components/header/carrinho';

import FooterAreaSecundaria from '../../components/footer/area-secundaria';




class PageCadastrEndereco extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'password',
            password: ''
        }
        this.showHide = this.showHide.bind(this);
    }
    showHide(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input' ? 'password' : 'input'
        })
    }
    render() {
        const { password } = this.state;
        return (
            <div>
                <Helmet>
                    <link rel="canonical" href="http://www.gazin.com.br" />
                    <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!" />
                    <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                    <meta name="author" content="Luis Alfredo Rosar" />
                    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                    <meta charSet="utf-8" />
                    <title>Gazin - Cadastro de Endereço</title>
                </Helmet>
                <HeaderCarrinho
                    identificacao={true}
                    pagamento={false}
                    obrigado={false}
                />
                <ContentCadastro>
                    <div className="container">
                        <h2>Cadastre um endereço para entrega</h2>
                        <span className="info-form">Você ainda não possuí um endereço cadastrado</span>

                        <form action="">
                            <StyleInput>
                                <label htmlFor="">Nome do destinatário*</label>
                                <input type="text" placeholder="Nome" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">CEP*</label>
                                <InputMask
                                    type="text"
                                    mask="99999-999"
                                    maskPlaceholder=""
                                    placeholder="Número do CEP"
                                />
                            </StyleInput>
                            <StyleInput>
                                <DoubleInput>
                                    <div>
                                        <label htmlFor="">Endereço*</label>
                                        <input type="text" placeholder="Rua" className="input-endereco" />
                                    </div>
                                    <div>
                                        <label htmlFor="">Número*</label>
                                        <input type="text" placeholder="Nº" />
                                    </div>
                                </DoubleInput>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Complemento</label>
                                <input type="text" placeholder="Opcional" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Bairro*</label>
                                <input type="text" placeholder="Nome do bairro" />
                            </StyleInput>
                            <StyleInput>
                                <DoubleInput>
                                    <div>
                                        <label htmlFor="">Cidade*</label>
                                        <input type="text" placeholder="Digite sua cidade" className="input-endereco" />
                                    </div>
                                    <div>
                                        <label htmlFor="">Estado*</label>
                                        <select name="" id="">
                                            <option value="">Estado</option>
                                            <option value="">MG</option>
                                            <option value="">SP</option>
                                            <option value="">RJ</option>
                                            <option value="">SP</option>
                                        </select>
                                    </div>
                                </DoubleInput>
                            </StyleInput>
                            <StyleInput>
                                <ButtonSubmit>entregar neste endereço</ButtonSubmit>
                            </StyleInput>
                        </form>
                    </div>
                </ContentCadastro>
                <footer>
                    <div className="container">
                        <FooterAreaSecundaria />
                    </div>
                </footer>
            </div>
        );
    }
}

export default PageCadastrEndereco;