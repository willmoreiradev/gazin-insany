import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import { Link } from 'react-router-dom';

import PasswordStrengthMeter from './PasswordStrengthMeter';

import InputMask from "react-input-mask";

import { StyleInput, GroupCheckBox, RadioButton } from '../../Style/global';

import { ContentCadastro, ButtonSubmit, Cadastrar } from './style';

import EyeOff from '../../images/eye-off.svg'
import EyeOn from '../../images/eye-on.svg'
import SocialLogin from '../../components/socialLogin';

import HeaderCarrinho from '../../components/header';

import FooterAreaSecundaria from '../../components/footer/area-secundaria';




class PageCadastro extends Component {
    constructor(props){
        super(props);
        this.state = {
            type: 'password',
            password: ''
        }
        this.showHide = this.showHide.bind(this);    
    }
    showHide(e){
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input' ? 'password' : 'input'
        })  
    }
    render() { 
        const { password } = this.state;
        return ( 
            <div>
                <Helmet>
                    <link rel="canonical" href="http://www.gazin.com.br"/>
                    <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
                    <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                    <meta name="author" content="Luis Alfredo Rosar" />
                    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                    <meta charSet="utf-8" />
                    <title>Gazin - Cadastro</title>
                </Helmet>
                <HeaderCarrinho
                    from={this.props.from}
                    identificacao={true}
                    pagamento={false}
                    obrigado={false}
                />
                <ContentCadastro>
                    <div className="container">
                        <h2>Cadastre-se na Gazin</h2>
                        <SocialLogin texto="Crie uma conta rápido e fácil com o Facebook ou Google" />
                        <span className="info-form">Ou preencha os campos logo abaixo</span>

                        <form action="">
                            <StyleInput>
                                <label htmlFor="">Nome</label>
                                <input type="text" placeholder="Seu nome"/>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">E-mail</label>
                                <input type="email" placeholder="Ex.: jorge@email.com"/>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">CPF</label>
                                <InputMask 
                                    type="text"
                                    mask="999.999.999-99"
                                    maskPlaceholder=""
                                    placeholder="Seu CPF"
                                />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Data de nascimento</label>
                                <InputMask 
                                    type="text"
                                    mask="99/99/9999"
                                    maskPlaceholder=""
                                    placeholder="Data"
                                />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Nº celular</label>
                                <InputMask 
                                    type="text"
                                    mask="(99) 99999-9999"
                                    maskPlaceholder=""
                                    placeholder="Celular"
                                />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Sexo</label>
                                <GroupCheckBox>
                                    <div>
                                        <input type="radio" name="sexo" id="feminino" checked/>
                                        <label htmlFor="feminino">Feminino</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="sexo" id="masculino"/>
                                        <label htmlFor="masculino">Masculino</label>
                                    </div>
                                </GroupCheckBox>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Senha</label>
                                <div>
                                    <div className="meter">
                                        <input autoComplete="off" type={this.state.type} onChange={e => this.setState({ password: e.target.value })} />
                                        <img title="Mostrar Senha" src={this.state.type === 'password' ? EyeOff : EyeOn} onClick={this.showHide} />
                                        <PasswordStrengthMeter password={password} />
                                    </div>
                                </div>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Confirmar senha</label>
                                <div>
                                    <input type={this.state.type} placeholder="Digite a sua senha" required />
                                    <img title="Mostrar Senha" src={this.state.type === 'password' ? EyeOff : EyeOn} onClick={this.showHide} />
                                </div>
                            </StyleInput>
                            <StyleInput>
                                <RadioButton>
                                    <input type="checkbox" id="ofertas" name="ofertas" />
                                    <label htmlFor="ofertas">Desejo receber ofertas por e-mail?</label>
                                </RadioButton>
                            </StyleInput>
                            <StyleInput>
                                <Link to="/cadastro/endereco"><ButtonSubmit>cadastrar</ButtonSubmit></Link>
                            </StyleInput>
                        </form>
                        <Cadastrar>
                            <span>Já possuí cadastro? <Link to={{
                        pathname: "/login",
                        state:{
                            from: this.props.from
                        }
                    }}>Acesse sua conta</Link></span>
                        </Cadastrar>
                    </div>
                </ContentCadastro>
                <footer>
                    <div className="container">
                    <FooterAreaSecundaria/>
                    </div>
                </footer>
            </div>
        );
    }
}
 
export default PageCadastro;