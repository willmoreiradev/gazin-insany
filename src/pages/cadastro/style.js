import styled from 'styled-components';

export const ContentCadastro = styled.div`
    margin-top: 114px;
    padding-top: 60px;
    padding-bottom: 60px;
    .container {
        width: 467px;
    }
    h2 {
        font-size: 20px;
        line-height: 26px;
        text-align: center;
        color: #363843;
        margin-bottom: 10px;
    }
    .info-form {
        display: block;
        text-align: center;
        font-size: 14px;
        line-height: 17px;
        color: #646981;
    }
    form {
        margin-top: 32px;
    }
    @media(max-width: 1200px) {
        margin-top: 150px;
        padding: 40px 0px;
        .container {
            width: 100%;
        }
    }
    @media(max-width: 480px) {
        margin-top: 125px;
        h2 {
            max-width: 250px;
            margin: 0 auto;
            margin-bottom: 15px;
        }
    }
`;

export const ButtonSubmit = styled.button`
    width: 100%;
    height: 56px;
    background-color: #FFA82E;
    border: 2px solid #FFA82E;
    border-radius: 6px;
    font-weight: 600;
    letter-spacing: -0.114286px;
    text-transform: uppercase;
    color: #FFFFFF;
    transition: all .3s;
    &:hover {
        background-color: transparent;
        color: #FFA82E;
        transition: all .3s;
    }
`;

export const Cadastrar = styled.div`
  span {
    font-size: 14px;
    line-height: 17px;
    color: #363843;
    text-align: center;
    display: block;
    a {
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        color: #0D71F0;
        transition: all .3s;
        &:hover {
            transition: all .3s;
            color: #FFA82E;
        }
    }
  }
`;

