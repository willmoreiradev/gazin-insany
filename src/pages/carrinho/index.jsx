import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { ContentCarrinho, Left, Resumo, CalcularFrete } from './style';
import { Link } from 'react-router-dom';

import Header from '../../components/header';
import Categorias from '../../components/categoria';
import ItensCarrinho from '../../components/carrinho/itens';
import SessaoProdutos from '../../components/produtos';
import InformacoesPagamentoOferta from '../../components/pagamento-oferta';
import FooterAreaPrimaria from '../../components/footer/area-primaria';
import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import Icone from '../../components/fontawesome';

import IconeCarrinhoWhite from '../../images/icone-carrinho-white.svg';

//Images

import IconeCarrinho from '../../images/icone-carrinho-azul.svg';
import IconDelivery from '../../images/icon-home-delivery.svg';

class PageHome extends Component {
    state = {
        qtdItens: 0
    }
    componentDidMount() {
        let qtdProdutos = document.querySelectorAll('table tbody tr');
        this.setState({ qtdItens: qtdProdutos.length })
    }
    render() {
        return (
            <div>
                <Helmet>
                    <link rel="canonical" href="http://www.gazin.com.br" />
                    <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!" />
                    <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                    <meta name="author" content="Luis Alfredo Rosar" />
                    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                    <meta charSet="utf-8" />
                    <title>Gazin - Carrinho</title>
                </Helmet>

                <Header />

                <Categorias />

                <ContentCarrinho>
                    <div className="container">
                        <Left>
                            <div className="title">
                                <img src={IconeCarrinho} alt="" />
                                <span>Carrinho de compras ({this.state.qtdItens})</span>
                            </div>
                            <ItensCarrinho />
                            <CalcularFrete>
                                <img src={IconDelivery} alt=""/>
                                <div className="info">
                                    <h3>CalcularFrete</h3>
                                    <p>Informa o CEP para calcular o valor do frete para o seu endereço</p>
                                    <div className="formulario">
                                        <div className="form-group">
                                            <input type="text" placeholder="Digite seu CEP"/>
                                        </div>
                                        <div className="form-group">
                                            <button type="button">Calcular</button>
                                        </div>
                                    </div>
                                </div>
                            </CalcularFrete>
                        </Left>
                        <Resumo>
                            <h2>Resumo do pedido</h2>
                            <ul>
                                <li>
                                    <span>Subtotal</span>
                                    <strong>R$ 7.998,00</strong>
                                </li>
                                <li>
                                    <span>Descontos</span>
                                    <strong>R$ 0,00</strong>
                                </li>
                            </ul>
                            <div className="total">
                                <div className="esq">
                                    <span>Total</span>
                                </div>
                                <div className="dir">
                                    <h4>R$ 8.013,99</h4>
                                    <span>10 x de R$ 399,90 s/ juros</span>
                                </div>
                            </div>
                            <Link 
                                to={{
                                    pathname: "/checkout",
                                    state: {
                                        from: "/carrinho"
                                    }
                                }}
                                className="btn-entrar btn-continuar" 
                            >
                                <img src={IconeCarrinhoWhite} alt=""/>
                                Continuar
                            </Link>
                            <Link to="/" className="btn-comprar">Continuar comprando</Link>
                        </Resumo>
                    </div>
                </ContentCarrinho>

                <SessaoProdutos
                    titulo="Aproveite para levar também"
                />

                <InformacoesPagamentoOferta />

                <footer>
                    <div className="container">
                        <FooterAreaPrimaria />
                        <FooterAreaSecundaria />
                    </div>
                </footer>
            </div>
        );
    }
}

export default PageHome;