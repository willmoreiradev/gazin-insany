import styled from 'styled-components';

export const ContentCarrinho = styled.div`
    border-top: 1px solid #DDDDDD;
    padding-top: 40px;
    padding-bottom: 80px;
    .container {
        display: grid;
        grid-template-columns: 1fr 405px;
        grid-column-gap: 32px;
    }
    @media(max-width: 1200px) {
        margin-top: 150px;
        .container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
        }
    }
    @media(max-width: 480px) {
        margin-top: 125px;
        padding: 40px 0px;
        .cont-table {
            width: 100%;
            overflow-x: auto;
            border: 1px solid #DDDDDD;
        }
    } 
`;

export const Left = styled.div`
    .title {
        display: flex;
        align-items: center;
        margin-bottom: 28px;
        img {
            margin-right: 18px;
        }
        span {
            font-weight: 600;
            font-size: 18px;
            line-height: 22px;
            color: #363843;
        }
    }
    @media(max-width: 480px) {
        width: 100%;
        display: flex;
        overflow: hidden;
        flex-direction: column;
        align-items: flex-start;
    }
`;

export const Resumo = styled.div` 
    width: 100%;
    height: 439px;
    background: #F7F7F7;
    border-radius: 6px;
    padding: 30px 32px;
    margin-top: 52px;
    h2 {
        font-weight: 500;
        font-size: 16px;
        line-height: 1;
        color: #363843;
        padding-bottom: 23px;
        border-bottom: 1px solid #CCCCCC;
        margin-bottom: 32px;
    }
    ul {
        padding-bottom: 41px;
        border-bottom: 1px solid #CCCCCC;
        margin-bottom: 26px;
        li {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 22px;
            &:last-child {
                margin-bottom: 0px;
            }
            span {
                font-size: 14px;
                line-height: 17px;
                color: #646981;
            }
            strong {
                font-weight: 500;
                font-size: 16px;
                line-height: 19px;
                text-align: right;
                color: #646981;
            }
        }
    }
    .total {
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
        margin-bottom: 17px;
        span {
            font-size: 14px;
            line-height: 17px;
            color: #646981;
        }
        h4 {
            font-size: 20px;
            line-height: 24px;
            text-align: right;
            color: #363843;
        }
    }
    .btn-continuar {
        background-color: #6BB70B;
        border-radius: 8px;
        width: 100%;
        height: 54px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: 600;
        font-size: 16px;
        text-transform: uppercase;
        color: #FFFFFF;
        margin-bottom: 38px;
        transition: all .3s;
        img {
            margin-right: 10px;
        }
        &:hover {
            background-color: #5f9e0e;
            transition: all .3s;
        }
    }
    .btn-comprar {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        display: block;
        color: #646981;
    }
    @media(max-width: 480px) {
        margin-top: 30px;
        height: auto;
        padding: 30px 25px;
        .total {
            margin-top: 37px;
        }
    }
`;


export const CalcularFrete = styled.div`
    display: flex;
    align-items: flex-start;
    margin-top: 28px;
    .info {
        width: 100%;
        max-width: 430px;
        margin-left: 16px;
        h3 {
            font-weight: 600;
            font-size: 16px;
            line-height: 19px;
            color: #363843;
            margin-bottom: 4px;
        }
        p {
            font-size: 14px;
            line-height: 17px;
            color: #646981;
        }
        .formulario {
            margin-top: 16px;
            display: flex;
            align-items: flex-start;
            justify-content: space-between;
            input {
                width: 297px;
                height: 45px;
                background: #FFFFFF;
                border: 1px solid #DCDDE3;
                box-sizing: border-box;
                border-radius: 6px;
                padding: 0px 13px;
                font-size: 14px;
                transition: all .3s;
                &::placeholder {
                    color: #A0A4B3;
                }
                &:focus {
                    border: 1px solid #0C64D3;
                    transition: all .3s;
                }
            }
            button {
                width: 115px;
                height: 45px;
                border: 2px solid #FF9D2E;
                background-color: #FF9D2E;
                border-radius: 6px;
                font-weight: 500;
                font-size: 14px;
                color: #FFFFFF;
                transition: all .3s;
                &:hover {
                    background-color: transparent;
                    color: #FF9D2E;
                    transition: all .3s;
                }
            }
        }
    }
    @media(max-width:480px) {
        width: 100%;
        margin-top: 0;
        img {
            display: none;
        }
        .info {
            max-width: 100%;
            margin-left: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            h3 {
                font-weight: 600;
                font-size: 18px;
                line-height: 25px;
                text-align: center;
                margin-bottom: 8px;
            }
            p {
                font-size: 14px;
                line-height: 20px;
                text-align: center;
                max-width: 270px;
                margin-bottom: 0px;
            }
            .formulario {
                flex-direction: column;
                align-items: flex-start;
                width: 100%;
                .form-group {
                    margin-bottom: 15px;
                    width: 100%;
                    &:last-child {
                        margin-bottom: 0px;
                    }
                }
                input {
                    width: 100%;
                }
                button {
                    width: 100%;
                }
            }
        }
    }
`;