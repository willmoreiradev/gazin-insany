import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import { Link } from "react-router-dom";

import styled from 'styled-components'

import Header from '../../components/header';

import FooterCompra from '../../components/footer/area-compra';

import IconeHome from '../../images/icone-home.svg';

import IconCupom from '../../images/icon-cupom.svg'

import PagamentoAction from '../../components/pagamentoAction'

import RetirarLoja from '../../components/RetirarLoja'

const AreaCupom = styled.section`
    padding: 55px 0;
    .container{
        width: 1060px;
    }
    @media(max-width: 1200px) {
        .container {
            width: 100%;
        }
    }
    @media(max-width: 480px) {
        padding: 30px 0px;
    }
`

const AreaCheckout = styled.section`
    padding-top: 66px;
    .container{
        width: 1060px;
    }
    .top{
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding-bottom: 56px;
        border-bottom: 1px solid #E8E8E8;
        .endereco{
            display: flex;
            flex-direction: column;
            h2{
                font: normal 600 18px/26px 'Inter';
                color: #363843;
                margin-bottom: 12px;
            }
            .nome{
                font: normal bold 16px/27px 'Inter';
                color: #363843;
            }
            .end{
                width: 310px;
                font: normal normal 16px/27px 'Inter';
                color: #646981;
                margin-bottom: 15px;
            }
            a{
                width: 222px;
                height: 27px;
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: center;
                background-color: transparent;
                font: normal 500 14px/27px 'Inter';
                color: #094E99;
                img{
                    margin-right: 16px;
                }
            }
        }
        .detalhe{
            width: 624px;
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            justify-content: flex-start;
            h2{
                font: normal 600 18px/26px 'Inter';
                color: #363843;
                margin-bottom: 20px;
            }
            .info{
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-between;
                width: 100%;
                margin-bottom: 19px;
                .nProdutos{
                    font: normal 600 16px/19px 'Inter';
                    color: #646981;
                    button{
                        font: normal normal 13px/16px 'Inter';
                        color: #A0A4B3;
                        background-color: transparent;
                        margin-left: 19px;
                    }
                }
                .parcela{
                    font: normal 500 13px/16px 'Inter';
                    color: #A0A4B3;
                }
            }
            .cards{
                display: flex;
                flex-direction: row;
                align-items: center;
            }
        }
    }
    .entrega{
        padding-top: 30px;
        padding-bottom: 50px;
        h2{
            font: normal 600 18px/26px 'Inter';
            color: #363843;
            margin-bottom: 28px;
        }
        .cards{
            display: flex;
            align-items: center;
            flex-direction: row;

        }

    }
    @media(max-width: 1200px) {
        padding-top: 40px;
        margin-top: 137px;
        .container {
            width: 100%;
        }
        .top {
            flex-direction: column;
            align-items: center;
            .endereco {
                margin-bottom: 30px;
                display: flex;
                align-items: center;
                .end {
                    text-align: center;
                }
            }
            .detalhe {
                width: 100%;
                align-items: center;
                .info {
                    flex-direction: column;
                    margin: 0;
                    .nProdutos {
                        margin-bottom: 10px;
                    }
                }
                .cards {
                    width: 100%;
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    grid-gap: 20px;
                    margin-top: 30px;
                }
            }
        }
        .entrega {
            padding-bottom: 0;
            h2 {
                text-align: center;
                margin-bottom: 20px;
            }
            .cards {
                display: grid;
                grid-template-columns: 1fr 1fr;
                grid-gap: 20px;
            }
        }
    }
    @media(max-width: 480px) {
        .top {
            .detalhe {
                .cards {
                    grid-template-columns: 1fr 1fr;
                    grid-gap: 10px;
                }
            }
        }
        .entrega {
            .cards {
                grid-template-columns: 1fr;
                grid-gap: 10px;
            }
        }
    }
`

const Cupom = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    .info{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        .icon{
            margin-right: 22px;
            width: 60px;
        }
        .text{
            display: flex;
            align-items: flex-start;
            justify-content: flex-start;
            flex-direction: column;
            h3{
                font: normal 600 18px/26px 'Inter';
                color: #363843;
            }
            span{
                font: normal 500 14px/17px 'Inter';
                color: #646981;
            }
        }
    }
    form{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        input{
            width: 538px;
            height: 45px;
            border: 1px solid #DCDDE3;
            box-sizing: border-box;
            border-radius: 6px;
            padding-left: 24px;
            font: normal normal 14px/17px 'Inter';
            color: #A0A4B3;
            margin-right: 23px;
        }
        button{
            width: 104px;
            height: 45px;
            background: #6BB70B;
            border-radius: 6px;
            font: normal 500 14px/17px 'Inter';
            color: #FFFFFF;
            transform: scale(1);
            transition: all .3s;

            &:hover{
                transform: scale(1.05);
                transition: all .3s;
            }
        }
    }
    @media(max-width: 1200px) {
        flex-direction: column;
        align-items: center;
        .info {
            margin-bottom: 30px;
        }
    }
    @media(max-width: 480px) {
        form {
            width: 100%;
            flex-direction: column;
            align-items: center;
            input {
                width: 100%;
                margin-bottom: 15px;
                margin-right: 0px;
            }
            button {
                width: 100%;
            }
        }
    }
`

const CardDetalhes = styled.div`
    display: flex;
    flex-direction: column;
    width: 142px;
    height: 72px;
    background-color: #F6F6F6;
    padding-top: 14px;
    padding-left: 21px;
    margin-right: 3px;
    &:last-child{
        margin-right: 0;
    }
    .title{
        font: normal normal 14px/17px 'Inter';
        color: #A0A4B3;
        margin-bottom: 6px;
    }
    .info{
        font: normal 600 16px/19px 'Inter'; 
        color: ${props => props.isFreteGratis ? '#6BB70B' : '#363843'};
    }
    &:last-child{
        width: 200px; 
    }
    @media(max-width: 1200px) {
        width: 100% !important;
        padding: 20px 10px;
        height: auto;
        .title {
            text-align: center;
        }
    }
`

const CardEntrega = styled.label`
    cursor: pointer;
    width: 244px;
    height: 100px;
    padding-top: 13px;
    padding-left: 16px;
    border: 1px solid ${props => props.active ? '#0D71F0' : '#DCDDE3'};
    border-radius: 6px;
    display: flex;
    margin-right: 27px;
    &:last-child{
        margin-right: 0;
    }
    input{
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    input:checked ~ .radio{
        opacity: 1;
    }
    input:checked ~ .radio .checkmark{
        background-color: #0D71F0;
    }
    .radio{
        display: flex;
        align-items: center;
        justify-content: center;
        .checkmark{
            height: 8.8px;
            width: 8.8px;
            border-radius: 9px;
            background-color: #ffffff;
        }
        width: 20px;
        border-radius: 20px;
        opacity: 0.4;
        border: 1px solid #A0A4B3;
        height: 20px;
        margin-right: 16px;
    }
    .text{
        display: flex;
        flex-direction: column;
        .title{
            font: normal normal 16px/24px 'Inter';
            margin-bottom: 1px;
            color: #646981;
        }
        .tempo{
            font: normal normal 12px/22px 'Inter';
            margin-bottom: 1px;
            color: #A0A4B3;
        }
        .preco{
            display: flex;
            align-items: ${props => props.isRecomendado ? 'center' : 'flex-start' };
            justify-content: ${props => props.isRecomendado ? 'center' : 'flex-start' };
            padding: 0 ${props => props.isRecomendado ? '8px' : '0' };
            border: 1px solid ${props => props.isRecomendado ? '#6BB70B' : 'transparent'};
            border-radius: 2px;
            font: normal normal 14px/22px 'Inter';
            margin-bottom: 1px;
            color: ${props => props.isRecomendado ? '#6BB70B' : '#A0A4B3'};
        }
    }
    @media(max-width: 1200px) {
        width: 100%;
        margin-right: 0;
    }
`

class Checkout extends Component {
    constructor(props){
        super(props);
        this.state = {
            entrega: "convencional"
        }
    }

  render() { 
    return ( 
      <div>
        <Helmet>
            <link rel="canonical" href="http://www.gazin.com.br"/>
            <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
            <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
            <meta name="author" content="Luis Alfredo Rosar" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <meta charSet="utf-8" />
            <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
        </Helmet>
        <Header
            from="/carrinho"
            identificacao={true}
            pagamento={true}
            obrigado={false}
        />

        <AreaCheckout>
            <div className="container">
                <div className="top">
                    <div className="endereco">
                        <h2>Endereço de entrega</h2>
                        <span className="nome">
                            Humberto da Silva
                        </span>
                        <span className="end">
                            Rua Marquês de São Vicente, 111, Gávea 99999-999 - Rio de Janeiro/RJ
                        </span>
                        <Link to='/conta/enderecos' className="alterar">
                            <img src={IconeHome} alt=""/>
                            Alterar endereço de entrega
                        </Link>
                    </div>
                    <div className="detalhe">
                        <h2>Detalhes do seu pedido</h2>
                        <div className="info">
                            <span className="nProdutos">
                                02 produtos
                                <button>
                                    visualizar produtos
                                </button>
                            </span>
                            <span className="parcela">
                                10 x de R$ 399,90 s/ juros
                            </span>
                        </div>
                        <div className="cards">
                            <CardDetalhes isFreteGratis >
                                <span className="title">
                                    Frete
                                </span>
                                <span className="info">
                                    Grátis
                                </span>
                            </CardDetalhes>
                            <CardDetalhes>
                                <span className="title">
                                    Subtotal
                                </span>
                                <span className="info">
                                    R$ 7.998,00
                                </span>
                            </CardDetalhes>
                            <CardDetalhes>
                                <span className="title">
                                    Descontos
                                </span>
                                <span className="info">
                                    R$ 0,00
                                </span>
                            </CardDetalhes>
                            <CardDetalhes>
                                <span className="title">
                                    Total
                                </span>
                                <span className="info">
                                    R$ 8.013,99
                                </span>
                            </CardDetalhes>
                        </div>
                    </div>
                </div>
                <div className="entrega">
                    <h2>Opções de Entrega</h2>
                    <div className="cards">
                            <CardEntrega isRecomendado  className="cardEntrega" onClick={()=>this.setState({entrega : "loja"})} active={this.state.entrega == "loja" ? true : false}>
                                <input type="radio" checked={this.state.entrega == "loja" ? true : false} name="retirar" id="" />
                                <div className="radio">
                                    <div className="checkmark"></div>
                                </div>
                                <div className="text">
                                    <span className="title">
                                        Retirar na loja
                                </span>
                                    <span className="tempo">
                                        Grátis
                                </span>
                                    <span className="preco">
                                        Recomendado
                                </span>
                                </div>
                            </CardEntrega>
                            <CardEntrega className="cardEntrega" onClick={()=>this.setState({entrega : "convencional"})} active={this.state.entrega == "convencional" ? true : false} >
                                <input type="radio" checked={this.state.entrega == "convencional" ? true : false}  name="retirar" id="" />
                                <div className="radio">
                                    <div className="checkmark"></div>
                                </div>
                                <div className="text">
                                    <span className="title">
                                        Convencional
                                    </span>
                                    <span className="tempo">
                                        até 8 dias úteis
                                    </span>
                                    <span className="preco">
                                        R$ 15,99
                                    </span>
                                </div>
                            </CardEntrega>
                            <CardEntrega className="cardEntrega" onClick={()=>this.setState({entrega : "rapida"})} active={this.state.entrega == "rapida" ? true : false}>
                                <input type="radio" checked={this.state.entrega == "rapida" ? true : false} name="retirar" id="" />
                                <div className="radio">
                                    <div className="checkmark"></div>
                                </div>
                                <div className="text">
                                    <span className="title">
                                        Rápida
                                    </span>
                                    <span className="tempo">
                                        até 8 dias úteis
                                    </span>
                                    <span className="preco">
                                        R$ 15,99
                                    </span>
                                </div>
                            </CardEntrega>
                            <CardEntrega className="cardEntrega" onClick={()=>this.setState({entrega : "agendada"})} active={this.state.entrega == "agendada" ? true : false}>
                                <input type="radio" checked={this.state.entrega == "agendada" ? true : false} name="retirar" id="" />
                                <div className="radio">
                                    <div className="checkmark"></div>
                                </div>
                                <div className="text">
                                    <span className="title">
                                        Agendada
                                    </span>
                                    <span className="tempo">
                                        à partir de 15 de dezembro
                                    </span>
                                    <span className="preco">
                                        R$ 15,99
                                    </span>
                                </div>
                            </CardEntrega>
                    </div>
                </div>
                </div>
            </AreaCheckout>

            <RetirarLoja show={this.state.entrega == "loja" ? true : false} /> 

            <AreaCupom>
                <div className="container">
                    <Cupom>
                        <div className="info">
                            <div className="icon">
                                <img src={IconCupom} alt=""/>
                            </div>
                            <div className="text">
                                <h3>Possui cupom ou vale?</h3>
                                <span>Digite o código do seu cupom ou vale</span>
                            </div>
                        </div>
                        <form action="">
                            <input type="text" placeholder="Digite o código" />
                            <button type="submit">ENVIAR</button>
                        </form>
                    </Cupom>
                </div>
            </AreaCupom>
            <PagamentoAction />
            

        <footer>
          <div className="container">
            <FooterCompra />
          </div>
        </footer>
      </div>
    );
  }
}
 
export default Checkout;