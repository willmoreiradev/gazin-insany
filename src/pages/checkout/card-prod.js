import React, { Component } from 'react';

import { CardProduto } from './styles';

class ComponentCardProd extends Component {
  render() { 
    return ( 
      <CardProduto>
        <div className="image">
          <img src={this.props.image} alt=""/>
        </div>
        <div className="info">
          <h4>{this.props.nome_produto}</h4>
          <ul>
            <li>Qnt: {this.props.qtd}</li>
            <li>R$ {this.props.valor}</li>
          </ul>
        </div>
      </CardProduto>
    );
  }
}
 
export default ComponentCardProd;