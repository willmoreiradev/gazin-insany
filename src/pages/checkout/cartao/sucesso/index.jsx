import React, { useState } from 'react';

import { Helmet } from "react-helmet";

import { Link } from "react-router-dom";

import styled from 'styled-components'

import FooterCompra from '../../../../components/footer/area-compra';

import Check from '../../../../images/icon/check.svg'

import IconCartaoActive from '../../../../images/cartao-ativo.svg'

import LogoCheckout from '../../../../images/new-logo.svg';
import CheckoutCheckout from '../../../../images/icon-cart-checkout.svg';
import CheckoutUser from '../../../../images/icon-ident-checkout.svg';
import IconCheckoutPagamento from '../../../../images/icon-cash-checkout.svg';
import IconObrigado from '../../../../images/icon-obrigado-checkout.svg';
import IconCadeado from '../../../../images/icon-cadeado.svg';

import ProdutoResumo from '../../../../images/resumo-produto.png';

import {
    HeaderCheckout,
    EtapaMeuCarrinho,
    EtapaIdentificacao,
    EtapaPagamento,
    EtapaObrigado
} from '../../styles';


const Area = styled.section`
    padding-top: 66px;
    margin-top: 100px;
    .container{
        width: 1060px;
    }
    .top{
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding-bottom: 56px;
        border-bottom: 1px solid #E8E8E8;
        .obrigado{
            display: flex;
            align-items: center;
            justify-content: flex-start;
            .icon{
                margin-right: 31px;
            }
            .texto{
                max-width: 463px;
                flex-direction: column;
                h2{
                    font:normal 600 18px/26px 'Inter';
                    color: #363843;
                    margin-bottom: 6px;
                }
                span{
                    font:normal normal 15px/26px 'Inter';
                    color: #363843;
                    strong{
                        font:normal bold 15px/26px 'Inter';
                    }
                }
            }
        }
        .pedido{
            width: 405px;
            height: 114px;
            background-color: #F6F6F8;
            border: 1.5px solid #FFFFFF;
            box-sizing: border-box;
            border-radius: 6px;
            display: flex;
            align-items: center;
            justify-content: center;
            .texto{
                font:normal normal 18px/26px 'Inter';
                color: #646981;
                margin-right: 46px;
            }
            .numero{
                font:normal 600 24px/26px 'Inter';
                color: #363843;
            }
        }    
    }
    .info-compra{
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding-top: 41px;
        padding-bottom: 42px;
        border-bottom: 1px solid #E8E8E8;
        .pagamento{
            h2{
                font:normal 600 18px/26px 'Inter';
                color: #4F4F4F;
                margin-bottom: 42px;
            }
            .tipo-pagamento{
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: flex-start;
                margin-bottom: 51px;
                .icon{
                    width: 40px;
                    height: 29.16px;
                    margin-right: 27px;
                    img{
                        width: 100%;
                    }
                }
                .info{
                    display: flex;
                    align-items: center;
                    justify-content: flex-start;
                    .nome{
                        font:normal 600 16px/20px 'Inter';
                        padding-right: 35px;
                        border-right: 1px solid #DCDDE3;
                        color: #646981;
                    }
                    .valor{
                        padding-left: 33px;
                        font:normal normal 16px/20px 'Inter';
                        color: #646981;
                        strong{
                            font:normal 600 16px/20px 'Inter';
                            color: #646981;
                        }
                    }
                }
            }
        }
        .endereco{
            h2{
                align-self: flex-start;
                font:normal 600 18px/26px 'Inter';
                color: #4F4F4F;
                margin-bottom: 13px;
            }
            max-width: 425px;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            .info{
                .nome{
                    font:normal bold 16px/27px 'Inter';
                    color: #363843;
                }
                .endereco{
                    font:normal normal 16px/27px 'Inter';
                    color: #646981;
                    max-width: 310px;
                }
            }
        }
    }
    .resumo{
        padding-top: 43px;
        padding-bottom: 70px;
        h2{
            font:normal 600 18px/26px 'Inter';
            color: #363843;
            margin-bottom: 28px;
        }
        .produtos{
            width: 1058px;
            border: 1px solid #DCDDE3;
            box-sizing: border-box;
            border-radius: 6px;
            .head{
                width: 100%;
                padding: 0 38px;
                display: flex;
                align-items: center;
                justify-content: flex-start;
                height: 73px;
                border-bottom: 1px solid #DCDDE3;
                span{
                    display: flex;
                    align-items: center;
                    justify-content: flex-start;
                }
                .produto{
                    width: 472px;
                }
                .qtd{
                    width: 133px;
                }
                .entrega{
                    width: 193px
                }
                .preco{
                    width: 184px;
                }
            }
        }
    }
    .valor-total-container{
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: flex-end;
        padding-top: 28px;
        padding-right: 102px;
        .valor-total{
            text-align: left;
            display: flex;
            flex-direction: column;
            span{
                font:normal normal 16px/20px 'Inter';
                color: #646981;
                margin-bottom: 14px;
            }
            .valor{
                font:normal 600 20px/20px 'Inter';
                color: #0D71F0;
            }
        }
    }
    @media(max-width: 1200px) {
        margin-top: 137px;
        padding: 40px 0px;
        .container {
            width: 100%;
        }
        .top {
            flex-direction: column;
            align-items: center;
            padding-bottom: 30px;
            .obrigado {
                flex-direction: column;
                align-items: center;
                margin-bottom: 20px;
                .icon {
                    margin-right: 0;
                    margin-bottom: 15px;
                }
                .texto {
                    max-width: 100%;
                    align-items: center;
                    display: flex;
                }
            }
        }
        .info-compra {
            padding: 40px 0px;
            flex-direction: column;
            align-items: center;
            .pagamento {
                margin-bottom: 30px;
                h2 {
                    text-align: center;
                    margin-bottom: 20px;
                }
                .tipo-pagamento {
                    justify-content: center;
                    align-items: center;
                    margin-bottom: 30px;
                    .icon{
                        display: none;
                    }
                    .info{
                        flex-direction: column;
                        .nome{
                            padding-right: 0;
                            border-right: none;
                            margin-bottom: 30px
                        }
                        .valor{
                            padding-left: 0;
                        }
                    }
                }
                .etapas {
                    .etapa {
                        flex-direction: column;
                        align-items: center;
                        .numero {
                            margin-right: 0;
                            margin-bottom: 10px;
                        }
                        p {
                            text-align: center;
                            line-height: 22px;
                        }
                    }
                }
            }
            .endereco {
                text-align: center;
                h2 {
                    width: 100%;
                    text-align: center;
                }
                .info {
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                }
            }
        }
        .resumo {
            padding-bottom: 0;
            h2 {
                text-align: center;
            }
            .cont-table {
                width: 100%;
                overflow-x: auto;
                border: 1px solid #DCDDE3;
                .produtos {
                    border: none;
                }
            }
        }
        .valor-total-container {
            padding: 0;
            justify-content: center;
            margin-top: 40px;
            span {
                text-align: center;
            }
        }
    }
    @media(max-width: 480px) {
        .top {
            .obrigado {
                .icon {
                    img {
                        max-width: 55px;
                    }
                }
                .texto {
                    span {
                        text-align: center;
                    }
                }
            }
            .pedido {
                width: 100%;
                height: 82px;
                justify-content: space-between;
                padding: 0px 15px;
                .texto {
                    font-size: 13px;
                }
                .numero {
                    font-size: 18px;
                }
            }
        }
    }
`

const ProdutoContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    height: 146px;
    margin: 0 31px;
    border-bottom: 1px solid #DCDDE3;
    &:last-child{
        border-bottom: none;
    }
    .imagem{
        display: flex;
        align-items: center;
        justify-content: center;
        width: 85px;
        height: 85px;
        box-shadow: 0px 1.15254px 2.59322px rgba(54, 56, 67, 0.11);    
        background: #FFFFFF;
        border-radius: 2px;
        overflow: hidden;
        margin-right: 40px;
        img{
            width: initial;
        }
    }
    .nome{
        font:normal normal 15px/21px 'Inter';
        color: #646981;
        margin-right: 78px;
    }
    .qtd{ 
        font:normal normal 14px/17px 'Inter';
        text-align: center;
        letter-spacing: -0.1px;
        color: #646981;
        margin-right: 107px;
    }
    .entrega{
        font:normal normal 15px/18px 'Inter';
        color: #646981;
        margin-right: 112px;
    }
    .preco{
        font:normal normal 18px/22px 'Inter';
        color: #646981;
    }
`

const Produto = (props) => {
    return (
        <ProdutoContainer>
            <div className="imagem">
                <img src={props.imagem} alt="" />
            </div>
            <span className="nome">
                {props.nome}
            </span>
            <span className="qtd">
                {props.qtd}
            </span>
            <span className="entrega">
                {props.entrega}
            </span>
            <span className="preco">
                {props.preco}
            </span>
        </ProdutoContainer>
    )
}

export default function SucessoCartao() {
    const [carrinho, setCarrinho] = useState(true);
    const [identificacao, setIdentificacao] = useState(true);
    const [pagamento, setPagamento] = useState(true);
    const [obrigado, setObrigado] = useState(true);
    const [entrega, setEntrega] = useState('convencional');

    return (
        <div>
            <Helmet>
                <link rel="canonical" href="http://www.gazin.com.br" />
                <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!" />
                <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                <meta name="author" content="Luis Alfredo Rosar" />
                <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                <meta charSet="utf-8" />
                <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
            </Helmet>
            <HeaderCheckout>
                <div className="container">
                    <Link to="/">
                        <img src={LogoCheckout} alt="" />
                    </Link>
                    <ul>
                        <EtapaMeuCarrinho active={carrinho}>
                            <img src={CheckoutCheckout} alt="" />
                            <span>Meu Carrinho</span>
                        </EtapaMeuCarrinho>
                        <EtapaIdentificacao active={identificacao}>
                            <img src={CheckoutUser} alt="" />
                            <span>Identificação</span>
                        </EtapaIdentificacao>
                        <EtapaPagamento active={pagamento}>
                            <img src={IconCheckoutPagamento} alt="" />
                            <span>Pagamento</span>
                        </EtapaPagamento>
                        <EtapaObrigado active={obrigado}>
                            <img src={IconObrigado} alt="" />
                            <span>Obrigado!</span>
                        </EtapaObrigado>
                    </ul>
                    <div className="seguro">
                        <div className="icon">
                            <img src={IconCadeado} alt="" />
                        </div>
                        <span>Ambiente <strong>seguro!</strong></span>
                    </div>
                </div>
            </HeaderCheckout>

            <Area>
                <div className="container">
                    <div className="top">
                        <div className="obrigado">
                            <div className="icon">
                                <img src={Check} alt="" />
                            </div>
                            <div className="texto">
                                <h2>Muito obrigado, Humberto!</h2>
                                <span>Enviamos um e-mail de confirmação de compra para <strong>humberto@email.com</strong></span>
                            </div>
                        </div>
                        <div className="pedido">
                            <span className="texto">Nº do pedido</span>
                            <span className="numero">0303030409</span>
                        </div>
                    </div>
                    <div className="info-compra">
                        <div className="pagamento">
                            <h2>Forma de pagamento</h2>
                            <div className="tipo-pagamento">
                                <div className="icon">
                                    <img src={IconCartaoActive} alt="" />
                                </div>
                                <div className="info">
                                    <span className="nome">Cartão de crédito</span>
                                    <span className="valor">Valor: <strong>R$ 3.200,00</strong></span>
                                </div>
                            </div>
                        </div>
                        <div className="endereco">
                            <h2>Endereço de entrega</h2>
                            <div className="info">
                                <span className="nome">
                                    Humberto da Silva
                              </span>
                                <span className="endereco">
                                    Rua Marquês de São Vicente, 111, Gávea 99999-999 - Rio de Janeiro/RJ
                              </span>
                            </div>
                        </div>
                    </div>
                    <div className="resumo">
                        <h2>Resumo da compra</h2>
                        <div className="cont-table">
                            <div className="produtos">
                                <div className="head">
                                    <span className="produto">Produto</span>
                                    <span className="qtd">Qtd.</span>
                                    <span className="entrega">Entrega</span>
                                    <span className="preco">Preço</span>
                                </div>
                                <div className="lista">
                                    <Produto
                                        imagem={ProdutoResumo}
                                        nome="Phone 8 64GB Cinza Espacial Tela 4.7"
                                        qtd="01"
                                        entrega="Frete Grátis"
                                        preco="R$ 3.999,00"
                                    />
                                    <Produto
                                        imagem={ProdutoResumo}
                                        nome="Phone 8 64GB Cinza Espacial Tela 4.7"
                                        qtd="01"
                                        entrega="Frete Grátis"
                                        preco="R$ 3.999,00"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="valor-total-container">
                            <div className="valor-total">
                                <span>Valor Total:</span>
                                <span className="valor">R$ 3.200,00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </Area>



            <footer>
                <div className="container">
                    <FooterCompra />
                </div>
            </footer>
        </div>
    );
}
