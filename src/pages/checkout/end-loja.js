import React, { useState } from 'react';
import styled from 'styled-components';

const StyleComponent = styled.div`
    display: flex;
    align-items: flex-start;
    border-bottom: 1px solid #DCDDE3;
    padding: 12px 0px;
    cursor: pointer;
    &:last-child {
      padding-bottom: 0px;
      border-bottom: none;
    }
    .circle {
      width: 20px;
      height: 20px;
      border: ${props => props.check ? '1px solid #0D71F0' : '1px solid #DCDDE3'};
      background-color: ${props => props.check ? '#0D71F0' : '#fff'};
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: all .3s;
      &:after {
        content: "";
        transform: ${props => props.check ? 'scale(1)' : 'scale(0)'};
        width: 10px;
        height: 10px;
        display: inline-block;
        background-color: #fff;
        border-radius: 50%;
        transition: all .3s;
      }
    }
    .info {
      flex: 1;
      margin-left: 16px;
      .end {
        margin-bottom: 5px;
        font-size: 14px;
        line-height: 22px;
        color: #363843;
        max-width: 215px;
        strong {
          color: #363843;
          font-size: 14px;
        }
      }
      .time-retirada {
        display: block;
        font-size: 13px;
        line-height: 19px;
        color: #363843;
      }
      .distancia {
        font-size: 14px;
        line-height: 24px;
        color: #646981;
      }
    }
`;

export default function ComponentCheckout(props) {
  const [checked, setChecked] = useState(false);
  return ( 
    <StyleComponent check={checked} onClick={() => {
      setChecked(true);
    }}>
      <div className="circle"></div>
      <div className="info">
        <p className="end"><strong>{props.endereco_loja}</strong> | {props.cep_loja} {props.cidade_loja}</p>
        <span className="time-retirada">{props.tempo_retirada}</span>
        <span className="distancia">{props.distancia_loja}</span>
      </div>
    </StyleComponent>
  );
}
 