import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Checkbox, Select } from 'antd';
import Helmet from 'react-helmet';

import InputMask from "react-input-mask";
import PasswordStrengthMeter from '../cadastro/PasswordStrengthMeter';

import PagamentoAction from '../../components/pagamentoAction';

import { StyleModalSenha } from '../login/style';

import { ContainerGlobal } from '../../Style/global';

import {  StyleCheckout, 
          BoxWhite, 
          LoginUsuario,
          ResumoUsuarioLogado,
          BotaoEditarUser,
          CadastroUsuario,
          EntregaPadrao,
          PagamentoPadrao,
          ButtonConfirmarCompra,
          ModuloEntrega,
          CadastroEntrega,
          OpcoesEntrega,
          PegarNaLoja,
          OptStyle,
          EntregaEscolhida,
          BoxCupom,
          EnderecoEntrega,
          FluxoPegaNaLoja,
          SelectLojaProxima,
          SelectDemaisLocal,
          BotaoConcluirEnd,
          ResumoEntrega,
          BotaoEditarEnd,
          ContentPagamento,
          HeaderCheckout,
          EtapaMeuCarrinho,
          EtapaIdentificacao,
          EtapaPagamento,
          EtapaObrigado } from './styles';

import FooterCompra from '../../components/footer/area-compra';
import ComponentSexo from '../../components/Sexo';
import CardProduto from './card-prod';
import Loja from './end-loja';

//Images & Icons

import IconUser from '../../images/icon-user-checkout.svg';
import IconEntrega from '../../images/icon-entrega-checkout.svg';
import IconPagamento from '../../images/icon-pagamento-checkout.svg';
import IconFacebook from '../../images/facebook.svg';
import IconGoogle from '../../images/googleLogo.svg';
import ViewPassword from '../../images/view-password.svg';
import IconEditar from '../../images/icon-editar-blue.svg';
import IconCart from '../../images/icon-cart-blue.svg';
import ImageProd from '../../images/img-prod-resumo.jpg';
import IconCupom from '../../images/icon-cupom-blue.svg';
import IconHome from '../../images/icon-home-blue.svg';
import IconPin from '../../images/icon-pin-blue.svg';
import ArrowSelect from '../../images/arrow-select-gray.svg';
import bagWhite from '../../images/bag-white.svg';
import Close from '../../images/close.svg';
import IconeOk from '../../images/icone-checkado.svg';
import LogoCheckout from '../../images/new-logo.svg';
import CheckoutCheckout from '../../images/icon-cart-checkout.svg';
import CheckoutUser from '../../images/icon-ident-checkout.svg';
import IconCheckoutPagamento from '../../images/icon-cash-checkout.svg';
import IconObrigado from '../../images/icon-obrigado-checkout.svg';
import IconCadeado from '../../images/icon-cadeado.svg';


export default function ComponentCheckout() {
  // Campos do Login
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // Campos do Cadastro
  const [nome, setNome] = useState('');
  const [cpf, setCpf] = useState('');
  const [data, setData] = useState('');
  const [celular, setCelular] = useState('');
  const [telefone, setTelefone] = useState('');
  const [passwordRegister, setPasswordRegister] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [cep_destinatario, setCepDestinatario] = useState('');

  // Estado do fluxo do checkout
  const [visibleLogin, setVisibleLogin] = useState(true); 
  const [visibleResumo, setVisibleResumo] = useState(false);
  const [visibleRegister, setVisibleRegister] = useState(false);
  const [visibleEditar, setVisibleEditar] = useState(false);
  const [MsgDefaultEntrega, setMsgDefaultEntrega] = useState(true);
  const [MsgDefaultPagamento, setMsgDefaultPagamento] = useState(true);
  const [visibleModEntrega, setModEntrega] = useState(false);
  const [visibleOpcoesEntrega, setVisibleOpcoesEntrega] = useState(true);
  const [visibleOptEscolhida, setVisibleOptEscolhida] = useState(false);
  const [visibleCadastroEndereco, setVisibleCadastroEndereco] = useState(false);
  const [visibleBoxCupom, setVisibleBoxCupom] = useState(false);
  const [visibleEnderecoEntrega, setVisibleEnderecoEntrega] = useState(false);
  const [visibleFluxoLoja, setVisibleFluxoLoja] = useState(false);
  const [visibleResumoEntrega, setVisibleResumoEntrega] = useState(false);
  const [visibleBtnEditarEnd, setVisibleBtnEditarEnd] = useState(false);
  const [visiblePagamentoCompleto, setVisiblePagamentoCompleto] = useState(false);
  const [recuperarSenha, setRecuperarSenha] = useState(false);
  const [modalSenha, setModalSenha] = useState(false);

  // Estado do botão finalizar compra
  const [DisabledBtn, setDisabledBtn] = useState(true);
  const [visibleBtnConcluir, setVisibleBtnConcluir] = useState(false);


  // Estado dos selects
  const [expandSelectLojas, setExpandSelectLojas] = useState(true);
  const [expandSelectLocal, setExpandSelectLocal] = useState(false);

  // Estados das etapas do Header

  const [carrinho, setCarrinho] = useState(true);
  const [identificacao, setIdentificacao] = useState(true);
  const [pagamento, setPagamento] = useState(false);
  const [obrigado, setObrigado] = useState(false);

  const [expandGrid, setExpandGrid] = useState(false);

  const { Option } = Select;

  function handleShowpassword(e) {
    let input = e.target.parentElement.parentElement.firstElementChild;

    if(input.getAttribute('type') === 'password') {
      input.setAttribute('type', 'text');
    } else {
      input.setAttribute('type', 'password');
    }
  }

  function handleChange(value) {
    console.log(`selected ${value}`);
  }

  function onChangeCheck(e) {
    console.log(`checked = ${e.target.checked}`);
  }

  return ( 
    <>
      <Helmet>
          <link rel="canonical" href="http://www.gazin.com.br"/>
          <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
          <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
          <meta name="author" content="Luis Alfredo Rosar" />
          <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
          <meta charSet="utf-8" />
          <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
      </Helmet>
      <HeaderCheckout>
        <div className="container">
          <Link to="/">
            <img src={LogoCheckout} alt=""/>
          </Link>
          <ul>
            <EtapaMeuCarrinho active={carrinho}>
              <img src={CheckoutCheckout} alt=""/>
              <span>Meu Carrinho</span>
            </EtapaMeuCarrinho>
            <EtapaIdentificacao active={identificacao}>
              <img src={CheckoutUser} alt=""/>
              <span>Identificação</span>
            </EtapaIdentificacao>
            <EtapaPagamento active={pagamento}>
              <img src={IconCheckoutPagamento} alt=""/>
              <span>Pagamento</span>
            </EtapaPagamento>
            <EtapaObrigado active={obrigado}>
              <img src={IconObrigado} alt=""/>
              <span>Obrigado!</span>
            </EtapaObrigado>
          </ul>
          <div className="seguro">
            <div className="icon">
              <img src={IconCadeado} alt=""/>
            </div>
            <span>Ambiente <strong>seguro!</strong></span>
          </div>
        </div>
      </HeaderCheckout>

      <StyleCheckout>
        <ContainerGlobal expand_grid={expandGrid}>
          <div className="geral">
            <div className="left">
              <BoxWhite>
                <div className="title">
                  <div className="left-title">
                    <img src={IconUser} alt=""/>
                    <h3>Identifique-se</h3>
                  </div>
                  <BotaoEditarUser visible={visibleEditar} to="/conta">
                    Editar <img src={IconEditar} alt=""/>
                  </BotaoEditarUser>
                </div>
                <LoginUsuario visible={visibleLogin}>
                  <div className="login-socials">
                    <p>Você pode acessar por suas redes sociais</p>
                    <div className="btns">
                      <button>
                        <div className="icon">
                          <img src={IconFacebook} alt=""/>
                        </div>
                        <span>Acessar com o <strong>Facebook</strong></span>
                      </button>
                      <button>
                        <div className="icon">
                          <img src={IconGoogle} alt=""/>
                        </div>
                        <span>Acessar com o <strong>Google</strong></span>
                      </button>
                    </div>
                  </div>
                  <div className="login">
                    <div className="form-group">
                      <label>E-mail*</label>
                      <input 
                        type="email" 
                        placeholder="Digite seu e-mail"
                        value={email}
                        onChange={e => setEmail(e.target.value)}  
                      />
                    </div>
                    <div className="form-group">
                      <label>Senha*</label>
                      <div className="input-password">
                        <input 
                          type="password" 
                          placeholder="Digite sua senha"
                          id="js-input-password"
                          value={password}
                          onChange={e => setPassword(e.target.value)}  
                        />
                        <button 
                          type="button"
                          onClick={handleShowpassword}
                        >
                          <img src={ViewPassword} alt=""/>
                        </button>
                      </div>
                    </div>
                    <div className="form-group">
                      <button 
                        type="button" 
                        className="btn-orange"
                        onClick={() => {
                          setVisibleLogin(false);
                          setVisibleResumo(true);
                          setVisibleEditar(true);
                          setModEntrega(true);
                          setMsgDefaultEntrega(false);
                        }}
                      >
                        Continuar
                      </button>
                    </div>
                    <div className="form-group">
                      <button 
                        type="button" 
                        className="btn-link"
                        onClick={() => {
                            setModalSenha(true);
                        }}
                      >Esqueci minha senha</button>
                    </div>
                  </div>
                  <div className="new-user">
                    <h3>Cliente novo? </h3>
                    <p>Faça seu cadastro para seguir o processo de compra</p>
                    <button 
                      type="button" 
                      className="btn-outline-orange"
                      onClick={() => {
                        setVisibleLogin(false);
                        setVisibleRegister(true);
                      }}
                      >cadastre-se</button>
                  </div>
                </LoginUsuario>
                <ResumoUsuarioLogado visibleRes={visibleResumo}>
                  <strong>Angélica Arakaki</strong>
                  <span>{email}</span>
                  <span>(00) 1234-5678 </span>
                </ResumoUsuarioLogado>
                <CadastroUsuario visibleRegister={visibleRegister}>
                  <div className="form-group">
                    <label htmlFor="">Nome completo*</label>
                    <input 
                      type="text" 
                      placeholder="Seu nome"
                      value={nome}
                      onChange={e => setNome(e.target.value)}  
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="">E-mail*</label>
                    <input 
                      type="text" 
                      placeholder="Seu e-mail"
                      value={email}
                      onChange={e => setEmail(e.target.value)}  
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="">CPF*</label>
                    <InputMask 
                        type="text"
                        mask="999.999.999-99"
                        maskPlaceholder=""
                        placeholder="Seu CPF"
                        value={cpf}
                        onChange={e => setCpf(e.target.value)}  
                    />
                  </div>
                  <div className="double-group">
                    <div className="form-group">
                      <label htmlFor="">Data de nascimento*</label>
                      <InputMask 
                          type="text"
                          mask="99/99/9999"
                          maskPlaceholder=""
                          placeholder="Data"
                          value={data}
                          onChange={e => setData(e.target.value)}  
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="">Nº celular*</label>
                      <InputMask 
                          type="text"
                          mask="(99)9 9999-9999"
                          maskPlaceholder=""
                          placeholder="Celular"
                          value={celular}
                          onChange={e => setCelular(e.target.value)}  
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="">Sexo</label>
                    <ComponentSexo/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="">Telefone</label>
                    <InputMask 
                        type="text"
                        mask="(99) 9999-9999"
                        maskPlaceholder=""
                        placeholder="Telefone"
                        value={telefone}
                        onChange={e => setTelefone(e.target.value)}  
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="">Senha*</label>
                    <div className="meter">
                      <div className="input-password">
                        <input 
                          type="password" 
                          placeholder="Digite sua senha"
                          className="js-input-password"
                          value={passwordRegister}
                          onChange={e => setPasswordRegister(e.target.value)}  
                        />
                        <button 
                          type="button"
                          onClick={handleShowpassword}
                        >
                          <img src={ViewPassword} alt=""/>
                        </button>
                      </div>
                      <PasswordStrengthMeter password={passwordRegister} />
                    </div>
                  </div>
                  <div className="form-group">
                    <label htmlFor="">Confirmar senha*</label>
                    <div className="input-password">
                      <input 
                        type="password" 
                        placeholder="Digite sua senha"
                        className="js-input-password"
                        value={passwordConfirm}
                        onChange={e => setPasswordConfirm(e.target.value)}  
                      />
                      <button 
                        type="button"
                        onClick={handleShowpassword}
                      >
                        <img src={ViewPassword} alt=""/>
                      </button>
                    </div>
                  </div>
                  <div className="form-group">
                    <Checkbox onChange={onChangeCheck}>Desejo receber ofertas por e-mail?</Checkbox>
                  </div>
                  <div className="form-group">
                    <button 
                      type="button" 
                      className="btn-orange"
                      onClick={() => {
                        setVisibleResumo(true);
                        setVisibleEditar(true);
                        setVisibleRegister(false);
                        setVisibleCadastroEndereco(true);
                        setMsgDefaultEntrega(false);
                      }}>Cadastrar</button>
                  </div>
                </CadastroUsuario>
              </BoxWhite>
              <BoxWhite>
                <div className="title">
                  <div className="left-title">
                    <img src={IconEntrega} alt=""/>
                    <h3>Entrega</h3>
                  </div>
                  <BotaoEditarEnd visible={visibleBtnEditarEnd} to="/conta/enderecos">
                    Editar <img src={IconEditar} alt=""/>
                  </BotaoEditarEnd>
                </div>
                <EntregaPadrao visible={MsgDefaultEntrega}>Aguardando preenchimento de dados</EntregaPadrao>
                <CadastroEntrega visible={visibleCadastroEndereco}>
                 <div className="form-group">
                   <label htmlFor="">Nome do destinatário*</label>
                   <input type="text" placeholder="nome"/>
                 </div>
                 <div className="form-group">
                   
                   <label htmlFor="">CEP*</label>
                   <InputMask 
                        type="text"
                        mask="99999-999"
                        maskPlaceholder=""
                        placeholder="Número do CEP"
                        value={cep_destinatario}
                        onChange={e => setCepDestinatario(e.target.value)}  
                    />
                   <a href="" class="btn-cep">Não seu meu CEP</a>
                 </div>
                 <div className="form-group">
                   <label htmlFor="">Endereço*</label>
                   <input type="text" placeholder="Rua"/>
                 </div>
                 <div className="form-group">
                   <label htmlFor="">Complemento</label>
                   <input type="text" placeholder="Opcional"/>
                 </div>
                 <div className="form-group">
                   <label htmlFor="">Bairro</label>
                   <input type="text" placeholder="Nome do bairro"/>
                 </div>
                 <div className="double-group">
                  <div className="form-group">
                    <label htmlFor="">Cidade</label>
                    <input type="text" placeholder="Cidade"/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="">Estado*</label>
                    <Select defaultValue="disabled" style={{ width: '100%' }} onChange={handleChange}>
                      <Option value="disabled" disabled>
                        Estado
                      </Option>
                      <Option value="SP">SP</Option>
                      <Option value="MG">MG</Option>
                      <Option value="RJ">RJ</Option>
                    </Select>
                  </div>
                 </div>
                 <div className="form-group">
                      <button class="btn-orange" onClick={() => {
                        setVisibleCadastroEndereco(false);
                        setVisibleLogin(false);
                        setVisibleResumo(true);
                        setVisibleEditar(true);
                        setModEntrega(true);
                        setMsgDefaultEntrega(false);
                      }}>entregar neste endereço</button>
                 </div>
                </CadastroEntrega>
                <ResumoEntrega  visible={visibleResumoEntrega}>
                    <div className="item">
                      <ul>
                        <li>Avenida São José, 229 - Centro</li>
                        <li>Alfenas MG</li>
                        <li>CEP: 37130-115</li>
                      </ul>
                    </div>
                    <div className="item">
                      <h4>Opções de entrega</h4>
                      <div className="desc-entrega">
                        <div className="left-item">
                          <strong>Convecional</strong>
                          <span>até 8 dias úteis </span>
                        </div>
                        <span>R$ 15,99</span>
                      </div>
                    </div>
                </ResumoEntrega>
                <ModuloEntrega visible={visibleModEntrega}>
                  <div className="area-cep">
                    <div className="form-group">
                      <label htmlFor="">CEP*</label>
                      <input type="text" placeholder="Número do CEP"/>
                      <a href="" className="btn-find-cep">Não sei meu CEP</a>
                    </div>
                  </div>
                  <OpcoesEntrega visible={visibleOpcoesEntrega}>
                    <h3>Opções de entrega</h3>
                    <OptStyle onClick={() => {
                      setVisibleOpcoesEntrega(false);
                      setVisibleOptEscolhida(true);
                      setVisibleBoxCupom(true);
                      setVisibleEnderecoEntrega(true);
                    }}>
                      <div className="left-opt">
                        <div className="circle"></div>
                        <div className="info">
                          <span>Convencional</span>
                          <p>até 8 dias úteis</p>
                        </div>
                      </div>
                      <h3>R$ 15,99</h3>
                    </OptStyle>
                    <OptStyle onClick={() => {
                      setVisibleOpcoesEntrega(false);
                      setVisibleOptEscolhida(true);
                      setVisibleBoxCupom(true);
                      setVisibleEnderecoEntrega(true);
                    }}>
                      <div className="left-opt">
                        <div className="circle"></div>
                        <div className="info">
                          <span>Rápida</span>
                          <p>até 8 dias úteis</p>
                        </div>
                      </div>
                      <h3>R$ 15,99</h3>
                    </OptStyle>
                    <OptStyle onClick={() => {
                      setVisibleOpcoesEntrega(false);
                      setVisibleOptEscolhida(true);
                      setVisibleBoxCupom(true);
                      setVisibleEnderecoEntrega(true);
                    }}>
                      <div className="left-opt">
                        <div className="circle"></div>
                        <div className="info">
                          <span>Agendada</span>
                          <p>à partir de 15 de dezembro</p>
                        </div>
                      </div>
                      <h3>R$ 15,99</h3>
                    </OptStyle>
                    <PegarNaLoja onClick={() =>{
                        setVisibleOpcoesEntrega(false);
                        setVisibleFluxoLoja(true);
                        setVisibleBoxCupom(true);
                        setVisibleBtnConcluir(true);
                    }}>
                      <div className="left-opt">
                        <div className="circle"></div>
                        <div className="info">
                          <span>Retirar na loja</span>
                          <p>Recomendado</p>
                        </div>
                      </div>
                      <h3>Grátis</h3>
                    </PegarNaLoja>
                  </OpcoesEntrega>
                  <EntregaEscolhida visible={visibleOptEscolhida}>
                      <div className="title">
                        <h3>Opções de entrega</h3>
                        <button 
                          type="button"
                          onClick={() => {
                            setVisibleOpcoesEntrega(true);
                            setVisibleOptEscolhida(false);
                            setVisibleBoxCupom(false);
                            setVisibleEnderecoEntrega(false);
                          }}
                        >
                          <img src={IconEditar} alt=""/> Editar</button>
                      </div>
                      <OptStyle checked={true}>
                        <div className="left-opt">
                          <div className="circle"></div>
                          <div className="info">
                            <span>Convencional</span>
                            <p>até 8 dias úteis</p>
                          </div>
                        </div>
                      <h3>R$ 15,99</h3>
                    </OptStyle>
                  </EntregaEscolhida>
                  <FluxoPegaNaLoja visible={visibleFluxoLoja}>
                    <div className="title-loja">
                      <h3>Opções de entrega</h3>
                      <button onClick={() => {
                        setVisibleOpcoesEntrega(true);
                        setVisibleFluxoLoja(false);
                        setVisibleBoxCupom(false);
                        setVisibleBtnConcluir(false);
                      }}><img src={IconEditar} alt=""/> Editar</button>
                    </div>
                    <OptStyle checked={true}>
                      <div className="left-opt">
                        <div className="circle"></div>
                        <div className="info">
                          <span>Retirar na loja</span>
                          <p>Recomendado</p>
                        </div>
                      </div>
                      <h3>Grátis</h3>
                    </OptStyle>
                    <div className="box-dados">
                      <h3>Entrega 02 de 02 por Gazin</h3>
                      <ul className="prods">
                        <li>01 IPhone 8 64GB Cinza Espacial Tela 4.7"</li>
                        <li>01 IPhone 8 64GB Cinza Espacial Tela 4.7"</li>
                      </ul>
                      <h3>Escolha a loja mais próxima a você para fazer a retirada</h3>
                      <span className="green">Frete grátis</span>

                      <SelectLojaProxima 
                        expand={expandSelectLojas}
                        onClick={() => {
                          setExpandSelectLojas(true);
                          setExpandSelectLocal(false);
                        }}
                      >
                        <div className="topo-select">
                          <div className="left-select">
                            <img src={IconPin} alt=""/>
                            <span>Lojas próximas</span>
                          </div>
                          <img src={ArrowSelect} alt="" className="arrow"/>
                        </div>
                        <div className="all-end">
                          <Loja
                            endereco_loja="Rua Rui Barbosa, 2611"
                            cep_loja="79002-365"
                            cidade_loja="Campo Grande"
                            tempo_retirada="Retire em 2 horas após a aprovação da compra. **"
                            distancia_loja="0.95 km"
                          />
                          <Loja
                            endereco_loja="Av. Pedro Manvailer, 2543"
                            cep_loja="79990-000"
                            cidade_loja="Campo Grande"
                            tempo_retirada="A partir de 2 dias úteis*"
                            distancia_loja="1 km"
                          />
                          <Loja
                            endereco_loja="Av. Pedro Manvailer, 2543"
                            cep_loja="79990-000"
                            cidade_loja="Campo Grande"
                            tempo_retirada="A partir de 2 dias úteis*"
                            distancia_loja="1 km"
                          />
                        </div>
                      </SelectLojaProxima>
                      <SelectDemaisLocal 
                        expand={expandSelectLocal}
                        onClick={() => {
                          setExpandSelectLojas(false);
                          setExpandSelectLocal(true);
                        }}
                      >
                          <div className="topo-select">
                            <div className="left-select">
                              <img src={IconPin} alt=""/>
                              <span>Demais locais</span>
                            </div>
                            <img src={ArrowSelect} alt="" className="arrow"/>
                          </div>
                          <div className="cont-localidades">
                            <p>Escolha abaixo a loja para retirada</p>
                            <div className="form-group">
                              <label htmlFor="">Estado</label>
                              <select>
                                <option value="">Estado</option>
                                <option value="MG">MG</option>
                                <option value="MG">SP</option>
                              </select>
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Cidade</label>
                              <select>
                                <option value="">Cidade</option>
                                <option value="Cidade 01">Cidade 01</option>
                                <option value="Cidade 02">Cidade 02</option>
                              </select>
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Bairro</label>
                              <select>
                                <option value="">Bairro</option>
                                <option value="Bairro 01">Bairro 01</option>
                                <option value="Bairro 02">Bairro 02</option>
                              </select>
                            </div>
                            <div className="form-group">
                              <label htmlFor="">Loja</label>
                              <select>
                                <option value="">Loja</option>
                                <option value="Bairro 01">Loja 01</option>
                                <option value="Bairro 02">Loja 02</option>
                              </select>
                            </div>
                          </div>
                          
                      </SelectDemaisLocal>
                      <div className="dados-user">
                        <div className="form-group">
                          <Checkbox onChange={onChangeCheck}>Os produtos serão retirados por outra pessoa</Checkbox>
                        </div>
                        <div className="form-group">
                          <label htmlFor="">Nome completo</label>
                          <input type="text" placeholder="Seu nome"/>
                        </div>
                        <div className="form-group">
                          <label htmlFor="">RG ou CNH</label>
                          <input type="text" placeholder="Seu CNPJ"/>
                        </div>
                      </div>
                    </div>
                  </FluxoPegaNaLoja>
                  <BoxCupom visible={visibleBoxCupom}>
                    <div className="title-box">
                      <img src={IconCupom} alt=""/>
                      <div className="txt">
                        <h3>Possui cupom ou vale?</h3>
                        <p>Digite o código do seu cupom ou vale</p>
                      </div>
                    </div>
                    <div className="input">
                      <div className="form-group">
                        <input type="text" placeholder="Digite o código"/>
                      </div>
                      <button type="button">ENVIAR</button>
                    </div>
                  </BoxCupom>
                  <EnderecoEntrega visible={visibleEnderecoEntrega}>
                    <h4>Endereço de entrega</h4>
                    <p>Avenida São José, 229 - Centro</p>
                    <span>Alfenas MG</span>
                    <Link to="/conta/enderecos"><img src={IconHome} alt=""/> Alterar endereço de entrega</Link>
                    <div className="form-group">
                      <label htmlFor="">Destinatário*</label>
                      <input type="text" placeholder="Nome completo"/>
                    </div>
                    <button 
                      type="button" 
                      class="btn-orange"
                      onClick={() => {
                        setModEntrega(false);
                        setVisibleResumoEntrega(true);
                        setVisibleBtnEditarEnd(true);
                        setExpandGrid(true);
                        setMsgDefaultPagamento(false);
                        setVisiblePagamentoCompleto(true);
                        setDisabledBtn(false);
                        setPagamento(true);
                      }}
                    >entregar neste endereço</button>
                  </EnderecoEntrega>
                  <BotaoConcluirEnd 
                    visible={visibleBtnConcluir}
                    onClick={() => {
                      setModEntrega(false);
                      setVisibleResumoEntrega(true);
                      setVisibleBtnEditarEnd(true);
                      setExpandGrid(true);
                      setMsgDefaultPagamento(false);
                      setVisiblePagamentoCompleto(true);
                      setDisabledBtn(false);
                      setPagamento(true);
                    }}
                  >Concluir</BotaoConcluirEnd>
                </ModuloEntrega>
              </BoxWhite>
              <BoxWhite>
                <div className="title">
                  <div className="left-title">
                    <img src={IconPagamento} alt=""/>
                    <h3>Pagamento</h3>
                  </div>
                </div>
                <PagamentoPadrao visible={MsgDefaultPagamento}>Aguardando preenchimento de dados</PagamentoPadrao>
                <ContentPagamento visible={visiblePagamentoCompleto}>
                  <PagamentoAction />
                </ContentPagamento>
                
              </BoxWhite>
            </div>
            <div className="right">
              <BoxWhite>
                <div className="title">
                  <div className="left-title">
                    <img src={IconCart} alt=""/>
                    <h3>Resumo do pedido</h3>
                  </div>
                </div>
                <div className="all-prods">
                    <CardProduto 
                      image={ImageProd}
                      nome_produto='Phone 8 64GB Cinza Espacial Tela 4.7"'
                      qtd='01'
                      valor='3.999,00'
                    />
                    <CardProduto 
                      image={ImageProd}
                      nome_produto='Phone 8 64GB Cinza Espacial Tela 4.7"'
                      qtd='01'
                      valor='3.999,00'
                    />
                </div>
                <ul className="list-valores">
                  <li>
                    <span>Subtotal</span>
                    <h3>R$ 7.998,00</h3>
                  </li>
                  <li>
                    <span>Descontos</span>
                    <h3>R$ 0,00</h3>
                  </li>
                  <li>
                    <span>Total</span>
                    <div className="total">
                      <h2>R$ 7.998,00</h2>
                      <p>10 x de R$ 399,90 s/ juros</p>
                    </div>
                  </li>
                </ul>
                <ButtonConfirmarCompra disabled={DisabledBtn}>finalizar compra</ButtonConfirmarCompra>
                <Link to="/carrinho" class="btn-back-cart">Voltar para o carrinho</Link>
              </BoxWhite>
            </div>
          </div>
        </ContainerGlobal>
      </StyleCheckout>

      <footer>
        <div className="container">
          <FooterCompra />
        </div>
      </footer>

      <StyleModalSenha recuperar_senha={recuperarSenha} modal_senha={modalSenha}>
          <div className="overlay"></div>
          <div className="box">
              <div className="topo">
                  <img src={bagWhite} alt=""/>
                  <h3>Esqueceu sua senha?</h3>
                  <button className="close" onClick={() => {
                      setRecuperarSenha(false);
                      setModalSenha(false);
                  }}> <img src={Close} alt=""/> </button>
              </div>
              <div className="body-modal">
                  <div className="recuperar-inicio">
                      <p>Identifique-se para receber um e-mail com as instruções e o link para criar uma nova senha.</p>
                      <form>
                          <div className="form-group">
                              <label>E-mail</label>
                              <input type="email" placeholder="Ex.: manuel@email.com"/>
                          </div>
                          <div className="form-group">
                              <button type="button" onClick={() => setRecuperarSenha(true)}>enviar senha</button>
                          </div>
                      </form>
                  </div>
                  <div className="resposta">
                      <img src={IconeOk}/>
                      <p>Solicitação enviada com sucesso. Verfique seu e-mail para fazer uma nova senha. </p>
                  </div>
              </div>
          </div>
      </StyleModalSenha>
    </>
    
  );
}
 