import styled from 'styled-components';

import { Link } from 'react-router-dom';

import ArrowList from '../../images/arrow-list.svg';
import CaretSelect from '../../images/caret-select.svg';

export const HeaderCheckout = styled.header`
    background: linear-gradient(87.02deg, #5C33FF -4.51%, #3399FF 104.58%);
    height: 100px;
    display: flex;
    align-items: center;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 50;
  .container {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
  ul {
    display: flex;
    margin-bottom: 0px;
    li {
      position: relative;
      display: flex;
      align-items: center;
      margin-left: 100px;
      &:after {
        content: "";
        background: url(${ArrowList}) no-repeat center center;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 1px;
        right: -55px;
      }
      &:last-child {
        &:after {
          display: none;
        }
      }
      &:first-child {
        margin-left: 0px;
      }
      img {
        margin-right: 9px;
      }
      span {
        color: #FFFFFF;
        font-weight: 500;
        font-size: 14px;
      }
    }
  }
  .seguro {
    display: flex;
    align-items: center;
    .icon {
      width: 36px;
      height: 36px;
      border-radius: 50%;
      background: rgba(255, 255, 255, 0.1);
      display: flex;
      align-items: center;
      justify-content: center;
      margin-right: 15px;
    }
    span {
      display: block;
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
      color: #FFFFFF;
      max-width: 65px;
      strong {
        display: block;
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        color: #FFFFFF;
      }
    }
  }
  @media(max-width: 1200px) {
    height: 85px;
    ul {
      li {
        span {
          display: none;
        }
      }
    }
  }
  @media(max-width: 768px) {
    .seguro {
      span {
        display: none;
      }
    }
  }
  @media(max-width: 480px) {
    height: 64px;
    a {
      width: 100px;
      display: block;
      img {
        max-width: 100%;
      }
    }
    ul {
      li {
        margin-left: 25px;
        &:after {
          right: -20px;
        }
      }
    }
    .seguro {
      display: none;
    }
  }
`;

export const EtapaMeuCarrinho = styled.li`
  opacity: ${props => props.active ? '1' : '0.4'};
`;

export const EtapaIdentificacao = styled.li`
    opacity: ${props => props.active ? '1' : '0.4'};
`;

export const EtapaPagamento = styled.li`
    opacity: ${props => props.active ? '1' : '0.4'};
`;

export const EtapaObrigado = styled.li`
    opacity: ${props => props.active ? '1' : '0.4'};
`;

export const BoxWhite = styled.div`
  padding: 30px 32px;
  background-color: #FFFFFF;
  border-radius: 8px;
  @media(max-width: 480px) {
    padding: 20px;
  }
`;

export const LoginUsuario = styled.div`
  display: ${props => props.visible ? "block" : "none"};
`;

export const CadastroUsuario = styled.div`
  display: ${props => props.visibleRegister ? 'block' : 'none'};
`;

export const BotaoEditarUser = styled(Link)`
  display: ${props => props.visible ? 'flex' : 'none'};
  align-items: center;
  color: #0D71F0;
  font-weight: 500;
  font-size: 14px;
  img {
    margin-left: 8px;
  }
`;

export const ResumoUsuarioLogado = styled.div`
  display: ${props => props.visibleRes ? "block" : "none"};
  strong {
    display: block;
    font-weight: bold;
    font-size: 16px;
    line-height: 27px;
    color: #646981;
    margin-bottom: 6px;
  }
  span {
    font-size: 16px;
    line-height: 23px;
    color: #646981;
    display: block;
    margin-bottom: 6px;
    &:last-child {
      margin-bottom: 0px;
    }
  }
`;

export const StyleCheckout = styled.section`
  margin-top: 100px;
  background-color: #F6F6F8;
  padding-top: 60px;
  padding-bottom: 136px;
  .ant-select-arrow {
    display: none !important;
  }
  .geral {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    .btn-outline-orange {
      width: 100%;
      height: 56px;
      background-color: transparent;
      border: 2px solid #6BB70B;
      border-radius: 6px;
      font: 600 16px/1 'Inter';
      font-weight: 600;
      text-align: center;
      letter-spacing: -0.114286px;
      text-transform: uppercase;
      color: #6BB70B;
      transition: all .3s;
      &:hover {
        background-color: #6BB70B;
        color: #FFF;
        transition: all .3s;
      }
    }
    .left {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-gap: 32px;
      align-items: flex-start;
      flex: 1;
      max-width: 840px;
      div${BoxWhite} {
        &:first-child {
          grid-area: 1 / 2 / 4;
          grid-row-start: 1;
          grid-column-start: 1;
          grid-column-end: 1;
        }
      }
    }
    .right {
      flex: 1;
      max-width: 404px;
    }
    .title {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-bottom: 24px;
      .left-title {
        display: flex;
        align-items: center;
      }
      img {
        margin-right: 18px;
      }
      h3 {
        font-weight: 600;
        font-size: 18px;
        line-height: 25px;
        margin-bottom: 0;
        color: #363843;
      }
    }
    .login-socials {
      margin-bottom: 16px;
      p {
        font-size: 14px;
        line-height: 17px;
        color: #646981;
        margin-bottom: 17px;
      }
      .btns {
        display: flex;
        justify-content: space-between;
        button {
          display: flex;
          align-items: center;
          justify-content: center;
          background-color: rgba(220, 221, 227, 0.3);
          border-radius: 6px;
          width: 161px;
          height: 57px;
          .icon {
            width: 31px;
            height: 31px;
            background-color: #FFFFFF;
            border-radius: 4.2px;
            display:flex;
            align-items: center;
            justify-content: center;
            margin-right: 15px;
          }
          span {
            text-align: left;
            display: block;
            flex: 1;
            font-size: 14px;
            line-height: 16px;
            color: #646981;
            max-width: 93px;
            strong {
              font-weight: bold;
              font-size: 14px;
              line-height: 16px;
              color: #646981;
            }
          }
        }
      }
    }
    .login {
      border-bottom: 1px solid #DCDDE3;
      padding-bottom: 24px;
      margin-bottom: 24px;
    }
    .new-user {
      h3 {
        font-weight: 600;
        font-size: 18px;
        line-height: 25px;
        color: #363843;
        margin-bottom: 9px;
      }
      p {
        max-width: 299px;
        font-size: 15px;
        line-height: 21px;
        color: #646981;
        margin-bottom: 32px;
      }
    }
    .double-group {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-gap: 17px;
      margin-bottom: 16px;
      .form-group {
        margin-bottom: 0px;
      }
    }
    .form-group {
      margin-bottom: 16px;
      &:last-child {
        margin-bottom: 0px;
      }
      label {
        display: block;
        font-weight: 500;
        font-size: 14px;
        line-height: 24px;
        color: #363843;
        margin-bottom: 5px;
      }
      input[type=text],
      input[type=email],
      input[type=password] {
        width: 100%;
        height: 45px;
        background-color: #FFFFFF;
        border: 1px solid #DCDDE3;
        border-radius: 6px;
        padding: 0px 13px;
        font-size: 14px;
        transition: all .3s;
        &::placeholder {
          color: #A0A4B3;
        }
        &:focus {
          border: 1px solid #0D71F0;
          transition: all .3s;
        }
      }
      .btn-cep {
        display: flex;
        justify-content: flex-end;
        color: #A0A4B3;
        font-size: 13px;
        margin-top: 5px;
      }
      .ant-select-selector {
        background: url(${CaretSelect}) no-repeat #FFFFFF right 14px center;
        border: 1px solid #DCDDE3;
        border-radius: 6px;
        padding: 0px 13px;
        height: 45px;
        .ant-select-selection-item {
          display: flex;
          align-items: center;
          color: #A0A4B3;
        }
      }
      .ant-radio-group {
        width: 100%;
        display: flex;
        align-items: center;
      }
      .password-strength-meter-label {
        margin-left: 10px;
        font-size: 13px !important;
      }
      .input-password {
        position: relative;
        button {
          position: absolute;
          top: 15px;
          right: 19px;
        }
      }
      .btn-orange {
        width: 100%;
        height: 56px;
        background-color: #6BB70B;
        border: 2px solid #6BB70B;
        border-radius: 6px;
        font: 600 16px/1 'Inter';
        font-weight: 600;
        text-align: center;
        letter-spacing: -0.114286px;
        text-transform: uppercase;
        color: #FFFFFF;
        transition: all .3s;
        &:hover {
          background-color: transparent;
          color: #6BB70B;
          transition: all .3s;
        }
      }
      .btn-link {
        display: table;
        text-align: center;
        margin: 0 auto;
        font-weight: 500;
        font-size: 14px;
        color: #646981;
        transition: all .3s;
        &:hover {
          color: #6BB70B;
          transition: all .3s;
        }
      }
    }
  }
  .all-prods {
    margin-top: 35px;
  }
  .list-valores {
    margin-top: 31px;
    li {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-bottom: 24px;
      &:last-child {
        align-items: flex-start;
        margin-bottom: 0px;
      }
      span {
        color: #646981;
        font-size: 14px;
      }
      h3 {
        color: #646981;
        font-weight: 500;
        font-size: 16px;
        line-height: 1;
        margin-bottom: 0;
      }
      h2 {
        line-height: 1;
        color: #0D71F0;
        font-weight: bold;
        font-size: 20px;
        margin-bottom: 5px;
        text-align: right;
      }
      p {
        color: #646981;
        font-size: 14px;
        font-weight: 500;
      }
    }
  }
  .btn-back-cart {
    display: block;
    text-align: center;
    color: #646981;
    font-weight: 500;
    font-size: 14px;
    transition: all .3s;
    &:hover {
      color: #0D71F0;
      transition: all .3s;
    }
  }
  @media(max-width: 1300px) {
    padding: 30px 0px;
    .geral {
      .left {
        grid-gap: 20px;
        max-width: 828px;
      }
    }
  }
  @media(max-width: 1200px) {
    margin-top: 85px;
    .geral {
      flex-direction: column;
      align-items: flex-start;
      .left {
        max-width: 100%;
        width: 100%;
      }
      .right {
        max-width: 100%;
        width: 100%;
        margin-top: 30px;
      }
    }
  }
  @media(max-width: 480px) {
    margin-top: 64px;
    .geral {
      .left {
        div${BoxWhite} {
          width: 100% !important;
          margin-bottom: 20px;
        }
      }
      .right {
        margin-top: 0;
      }
      .login-socials {
        .btns {
          flex-direction: column;
          align-items: center;
          button {
            width: 100%;
            margin-bottom: 10px;
            &:last-child {
              margin-bottom: 0px;
            }
          }
        }
      }
      .new-user {
        h3 {
          text-align: center;
        }
        p {
          text-align: center;
        }
      }
    }
  }
`;

export const EntregaPadrao = styled.p`
  display: ${props => props.visible ? 'block' : 'none'};
  font-size: 15px;
  color: #646981;
`;

export const CadastroEntrega = styled.div`
  display: ${props => props.visible ? 'block' : 'none'};
`;

export const ModuloEntrega = styled.div`
  display: ${props => props.visible ? 'block' : 'none'};
  .btn-find-cep {
    display: block;
    text-align: right;
    font-weight: 500;
    font-size: 14px;
    line-height: 24px;
    color: #A0A4B3;
    margin-top: 9px;
  }
`;

export const OpcoesEntrega = styled.div`
  display: ${props => props.visible ? 'block' : 'none'};
`;

export const PegarNaLoja = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding: 24px 0px;
  padding-bottom: 0;
  cursor: pointer;
  &:first-child {
    padding-top: 0;
  }
  .left-opt {
    display: flex;
    align-items: flex-start;
    .circle {
      width: 20px;
      height: 20px;
      margin-right: 16px;
      border: ${props => props.checked ? '1px solid #0D71F0' : '1px solid rgba(160, 164, 179, 0.4)'};
      border-radius: 50%;
      background-color: ${props => props.checked ? '#0D71F0' : 'transparent'};
      margin-top: 3px;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: all .3s;
      &:before {
        content: "";
        width: 10px;
        height: 10px;
        display: inline-block;
        background-color: #fff;
        transform: ${props => props.checked ? 'scale(1)' : 'scale(0)'};
        border-radius: 50%;
        transition: all .3s
      }
    }
    .info {
      max-width: 185px;
      span {
        font-size: 16px;
        line-height: 23px;
        color: #646981;
      }
      p {
        font-size: 14px;
        margin-bottom: 0;
        border: 1px solid #6BB70B;
        padding: 0px 8px;
        line-height: 26px;
        border-radius: 2px;
        color: #6BB70B;
      }
    }
  }
  h3 {
    margin-bottom: 0px;
    color: #646981;
  }
`;

export const PagamentoPadrao = styled.p`
  display: ${props => props.visible ? 'block' : 'none'};
  font-size: 15px;
  color: #646981;
`;

export const CardProduto = styled.div`
  display: flex;
  align-items: center;
  padding: 30px 0px;
  border-bottom: 1px solid #EFEFEF;
  &:first-child {
    padding-top: 0px;
  }
  .image {
    width: 85px;
    height: 85px;
    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 0px 1.15254px 2.59322px rgba(54, 56, 67, 0.11);
    background-color: #fff;
  }
  .info {
    flex: 1;
    margin-left: 34px;
    h4 {
      font-size: 15px;
      line-height: 24px;
      color: #646981;
      margin-bottom: 5px;
    }
    ul {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-bottom: 0px;
      li {
        span {
          color: #646981;
          font-size: 15px;
        }
      }
    }
  }
`;

export const ButtonConfirmarCompra = styled.button`
  width: 100%;
  height: 56px;
  background-color: ${props => props.disabled ? '#F6F6F8' : '#6BB70B'};
  border: ${props => props.disabled ? '2px solid #DCDDE3' : '2px solid #6BB70B'};
  border-radius: 8px;
  text-align: center;
  font: 600 16px/19px 'Inter';
  letter-spacing: -0.114286px;
  text-transform: uppercase;
  color: ${props => props.disabled ? '#A0A4B3' : '#fff'};
  pointer-events: ${props => props.disabled ? 'none' : 'all'};
  margin-bottom: 20px;
  transition: all .3s;
  &:hover {
    background-color: transparent;
    color: #6BB70B;
    transition: all .3s;
  }
`;

export const OptStyle = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding: 24px 0px;
  border-bottom: 1px solid #EFEFEF;
  cursor: pointer;
  &:first-child {
    padding-top: 0;
  }
  .left-opt {
    display: flex;
    align-items: flex-start;
    .circle {
      width: 20px;
      height: 20px;
      margin-right: 16px;
      border: ${props => props.checked ? '1px solid #0D71F0' : '1px solid rgba(160, 164, 179, 0.4)'};
      border-radius: 50%;
      background-color: ${props => props.checked ? '#0D71F0' : 'transparent'};
      margin-top: 3px;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: all .3s;
      &:before {
        content: "";
        width: 10px;
        height: 10px;
        display: inline-block;
        background-color: #fff;
        transform: ${props => props.checked ? 'scale(1)' : 'scale(0)'};
        border-radius: 50%;
        transition: all .3s
      }
    }
    .info {
      max-width: 185px;
      span {
        font-size: 16px;
        line-height: 23px;
        color: #646981;
      }
      p {
        color: #646981;
        font-size: 14px;
        margin-bottom: 0;
      }
    }
  }
  h3 {
    margin-bottom: 0px;
    color: #646981;
  }
`;

export const EntregaEscolhida = styled.div`
  display: ${props => props.visible ? 'block' : 'none'};
  margin-top: 24px;
  .title {
    h3 {
      font-weight: 600;
    }
    button {
      display: flex;
      align-items: center;
      color: #0D71F0;
      font-weight: 500;
      font-size: 14px;
    }
  }
  div${OptStyle} {
    padding: 0;
    border-bottom: none;
  }
`;

export const BoxCupom = styled.div`
  display: ${props  => props.visible ? 'block' : 'none'};
  margin: 32px 0px;
  background-color: #F6F6FA;
  border-radius: 4px;
  width: 100%;
  padding: 24px;
  .title-box {
    display: flex;
    align-items: flex-start;
    img {
      margin-right: 3px;
    }
    .txt {
      h3 {
        margin-bottom: 0;
        line-height: 1;
        font-weight: 600;
        font-size: 18px;
        margin-bottom: 3px;
      }
      p {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #646981;
      }
    }
  }
  .input {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .form-group {
      margin-bottom: 0px;
      max-width: 197px;
    }
    button {
      width: 104px;
      height: 45px;
      background-color: #6BB70B;
      border: 2px solid #6BB70B;
      border-radius: 6px;
      font-size: 14px;
      color: #FFFFFF;
      font-weight: 500;
      width: 90px;
      transition: all .3s;
      &:hover {
        background-color: transparent;
        color: #6BB70B;
        transition: all .3s;
      }
    }
  }
  @media(max-width: 480px) {
    .input {
      flex-direction: column;
      align-items: center;
      .form-group {
        max-width: 100%;
        width: 100%;
        margin-bottom: 10px;
      }
      button {
        width: 100%;
      }
    }
  }
`;

export const EnderecoEntrega = styled.div`
  display: ${props  => props.visible ? 'block' : 'none'};
  h4 {
    font-weight: 600;
    font-size: 16px;
    line-height: 141.52%;
    color: #363843;
  }
  p,
  span {
    font-size: 16px;
    line-height: 23px;
    color: #646981;
    margin-bottom: 0;
  }
  a {
    display: flex;
    align-items: center;
    margin-top: 16px;
    margin-bottom: 23px;
    color: #0D71F0;
    font-size: 14px;
    font-weight: 500;
    img {
      margin-right: 16px;
    }
  }
  .btn-orange {
    width: 100%;
    height: 56px;
    background-color: #6BB70B;
    border: 2px solid #6BB70B;
    border-radius: 6px;
    font: 600 16px/1 'Inter';
    font-weight: 600;
    text-align: center;
    letter-spacing: -0.114286px;
    text-transform: uppercase;
    color: #FFFFFF;
    margin-top: 32px;
    transition: all .3s;
    &:hover {
      background-color: transparent;
      color: #6BB70B;
      transition: all .3s;
    }
  }
`;

export const FluxoPegaNaLoja = styled.div`
  display: ${props  => props.visible ? 'block' : 'none'};
  margin-top: 24px;
  .title-loja {
    display: flex;
    align-items: center;
    justify-content: space-between;
    button {
      display: flex;
      align-items: center;
      font-weight: 500;
      color: #0D71F0;
      font-size: 14px;
      img {
        margin-right: 10px;
      }
    }
  }
  div${OptStyle} {
    padding-top: 20px;
    border-bottom: none;
    .left-opt {
      .info {
        p {
          font-size: 14px;
          margin-bottom: 0;
          border: 1px solid #6BB70B;
          padding: 0px 8px;
          line-height: 26px;
          border-radius: 2px;
          color: #6BB70B;
        }
      }
    }
  }
  .box-dados {
    background-color: #F6F6FA;
    border-radius: 4px;
    padding: 19px 16px;
    h3 {
      font-weight: 600;
      font-size: 16px;
      line-height: 141.52%;
      color: #363843;
    }
    .prods {
      margin-bottom: 10px;
      li {
        font-size: 14px;
        line-height: 27px;
        color: #646981;
      }
    }
    .green {
      display: block;
      color: #6BB70B;
    }
    .all-end {
      width: 100%;
      height: 241px;
      overflow-y: auto;
      padding: 0px 21px;
    }
  }
  .dados-user {
    margin-top: 28px;
    .ant-checkbox {
      &+ span {
        width: 100%;
        max-width: 100%;
        font-size: 12px;
        color: #646981;
      }
    }
  }
`;

export const SelectLojaProxima = styled.div`
  margin-top: 18px;
  background-color: #FFFFFF;
  border: 1px solid #DCDDE3;
  box-sizing: border-box;
  border-radius: 6px;
  width: 100%;
  height: ${props => props.expand ? 'auto' : '65px'};
  overflow: auto;
  padding-bottom: 17px;
  .topo-select {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0px 20px;
    width: 100%;
    height: 65px;
    cursor: pointer;
    .left-select {
      display: flex;
      align-items: center;
      img {
        margin-right: 19px;
      }
      span {
        font-weight: 600;
        color: #363843;
      }
    }
  }
`;

export const SelectDemaisLocal = styled.div`
  margin-top: 18px;
  background-color: #FFFFFF;
  border: 1px solid #DCDDE3;
  box-sizing: border-box;
  border-radius: 6px;
  width: 100%;
  height: ${props => props.expand ? 'auto' : '65px'};
  overflow: auto;
  padding-bottom: 17px;
  .topo-select {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0px 20px;
    width: 100%;
    height: 65px;
    cursor: pointer;
    .left-select {
      display: flex;
      align-items: center;
      img {
        margin-right: 19px;
      }
      span {
        font-weight: 600;
        color: #363843;
      }
    }
  }
  .cont-localidades {
    padding: 0px 20px;
    p {
      font-size: 16px;
      line-height: 24px;
      color: #363843;
    }
    select {
      width: 100%;
      height: 54px;
      border: 1px solid #DCDDE3;
      border-radius: 6px;
    }
  }
`;

export const BotaoConcluirEnd = styled.button`
  display: ${props  => props.visible ? 'block' : 'none'};
  margin-top: 20px;
  width: 100%;
  height: 56px;
  background-color: #6BB70B;
  border: 2px solid #6BB70B;
  border-radius: 6px;
  color: #FFFFFF;
  font-weight: 600;
  text-transform: uppercase;
  transition: all .3s;
  &:hover {
    background-color: transparent;
    color: #6BB70B;
    transition: all .3s;
  }
`;

export const ResumoEntrega = styled.div`
  display: ${props  => props.visible ? 'block' : 'none'};
  .item {
    border-bottom: 1px solid #EFEFEF;
    padding-bottom: 26px;
    margin-bottom: 24px;
    &:last-child {
      border-bottom: none;
      padding-bottom: 0;
    }
    ul {
      margin-bottom: 0px;
      li {
        color: #646981;
        font-size: 16px;
      }
    }
    h4 {
      line-height: 1;
      color: #646981;
      font-weight: 600;
      font-size: 16px;
      margin-bottom: 12px;
    }
    .desc-entrega {
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      .left-item {
        strong {
          display: block;
        }
      }
    }
  }
`;

export const BotaoEditarEnd = styled(Link)`
  display: ${props  => props.visible ? 'flex' : 'none'};
  font-weight: 500;
  font-size: 14px;
  line-height: 24px;
  color: #0D71F0;
  img {
    margin-left: 10px;
    margin-right: 0 !important;
  }
`;

export const ContentPagamento = styled.div`
  display: ${props  => props.visible ? 'block' : 'none'};
`;