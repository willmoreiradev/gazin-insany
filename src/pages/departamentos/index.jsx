import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import Header from '../../components/header';
import Categorias from '../../components/categoria';
import Breadcrumbs from '../../components/breadcrumbs';
import BannerPromocao from '../../components/banner-promocao';
import ListaProdutos from '../../components/produtos/lista-produtos';
import InformacoesDeEntrega from '../../components/info-entrega';
import InformacoesPagamentoOferta from '../../components/pagamento-oferta';
import FooterAreaPrimaria from '../../components/footer/area-primaria';
import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import ImgProduto01 from '../../images/produto-01.jpg';
import ImgProduto02 from '../../images/produto-02.jpg';
import ImgProduto03 from '../../images/produto-03.jpg';
import ImgProduto04 from '../../images/produto-04.jpg';

const produtos = [
  {
    indisponivel: false,
    porcetagem_oferta: "30%",
    caminho_img: ImgProduto01,
    nome_produto: "Smartphone Apple Iphone 6S 32GB",
    valor_de: "1489",
    valor_produto: "989",
    parcelas: '12',
    valor_parcelado: (989/12)
  },
  {
    porcetagem_oferta: "30%",
    indisponivel: true,
    caminho_img: ImgProduto02,
    nome_produto: "Monitor Gamer 23'' 1 ms 75Hz Ultra...",
  },
  {
    indisponivel: false,
    porcetagem_oferta: "12%",
    caminho_img: ImgProduto03,
    nome_produto: "Cadeira Charles Eames Wood Base ...",
    valor_de: "89,90",
    valor_produto: " 79,11",
    parcelas: '8',
    valor_parcelado: (89.90/8)
  },
  {
    indisponivel: false,
    caminho_img: ImgProduto04,
    nome_produto: "Micro-ondas Consul Espelhado - 20L ...",
    valor_produto: " 369,99",
    parcelas: '12',
    valor_parcelado: (369.99/12)
  },
  {
    indisponivel: false,
    porcetagem_oferta: "30%",
    caminho_img: ImgProduto01,
    nome_produto: "Smartphone Apple Iphone 6S 32GB",
    valor_de: "1489",
    valor_produto: "989",
    parcelas: '12',
    valor_parcelado: (989/12)
  },
  {
    porcetagem_oferta: "30%",
    indisponivel: true,
    caminho_img: ImgProduto02,
    nome_produto: "Monitor Gamer 23'' 1 ms 75Hz Ultra Fino SA0 Series...",
  },
  {
    indisponivel: false,
    porcetagem_oferta: "12%",
    caminho_img: ImgProduto03,
    nome_produto: "Cadeira Charles Eames Wood Base Madeira - Design...",
    valor_de: "89,90",
    valor_produto: " 79,11",
    parcelas: '8',
    valor_parcelado: (89.90/8)
  },
  {
    indisponivel: false,
    caminho_img: ImgProduto04,
    nome_produto: "Micro-ondas Consul Espelhado - 20L - Cinza 127V - Cm020",
    valor_produto: " 369,99",
    parcelas: '12',
    valor_parcelado: (369.99/12)
  },
]
class PageHome extends Component {
  render() { 
    return ( 
      <div>
        <Helmet>
            <link rel="canonical" href="http://www.gazin.com.br"/>
            <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
            <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
            <meta name="author" content="Luis Alfredo Rosar" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <meta charSet="utf-8" />
            <title>Gazin - Departamentos</title>
        </Helmet>
        <Header/>
        <Categorias/>
        <Breadcrumbs 
          HaveSocial 
          origin="Eletrônicos"
          destination1 = "Celular e Smartphones"
        />
        <BannerPromocao
          isLoja={false}
        />
        <ListaProdutos produtos={produtos} />
        <InformacoesDeEntrega/>
        <InformacoesPagamentoOferta/>
        <footer>
          <div className="container">
            <FooterAreaPrimaria/>
            <FooterAreaSecundaria/>
          </div>
        </footer>
      </div>
    );
  }
}
 
export default PageHome;