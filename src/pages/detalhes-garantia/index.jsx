import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import {
    BannerGarantia,
    ContentDetalhesGarantia,
    Left,
    Right,
    BoxGarantia,
    RightValores
} from './style.js';

import { Helmet } from "react-helmet";

import NavigationProd from '../../components/produtos/navigation-fixed-prod';
import Header from '../../components/header';
import Categorias from '../../components/categoria';
import InformacoesPagamentoOferta from '../../components/pagamento-oferta';
import FooterAreaPrimaria from '../../components/footer/area-primaria';
import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import IconeGarantia from '../../images/icone-seguranca.svg';
import FotoProduto from '../../images/foto-produto-fornecedor.jpg';



class PageHome extends Component {
    state = {
        active: 2
    }
    selectedGarantia = (index) =>{
        this.setState({ active: index }) 
    }
    render() {
        return (
            <div>
                <Helmet>
                    <link rel="canonical" href="http://www.gazin.com.br" />
                    <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!" />
                    <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                    <meta name="author" content="Luis Alfredo Rosar" />
                    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                    <meta charSet="utf-8" />
                    <title>Gazin - Detalhes Garantia</title>
                </Helmet>

                <Header />

                <Categorias />

                <BannerGarantia>
                    <div className="container">
                        <img src={IconeGarantia} alt="" />
                        <div>
                            <h2>Saiba como proteger seu produto por mais tempo.</h2>
                            <span>A contratação se seguros é opcional</span>
                        </div>
                    </div>
                </BannerGarantia>

                <ContentDetalhesGarantia>
                    <div className="container">
                        <Left>
                            <div className="foto">
                                <img src={FotoProduto} alt="" />
                            </div>
                            <h3>iPhone 8 64GB Cinza Espacial Tela 4.7" IOS 4G Câmera 12MP - Apple</h3>
                            <span>cód.: 01010102040</span>
                        </Left>
                        <Right>
                            <div className="geral">
                                <div className="all">
                                    <BoxGarantia 
                                        onClick={() => this.selectedGarantia(1)} 
                                        selected={this.state.active == 1 ? true : false}>
                                        <div className="tipo">
                                            <div className="circle"></div>
                                            <span>Sem garantia</span>
                                        </div>
                                    </BoxGarantia>
                                    <BoxGarantia 
                                        onClick={() => this.selectedGarantia(2)} 
                                        selected={this.state.active == 2 ? true : false}>
                                        <div className="tipo">
                                            <div className="circle"></div>
                                            <span>+ 12 meses por 10x de R$ 50,00 sem juros</span>
                                        </div>
                                        <div className="list">
                                            <ul>
                                                <li>Seu produto garantido por mais tempo</li>
                                                <li>Consertos com peças originais</li>
                                                <li>São mais de 3 mil assistências técnicas em todo o Brasil</li>
                                                <li>Ficou alguma dúvida? Tem atendimento exclusivo</li>
                                            </ul>
                                        </div>
                                    </BoxGarantia>
                                    <BoxGarantia 
                                        onClick={() => this.selectedGarantia(3)} 
                                        selected={this.state.active == 3 ? true : false}>
                                        <div className="tipo">
                                            <div className="circle"></div>
                                            <span>+ 24 meses por 10x de R$ 50,00 sem juros</span>
                                        </div>
                                        <div className="list">
                                            <ul>
                                                <li>Seu produto garantido por mais tempo</li>
                                                <li>Consertos com peças originais</li>
                                                <li>São mais de 3 mil assistências técnicas em todo o Brasil</li>
                                                <li>Ficou alguma dúvida? Tem atendimento exclusivo</li>
                                            </ul>
                                        </div>
                                    </BoxGarantia>
                                </div> 
                                <RightValores>
                                    <h4>Total de serviços selecionados:</h4>
                                    <span>+ 12 meses: R$ 510,00</span>
                                    <h3>10x de R$ 50,00</h3>
                                    <span>s/ juros</span>
                                    <Link to='/carrinho'>Continuar</Link>
                                    <p>Próximo passo: <strong>Carrinho de compras</strong></p>
                                </RightValores>
                            </div>
                            <p>
                                <strong>Termos:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer hendrerit neque augue, vel blandit quam imperdiet nec. Mauris posuere orci ac elit placerat, ac vehicula orci rhoncus. Praesent lobortis fermentum lectus eu mattis. Sed eu faucibus nibh. Sed vulputate gravida tincidunt. Integer interdum efficitur turpis, vel dignissim sapien pulvinar ornare. Sed vel purus et elit placerat pulvinar vel nec nisl. Donec condimentum, sapien ut accumsan mattis, ex arcu ultricies tellus, non pretium purus augue sed nulla.
                            </p>
                        </Right>

                    </div>
                </ContentDetalhesGarantia>



                <InformacoesPagamentoOferta />

                <footer>
                    <div className="container">
                        <FooterAreaPrimaria />
                        <FooterAreaSecundaria />
                    </div>
                </footer>
            </div>
        );
    }
}

export default PageHome;