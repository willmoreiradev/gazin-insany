import styled from 'styled-components';

import CheckBlack from '../../images/checked-black.svg';
import CheckAzul from '../../images/check-azul.svg';

export const BannerGarantia = styled.div`
    background-color: #F6F6F8;
    padding-top: 32px;
    padding-bottom: 28px;
    .container {
        display: flex;
        align-items: center;
    }
    img {
        margin-right: 19px;
    }
    h2 {
        font: normal bold 24px/37px 'Inter';
        color: #363843;
    }
    span {
        font-size: 14px;
        line-height: 24px;
        color: #646981;
    }
    @media(max-width: 1200px) {
        margin-top: 150px;
        .container {
            justify-content: center;
        }
    }
    @media(max-width: 480px) {
        margin-top: 125px;
        padding: 20px 0px;
        h2 {
            font-size: 18px;
            line-height: 24px;
        }
        span {
            font-size: 12px;
        }
        .container {
            div {
                max-width: 230px;
            }
        }
    }
`;

export const ContentDetalhesGarantia = styled.div`
    border-bottom: 1px solid #E8E8E8;
    padding-top: 60px;
    padding-bottom: 112px;
    .container {
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
    }
    @media(max-width: 1200px) {
        padding: 40px 0px;
        .container {
            flex-direction: column;
            align-items: center;
        }
    }
`;

export const Left = styled.div`
    max-width: 295px;
  .foto {
    width: 295px;
    height: 295px;
    background-color: #FFFFFF;
    box-shadow: 0px 4px 9px rgba(54, 56, 67, 0.11);
    border-radius: 6px;
    margin-bottom: 26px;
    display: flex;
    align-items: center;
    justify-content: center;
    img {
        max-width: 100%;
    }
  }
  h3 {
    font-weight: bold;
    font-size: 16px;
    line-height: 25px;
    color: #464646;
    margin-bottom: 5px;
  }
  span {
    font-size: 14px;
    line-height: 29px;
    color: #A0A4B3;
  }
  @media(max-width: 480px) {
    max-width: 100%;
    .foto {
        margin: 0 auto;
        margin-bottom: 40px;
    }
    h3 {
        text-align: center;
    }
    span {
        text-align: center;
        display: block;
    }
  }
`;

export const Right = styled.div`
  max-width: 903px;
  .geral {
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      margin-bottom: 74px;
  }
  p {
    font-size: 14px;
    line-height: 24px;
    color: #646981;
    strong {
        font-size: 14px;
        line-height: 24px;
        color: #646981;
    }
  }
  @media(max-width: 1200px) {
      .geral {
        flex-direction: column;
        align-items: center;
        margin-top: 50px;
        .all {
            width: 100%;
            margin-bottom: 50px;
        }
      }
        p {
            text-align: center;
        }
  }
  @media(max-width: 480px) {
    max-width: 100%;
    margin-top: 30px;
    .geral {
        margin-top: 0;
        margin-bottom: 20px;
    }
  }
`;

export const BoxGarantia = styled.div`
    width: 480px;
    margin-bottom: 12px;
    cursor: pointer;
    &:last-child {
        margin-bottom: 0px;
    }
    .tipo {
        height: 78px;
        background-color: ${props => props.selected ? '#0B57B7' : '#F6F6F6'};
        border: 1px solid ${props => props.selected ? '#0B57B7' : '#DCDDE3'};
        box-sizing: border-box;
        border-radius: 6px;
        padding-left: 24px;
        display: flex;
        align-items: center;
        .circle {
            width: 20px;
            height: 20px;
            background-color: #FFFFFF;
            border: 1px solid #DCDDE3;
            border-radius: 50%;
            margin-right: 22px;
            display: flex;
            align-items: center;
            justify-content: center;
            &:before {
                content: "";
                width: 8px;
                height: 8px;
                background-color: #0D71F0;
                border-radius: 50%;
                transform: ${props => props.selected ? 'scale(1)' : 'scale(0)'};
                transition: all .3s;
            }
        }
        span {
            font-size: 16px;
            line-height: 19px;
            color:  ${props => props.selected ? '#ffffff' : '#646981'};
        }
    }
    .list {
        border: 1px solid #DCDDE3;
        border-top: 1px solid transparent;
        border-radius: 0px 0px 6px 6px;
        padding: 35px 29px;
        margin-top: -5px;
        ul {
            padding-left: 20px;
            li {
                list-style-image: url(${props => props.selected ? CheckAzul : CheckBlack});
                margin-bottom: 20px;
                font-size: 14px;
                color: #646981;
                &:last-child {
                    margin-bottom: 0px;
                }
                
            }
        }
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
    @media(max-width: 480px) {
        .tipo {
            span {
                font-size: 14px;
                max-width: 150px;
            }
        }
        .list {
            padding: 30px 20px;
            ul {
                li {
                    font-size: 14px;
                    line-height: 22px;
                }
            }
        }
    }
`;

export const RightValores = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    h4 {
        font-size: 16px;
        line-height: 24px;
        text-align: center;
        color: #363843;
        margin-bottom: 35px;
    }
    span {
        font-size: 14px;
        line-height: 17px;
        text-align: center;
        color: #646981;
        display: block;
    }
    h3 {
        font-weight: bold;
        font-size: 20px;
        line-height: 24px;
        text-align: center;
        color: #0D71F0;
        margin-top: 28px;
        margin-bottom: 5px;
    }
    a {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 27px;
        width: 281px;
        height: 58px;
        background: #6BB70B;
        border-radius: 8px;
        margin-bottom: 15px;
        font-weight: 600;
        color: #FFFFFF;
        text-transform: uppercase;
        transition: all .3s;
        &:hover {
            background: #549108;
            color: #FFFFFF;
            transition: all .3s;
        }
    }
    p {
        font-size: 13px;
        line-height: 24px;
        text-align: center;
        color: #646981;
        strong {
            font-size: 13px;
            line-height: 24px;
            text-align: center;
            color: #646981;
        }
    }
`;