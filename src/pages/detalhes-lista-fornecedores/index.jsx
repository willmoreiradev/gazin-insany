import React, { Component } from 'react';

import styled from 'styled-components';

import { Helmet } from "react-helmet";

import { Link } from "react-router-dom";

import Icone from '../../components/fontawesome';

import Header from '../../components/header';

import Categorias from '../../components/categoria';

import Breadcrumbs from '../../components/breadcrumbs';

import BuscarCep from '../../components/cep/buscar-cep';

import InformacoesDeEntrega from '../../components/info-entrega';

import InformacoesPagamentoOferta from '../../components/pagamento-oferta';

import FooterAreaPrimaria from '../../components/footer/area-primaria';

import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import ImagemProduto from '../../images/foto-produto-fornecedor.jpg';

import IconeFrete from '../../images/frete-azul.svg';

import LogoGazin from '../../images/logo-gazin-quadrado.jpg';

import LogoSubmarino from '../../images/logo-submarino.jpg';

import IconeCarrinho from '../../images/icone-carrinho-white.svg';


const ContentDetalhesFornecedores = styled.div`
  padding: 30px 0px 130px 0px;
  .tabela-container{
    @media (max-width: 768px) {
        overflow-x: scroll;
    }
  }
`;

const Produto = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 61px;
  border-bottom: 1px solid #F0F0F0;
  margin-bottom: 56px;
  @media (max-width: 768px) {
        flex-direction: column;
        margin-bottom: 26px
    }
`;

const FotoProduto = styled.div`
    width: 302px;
    height: 312.28px;
    background-color: #ffffff;
    box-shadow: 5px 4px 15px rgba(28, 23, 72, 0.06);
    border-radius: 6px;
    border-radius: 3px;
    margin-right: 102px;
    display: flex;
    align-items: center;
    justify-content: center;
    img {
        max-width: 100%;
    }
    @media (max-width: 768px) {
        margin-right: 0;
        flex-direction: column
    }
`;

const InfoProduto = styled.div`
    flex: 1 1 auto;
    margin-right: 60px;
    h2 {
    font-weight: 600;
    font-size: 24px;
    line-height: 36px;
    color: #363843;
    max-width: 551px;
    margin-bottom: 22px;
    }
    a {
        color: #0D71F0;
        font-weight: 600;
        font-size: 14px;
    }
    @media (max-width: 768px) {
        position: relative;
        margin-right: 0;
        display: flex;
        align-items: center;
        flex-direction: column-reverse;
        h2{
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 20px;
            line-height: 28px;
            text-align: center;
            color: #363843;
        }
        a{
            display: none;
            pointer-events: none
        }
    }
`;

const TopoInfoProduto = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 67px;
  h3 {
    font-size: 32px;
    line-height: 37px;
    color: #363843;
  }
  ul {
      display: flex;
      align-items: center;
      margin-right: 14px;
      li {
          margin-left: 10px;
          &:first-child {
              margin-left: 0px;
          }
          span {
              font-size: 20px;
              color: #DCDDE3;
              &.yellow {
                  color: #FFA82E;
              }
          }
      }
  }
  a {
    font-size: 13px;
    line-height: 16px;
    color: #646981;
    font-weight: 400;
  }
    @media (max-width: 768px) {
        margin-bottom: 0;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        h3{
            text-align: center;
            margin-bottom: 26px;
        }
        a{
            display: block;
            pointer-events: all
        }
    }
`;

const Left = styled.div`
    display: flex;
    align-items: center;
    @media (max-width: 768px) {
        width: 100%;
    }   
`;

const Right = styled.div`
    display: flex;
    align-items: center;
    .input {
        margin-right: 0px;
    }
    &.calcfrete {
        button {
            margin-left: 0;
        }
    }
    button {
        margin-left: 20px;
        background-color: transparent;
        &:first-child {
            margin-left: 0px;
        }
        span {
            font-size: 18px;
            color: #A0A4B3;
        }
    }
    @media (max-width: 768px) {
        &.calcfrete{
            width: 100%;
            top: unset;
            right: unset;
            position: relative;
            flex-direction: column;
            align-items: center;
            margin-right: 0;
            margin-bottom: 35px;
            div{
                &:first-child{
                    margin-bottom: 19px;
                }
                &:last-child{
                    flex-direction: column;
                    align-items: flex-start;
                }
            }
            button{
                box-shadow: none;
                border-radius: initial;
                background: initial;
                width: initial;
                height: initial;
            }
        }
        top: -285%;
        right: 35px;
        position: absolute;
        button{
            display: flex;
            align-items: center;
            justify-content: center;
            background: #FFFFFF;
            box-shadow: 1.07955px 2.69886px 6.47727px rgba(22, 24, 41, 0.13);
            border-radius: 50%;
            width: 40px;
            height: 40px;
            &:last-child{
                margin-left: 5px;
            }
        }
    }
`;

const TodasLojas = styled.div`

`;

const TitleCep = styled.div`
    display: flex;
    align-items: center;
    margin-right: 34px;
    img {
        margin-right: 11px;
    }
    span {
        font-size: 14px;
        line-height: 24px;
        color: #646981;
    }

`

const Tabela = styled.table`
    width: 100%;
    border: 1px solid #DDDDDD;
    border-radius: 6px;
    border-collapse: collapse;
    thead {
        th {
            height: 69px;
            border: 1px solid #DDDDDD;
            padding-left: 26px;
            font-weight: 500;
            font-size: 16px;
            line-height: 19px;
            color: #363843;
        }
    }
    tbody {
        tr {
            height: 164px;
            td {
                border: 1px solid #DDDDDD;
                padding: 0 26px;
                h3 {
                    font-weight: 600;
                    font-size: 24px;
                    line-height: 29px;
                    color: #0D71F0;
                    margin-bottom: 2px;
                }
                span {
                    font-size: 14px;
                    line-height: 24px;
                    color: #646981;
                }
                ul {
                    display: flex;
                    align-items: flex-start;
                    li {
                        margin-left: 30px;
                        &:first-child {
                            margin-left: 0px;
                        }
                    }
                }
            }
        }
    }
`;

const LogoLoja = styled.div`
    overflow: hidden;
    width: 112px;
    height: 112px;
    border-radius: 17px;
    display: flex;
    align-items: center;
    justify-content: center;
    img {
         max-width: 224px;
    }
`;

const BotaoComprar = styled.button`
    background: #6BB70B;
    border-radius: 8px;
    width: 208px;
    height: 64px;
    display: flex;
    align-items: center;
    justify-content: center;
    transition: all .3s;
    img {
        margin-right: 10px;
    }
    span {
        font-weight: 600;
        font-size: 18px !important;
        text-transform: uppercase;
        color: #FFFFFF !important;
    }
    &:hover {
        background-color: #5fa00d;
        transition: all .3s;
    }
`;


class DetalheProduto extends Component {
    render() {
        return (
            <div>
                <Helmet>
                    <link rel="canonical" href="http://www.gazin.com.br" />
                    <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!" />
                    <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                    <meta name="author" content="Luis Alfredo Rosar" />
                    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                    <meta charSet="utf-8" />
                    <title>Gazin - Detalhes de Lista de Fornecedores</title>
                </Helmet>
                <Header />
                <Categorias />
                <Breadcrumbs />

                <ContentDetalhesFornecedores>
                    <div className="container">
                        <Produto>
                            <FotoProduto>
                                <img src={ImagemProduto} alt="" />
                            </FotoProduto>
                            <InfoProduto>
                                <TopoInfoProduto>
                                    <Left>
                                        <ul>
                                            <li><Icone nome_icone="star yellow" /></li>
                                            <li><Icone nome_icone="star yellow" /></li>
                                            <li><Icone nome_icone="star yellow" /></li>
                                            <li><Icone nome_icone="star yellow" /></li>
                                            <li><Icone nome_icone="star" /></li>
                                        </ul>
                                        <a href="#">4,9 (21) Avaliar produto</a>
                                    </Left>
                                    <Right>
                                        <button type="button">
                                            <Icone nome_icone="heart-o"></Icone>
                                        </button>
                                        <button type="button">
                                            <Icone nome_icone="share-alt"></Icone>
                                        </button>
                                    </Right>
                                </TopoInfoProduto>
                                <h2>Phone 8 64GB Cinza Espacial Tela 4.7" IOS 4G Câmera 12MP - Apple</h2>
                                <Link to="/detalhes-produto">Voltar para página do produto</Link>
                            </InfoProduto>
                        </Produto>
                        <TodasLojas>
                            <TopoInfoProduto>
                                <h3>Veja todas as lojas com esse produto</h3>
                                <Right className="calcfrete">
                                    <TitleCep>
                                        <img src={IconeFrete} alt="" />
                                        <span>Calcular frete e prazo</span>
                                    </TitleCep>
                                    <BuscarCep mostrar_botao={false} />
                                </Right>
                            </TopoInfoProduto>
                            <div className="tabela-container">
                                <Tabela>
                                    <thead>
                                        <th>
                                            <td>lojas</td>
                                        </th>
                                        <th>
                                            <td>preço produto</td>
                                        </th>
                                        <th>
                                            <td>Frete</td>
                                        </th>
                                        <th colSpan="2">
                                            <td>
                                                preço + frete
                                            </td>
                                        </th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <LogoLoja>
                                                    <img src={LogoGazin} alt="" />
                                                </LogoLoja>
                                            </td>
                                            <td>
                                                <h3>R$ 3.999,00</h3>
                                                <span>ou 12x de R$ 80,90 sem juros</span>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>
                                                        <h4>Entrega</h4>
                                                        <span>Convencional</span>
                                                    </li>
                                                    <li>
                                                        <h4>Frete</h4>
                                                        <span>R$ 15,99</span>
                                                    </li>
                                                    <li>
                                                        <h4>Prazo</h4>
                                                        <span>8 dias</span>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <h3>R$ 3.999,00</h3>
                                                <span>ou 12x de R$ 80,90 sem juros</span>
                                            </td>
                                            <td>
                                                <BotaoComprar>
                                                    <img src={IconeCarrinho} alt="" />
                                                    <span>Comprar</span>
                                                </BotaoComprar>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <LogoLoja>
                                                    <img src={LogoSubmarino} alt="" />
                                                </LogoLoja>
                                            </td>
                                            <td>
                                                <h3>R$ 3.999,00</h3>
                                                <span>ou 12x de R$ 80,90 sem juros</span>
                                            </td>
                                            <td>
                                                <ul>
                                                    <li>
                                                        <h4>Entrega</h4>
                                                        <span>Convencional</span>
                                                    </li>
                                                    <li>
                                                        <h4>Frete</h4>
                                                        <span>R$ 15,99</span>
                                                    </li>
                                                    <li>
                                                        <h4>Prazo</h4>
                                                        <span>8 dias</span>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <h3>R$ 3.999,00</h3>
                                                <span>ou 12x de R$ 80,90 sem juros</span>
                                            </td>
                                            <td>
                                                <BotaoComprar>
                                                    <img src={IconeCarrinho} alt="" />
                                                    <span>Comprar</span>
                                                </BotaoComprar>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Tabela>
                            </div>

                        </TodasLojas>
                    </div>
                </ContentDetalhesFornecedores>


                <InformacoesDeEntrega />
                <InformacoesPagamentoOferta />
                <footer>
                    <div className="container">
                        <FooterAreaPrimaria />
                        <FooterAreaSecundaria />
                    </div>
                </footer>
            </div>
        );
    }
}

export default DetalheProduto;