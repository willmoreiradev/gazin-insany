import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import Header from '../../components/header';

import Categorias from '../../components/categoria';
 
import Breadcrumbs from '../../components/breadcrumbs';

import DetalhesGerais from '../../components/produtos/detalhes';

import SessaoProdutos from '../../components/produtos/index';

import InformacoesDeEntrega from '../../components/info-entrega';

import InformacoesPagamentoOferta from '../../components/pagamento-oferta';

import FooterAreaPrimaria from '../../components/footer/area-primaria';
import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import NavigationFixedProduct from '../../components/produtos/navigation-fixed-prod';




class DetalheProduto extends Component {
  render() { 
    return ( 
      <div>
        <Helmet>
            <link rel="canonical" href="http://www.gazin.com.br"/>
            <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
            <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
            <meta name="author" content="Luis Alfredo Rosar" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <meta charSet="utf-8" />
            <title>Gazin - Detalhes do Produto</title>
        </Helmet>
        <NavigationFixedProduct/>
        <Header/>
        <Categorias/>
        <Breadcrumbs
          origin="Celular e Smartphone"
          destination1="Motorola One Vision"
        />
        <DetalhesGerais
            nome_produto={`Smartphone Motorola One Vision 128GB Dual Chip Android Pie 9.0 Tela 6,3" 4G + Câmera 48+5MP - Azul Safira`}
        />
        <SessaoProdutos
          titulo="Quem viu, viu também"
          subtitulo="Confira outras ofertas"
        />
        <InformacoesDeEntrega/>
        <InformacoesPagamentoOferta/>
        <footer>
          <div className="container">
            <FooterAreaPrimaria/>
            <FooterAreaSecundaria/>
          </div>
        </footer>
      </div>
    );
  }
}
 
export default DetalheProduto;