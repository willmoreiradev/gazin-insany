import React, { Component } from 'react'

import {Helmet} from "react-helmet";

import { Link } from "react-router-dom";

import styled from 'styled-components'

import Header from '../../components/header';

import Categorias from '../../components/categoria'

import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import ListaProdutos from '../../components/produtos/lista-produtos'

import {} from '../../Style/global';

import {InfoFav} from './styles'

import IconFavoritos from '../../images/icon/fav.svg'

import ImgProduto01 from '../../images/produto-01.jpg';
import ImgProduto02 from '../../images/produto-02.jpg';
import ImgProduto03 from '../../images/produto-03.jpg';
import ImgProduto04 from '../../images/produto-04.jpg';

const produtos = [
    {
      indisponivel: false,
      porcetagem_oferta: "30%",
      caminho_img: ImgProduto01,
      nome_produto: "Smartphone Apple Iphone 6S 32GB",
      valor_de: "1489",
      valor_produto: "989",
      parcelas: '12',
      valor_parcelado: (989/12)
    },
    {
      porcetagem_oferta: "30%",
      indisponivel: true,
      caminho_img: ImgProduto02,
      nome_produto: "Monitor Gamer 23'' 1 ms 75Hz Ultra Fino SA0 Series...",
    },
    {
      indisponivel: false,
      porcetagem_oferta: "12%",
      caminho_img: ImgProduto03,
      nome_produto: "Cadeira Charles Eames Wood Base Madeira - Design...",
      valor_de: "89,90",
      valor_produto: " 79,11",
      parcelas: '8',
      valor_parcelado: (89.90/8)
    }
  ]
export default class PageFavoritos extends Component {
    constructor(props){
        super(props);
        this.state = {
            user: {
                name: "Humberto da Silva",
                email: "humberto@email.com"
            }
        }
    }
    render() { 
        return ( 
            <div>
            <Helmet>
                <link rel="canonical" href="http://www.gazin.com.br"/>
                <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
                <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                <meta name="author" content="Luis Alfredo Rosar" />
                <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                <meta charSet="utf-8" />
                <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
            </Helmet>
            <Header
                from="/"
                IsLoggedIn={true}
                userName={this.state.user.name.split(" ")[0]}
            />
            <Categorias />
            <InfoFav>
                <div className="container">
                    <div className="img">
                        <img src={IconFavoritos} alt=""/>
                    </div>
                    <div className="info">
                        <h2>Seus favoritos</h2>
                        <p>
                            Gostou de um produto? Monte sua lista de favoritos com itens do site de seu interesse.
                            Esta é uma maneira mais prática de você compartilhar com seus amigos os produtos que realmente gostaria de ganhar, para que eles acertem em cheio no presente.
                        </p>
                    </div>
                </div>
            </InfoFav>
            <ListaProdutos isFavoritos produtos={produtos} />
            <footer>
                <div className="container">
                <FooterAreaSecundaria/>
                </div>
            </footer>
            </div>
        )
    }
}