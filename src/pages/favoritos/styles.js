import styled from 'styled-components'

export const InfoFav = styled.div`
    background: #F6F6F8;
    padding: 32px 0;
    margin-bottom: 40px;
    .container{
        display: flex;
        align-items: center;
        justify-content: flex-start;
    }
    .img{

    }
    .info{
        padding-left: 28px;
        h2{
            font:normal bold 24px/37px 'Inter';
            color: #363843;
        }
        p{
            max-width: 930px;
            font:normal normal 14px/24px 'Inter';
            color: #646981;
        }
    }
    @media(max-width: 1200px) {
        margin-top: 151px;
        .container {
            flex-direction: column;
            align-items: center;
        }
        .img {
            margin-bottom: 20px;
        }
        .info {
            padding-left: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            p {
                max-width: 100%;
                text-align: center;
            }
        }
    }
    @media(max-width: 480px) {
        margin-top: 125px;
        .img {
            margin-bottom: 10px;
        }
        .info {
            h2 {
                font-size: 22px;
            }
            p {
                font-size: 13px;
            }
        }
    }
`

export const Filtros = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`