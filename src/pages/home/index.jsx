import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import Header from '../../components/header';

import Categorias from '../../components/categoria';

import FullBanner from '../../components/slides/fullbanner';

import BoxesInfoCompra from '../../components/box-info-compra';

import PromocoesBoxes from '../../components/promocoes/boxes';

import SessaoProdutos from '../../components/produtos';

import Departamentos from '../../components/departamento';

import Servicos from '../../components/servicos';

import Lojas from '../../components/lojas';

import InformacoesDeEntrega from '../../components/info-entrega';

import InformacoesPagamentoOferta from '../../components/pagamento-oferta';

import FooterAreaPrimaria from '../../components/footer/area-primaria';
import FooterAreaSecundaria from '../../components/footer/area-secundaria';

class PageHome extends Component {
  render() { 
    return ( 
      <div>
        <Helmet>
            <link rel="canonical" href="http://www.gazin.com.br"/>
            <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
            <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
            <meta name="author" content="Luis Alfredo Rosar" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <meta charSet="utf-8" />
            <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
        </Helmet>
        <Header
          from={this.props.from}
        />
        <Categorias/>
        <FullBanner/>
        <BoxesInfoCompra/>
        <PromocoesBoxes
          titulo="Promoções imperdíveis"
          subtitulo="Confira mais promoções"
        />
        <SessaoProdutos
          titulo="Os mais vendidos dos últimos dias"
          subtitulo="Campeões de venda na Gazin"
        />
        <SessaoProdutos
          titulo="Campeões de venda na Gazin"
          subtitulo="Aqui estão os campeões em visualizações"
        />
        <Departamentos/>
        <Servicos/>
        <Lojas/>
        <InformacoesDeEntrega/>
        <InformacoesPagamentoOferta/>
        <footer>
          <div className="container">
            <FooterAreaPrimaria/>
            <FooterAreaSecundaria/>
          </div>
        </footer>
      </div>
    );
  }
}
 
export default PageHome;