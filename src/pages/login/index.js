import React, { Component } from 'react';

import {Helmet} from "react-helmet";

import { Link } from "react-router-dom";

import styled from 'styled-components'

import Header from '../../components/header';

import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import { StyleModalSenha } from './style';

import EyeOff from '../../images/eye-off.svg'
import EyeOn from '../../images/eye-on.svg'
import SocialLogin from '../../components/socialLogin';
import bagWhite from '../../images/bag-white.svg';
import Close from '../../images/close.svg';
import IconeOk from '../../images/icone-checkado.svg';


const LoginFormContainer = styled.div`
    padding-top: 103px;
    padding-bottom: 83px;

    .container{
        .heading{
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center;
            margin-bottom: 35px;
            h2{
                font-family: 'Inter';
                font-style: normal;
                font-weight: bold;
                font-size: 20px;
                line-height: 26px;
                margin-bottom: 10px;
            }
            span{
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 17px;
            }
        }
    }
    @media(max-width: 1200px) {
        margin-top: 150px;
        padding: 40px 0px;
    }
    @media(max-width: 480px) {
        margin-top: 125px;
        .container {
            margin-bottom: 20px;
        }
        .btnContainer {
            flex-direction: column;
            align-items: center;
        }
    }
`

const LoginForm = styled.form`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

const FormGroup = styled.div`
    width: 467px;
    margin-bottom: 24px;
    .labelContainer{
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        label{
            color: #363843;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
        }

        a{
            color: #646981;
            cursor: pointer;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;

            &:hover{

            }
        }
        
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
`

const InputContainer = styled.div`
    margin-top: 11px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    height: 54px;
    border: 1px solid #DCDDE3;
    box-sizing: border-box;
    border-radius: 6px;
    padding: 0 18px;
    input{
        width: 100%;
        font-family: 'Inter';
        color: #646981;
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 17px;

        &::placeholder{
            font-family: 'Inter';
            color: #A0A4B3;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 17px;
        }
    }
    img{
        cursor: pointer;
        justify-self: flex-end;
    }
`

const BtnSubmit = styled.button`
    width: 467px;
    height: 56px;
    background: #FFA82E;
    border-radius: 6px;

    font-family: 'Inter';
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 19px;
    letter-spacing: -0.114286px;
    text-transform: uppercase;
    color: #FFFFFF;
    transform: scale(1);
    transition: all .3s;

    &:hover{
        transform: scale(1.05);
        transition: all .3s;
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
`

const Cadastrar = styled.div`
  span {
    font-size: 14px;
    line-height: 17px;
    color: #363843;
    text-align: center;
    display: block;
    a {
        font-weight: bold;
        font-size: 14px;
        line-height: 17px;
        color: #0D71F0;
        transition: all .3s;
        &:hover {
            transition: all .3s;
            color: #FFA82E;
        }
    }
  }
`;

class PageLogin extends Component {
    constructor(props){
        super(props);
        this.state = {
            type: 'password',
            modal: false,
            send_password: false
        }
        this.showHide = this.showHide.bind(this);    
    }
    showHide(e){
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            type: this.state.type === 'input' ? 'password' : 'input'
        })  
    }
  render() { 
    return ( 
      <div>
        <Helmet>
            <link rel="canonical" href="http://www.gazin.com.br"/>
            <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
            <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
            <meta name="author" content="Luis Alfredo Rosar" />
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
            <meta charSet="utf-8" />
            <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
        </Helmet>
        <Header
            from={this.props.from}
            identificacao={true}
            pagamento={false}
            obrigado={false}
        />
        <LoginFormContainer>
            <div className="container">
                <div className="heading">
                    <h2>Entrar na minha conta</h2>
                    <span>Acesse sua conta Gazin =)</span>
                </div>
                <LoginForm>
                    <FormGroup>
                        <div className="labelContainer">
                            <label>
                                E-mail
                            </label>
                        </div>
                        <InputContainer>
                            <input type="email" placeholder="Ex.: manuel@email.com" required />
                        </InputContainer>
                    </FormGroup>
                    <FormGroup>
                        <div className="labelContainer">
                            <label>
                                Senha
                            </label>
                            <a href="#" onClick={(e) => {
                                e.preventDefault();
                                this.setState({ modal : true })
                            }}>
                                Esqueceu a senha?
                            </a>
                        </div>
                        <InputContainer>
                            <input type={this.state.type} placeholder="Digite a sua senha" required />
                            <img title="Mostrar Senha" src={this.state.type === 'password' ? EyeOff : EyeOn} onClick={this.showHide} />
                        </InputContainer>
                    </FormGroup>
                    <FormGroup>
                        <BtnSubmit>
                            ENTRAR
                        </BtnSubmit>
                    </FormGroup>
                </LoginForm>
                <SocialLogin
                    texto="Ou acesse com as redes sociais"
                />  
                <Cadastrar>
                    <span>Ainda não possuí cadastro? <Link to={{
                        pathname: "/cadastro",
                        state:{
                            from: this.props.from
                        }
                    }}>Cadastre-se agora</Link></span>
                </Cadastrar>
            </div>
            <StyleModalSenha recuperar_senha={this.state.send_password} modal_senha={this.state.modal}>
                <div className="overlay"></div>
                <div className="box">
                    <div className="topo">
                        <img src={bagWhite}/>
                        <h3>Esqueceu sua senha?</h3>
                        <button className="close" onClick={() => {
                            this.setState({ send_password : false });
                            this.setState({ modal : false })}
                        }><img src={Close}/></button>
                    </div>
                    <div className="body-modal">
                        <div className="recuperar-inicio">
                            <p>Identifique-se para receber um e-mail com as instruções e o link para criar uma nova senha.</p>
                            <form>
                                <div className="form-group">
                                    <label>E-mail</label>
                                    <input type="email" placeholder="Ex.: manuel@email.com"/>
                                </div>
                                <div className="form-group">
                                    <button type="button" onClick={() => this.setState({ send_password : true })}>enviar senha</button>
                                </div>
                            </form>
                        </div>
                        <div className="resposta">
                            <img src={IconeOk}/>
                            <p>Solicitação enviada com sucesso. Verfique seu e-mail para fazer uma nova senha. </p>
                        </div>
                    </div>
                </div>
            </StyleModalSenha>
        </LoginFormContainer>
        <footer>
          <div className="container">
            <FooterAreaSecundaria/>
          </div>
        </footer>
      </div>
    );
  }
}
 
export default PageLogin;