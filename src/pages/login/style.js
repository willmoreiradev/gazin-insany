import styled from 'styled-components';

export const StyleModalSenha = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 35;
    opacity: ${props => props.modal_senha ? '1' : '0'};
    pointer-events: ${props => props.modal_senha ? 'all' : 'none'};
    transition: all .3s;
    .overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 0;
        background-color: rgba(0, 0, 0, 0.5);
    }
    .box {
        position: relative;
        z-index: 1;
        margin: 0 auto;
        width: 468px;
        background-color: #ffffff;
        margin-top: 100px;
        border-radius: 6px;
        transform: ${props => props.modal_senha ? 'translateY(0px)' : ' translateY(100px)'};
        transition: all .3s;
        .topo {
            position: relative;
            height: 69px;
            background: #2A89FF;
            border-radius: 6px 6px 0px 0px;
            display: flex;
            align-items: center;
            padding: 0px 40px;
            img {
                margin-right: 20px;
            }
            h3 {
                font-weight: 600;
                font-size: 16px;
                line-height: 26px;
                color: #FFFFFF;
            }
            .close {
                width: 30px;
                height: 30px;
                position: absolute;
                right: 19px;
                top: -14px;
                border-radius: 50px;
                display: flex;
                align-items: center;
                justify-content: center;
                background-color: #ffffff;
                img{
                    width: 12px;
                    margin: 0;
                }
            }
        }
        .body-modal {
            padding: 40px;
            .recuperar-inicio {
                display: ${props => props.recuperar_senha ? 'none' : 'block'};
            }
            .resposta {
                display: ${props => props.recuperar_senha ? 'flex' : 'none'};
                flex-direction: column;
                align-items: center;
                img {
                    margin-bottom: 27px;
                }
                p {
                    text-align: center;
                    line-height: 20px;
                    max-width: 379px;
                }
            }
            p {
                max-width: 379px;
                font-size: 14px;
                line-height: 20px;
                color: #646981;
                margin-bottom: 21px;
            }
            form {
                .form-group {
                    margin-bottom: 21px;
                }
                label {
                    display: block;
                    font-weight: 500;
                    font-size: 14px;
                    line-height: 17px;
                    color: #363843;
                    margin-bottom: 11px;
                }
                input {
                    border: 1px solid #DCDDE3;
                    border-radius: 6px;
                    padding: 0px 18px;
                    width: 100%;
                    height: 54px;
                    font-size: 14px;
                    color: #A0A4B3;
                }
                button {
                    width: 100%;
                    height: 56px;
                    background-color: #FFA82E;
                    border: 2px solid #FFA82E;
                    border-radius: 6px;
                    text-align: center;
                    color: #FFFFFF;
                    text-transform: uppercase;
                    font-weight: 600;
                    font-size: 16px;
                    transition: all .3s;
                    &:hover {
                        background-color: transparent;
                        color: #FFA82E;
                        transition: all .3s;
                    }
                }
            }
        }
    }
    @media(max-width: 480px) {
        .box {
            width: 90%;
            margin-top: 60px;
            .body-modal {
                padding: 20px;
                p {
                    margin-bottom: 20px;
                    text-align: center;
                    max-width: 260px;
                    margin: 0 auto;
                }
            }
        }
    }
`;