import React, { Component } from 'react'

import {Helmet} from "react-helmet";

import { Link } from "react-router-dom";

import styled from 'styled-components'

import Header from '../../components/header';

import Categorias from '../../components/categoria'

import FooterAreaSecundaria from '../../components/footer/area-secundaria';

import EyeOff from '../../images/eye-off.svg'
import EyeOn from '../../images/eye-on.svg'
import SocialLogin from '../../components/socialLogin';
import BreadCrumbs from '../../components/breadcrumbs';

import IconSair from '../../images/icon/sair.svg'
import IconUser from '../../images/icon/user.svg'
import IconPedidos from '../../images/icon/pedidos.svg'
import IconEnderecos from '../../images/icon/endereco.svg'
import IconPreferencias from '../../images/icon/icon-pref.svg'

import InputMask from "react-input-mask";

import {MinhaContaContainer, LateralNavContainer, LateralNav, Sair, NavItem, NavLink, ActionSection, AlterarFormContainer, AlterarForm, ButtonSubmit} from './style'
import { StyleInput, GroupCheckBox, MultiInput } from '../../Style/global';
import CardAcompanharPedido from '../../components/card-acompanhar-pedido';
import ImgProduto from '../../images/pedido-img.png';


class Action extends Component {
    render() {
        if ((this.props.active == "/conta/dados" || this.props.active == "/conta")  && this.props.alterarDados){
            return(
                <ActionSection>
                    <AlterarFormContainer>
                        <AlterarForm>
                            <h2>Dados pessoa física</h2>
                            <StyleInput>
                                <label htmlFor="">Nome Completo*</label>
                                <input type="text" name="name" placeholder="Seu nome" required/>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">CPF*</label>
                                <InputMask 
                                    type="text"
                                    mask="999.999.999-99"
                                    maskPlaceholder=""
                                    placeholder="Seu CPF"
                                    required
                                />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Telefone*</label>
                                <InputMask 
                                    type="text"
                                    mask="(99) 99999-9999"
                                    maskPlaceholder=""
                                    placeholder="Celular"
                                    required
                                />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Sexo*</label>
                                <GroupCheckBox>
                                    <div>
                                        <input type="radio" name="sexo" id="feminino" checked/>
                                        <label htmlFor="feminino">Feminino</label>
                                    </div>
                                    <div>
                                        <input type="radio" name="sexo" id="masculino"/>
                                        <label htmlFor="masculino">Masculino</label>
                                    </div>
                                </GroupCheckBox>
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Data de nascimento*</label>
                                <InputMask 
                                    type="text"
                                    mask="99/99/9999"
                                    maskPlaceholder=""
                                    placeholder="Data"
                                    required
                                />
                            </StyleInput>
                            <StyleInput>
                                <ButtonSubmit onClick={this.props.handleDadosSubmit}>SALVAR ALTERAÇÕES</ButtonSubmit>
                            </StyleInput>
                        </AlterarForm>
                    </AlterarFormContainer>
                </ActionSection>
            )

        }else if ((this.props.active == "/conta/dados" || this.props.active == "/conta" ) && !this.props.alterarDados){
            return (
                <ActionSection>
                    <div className="minha-conta">
                        <div className="action">
                            <h3>Minha Conta</h3>
                            <span className="nome">
                                {this.props.user.name}
                            </span>
                            <span className="email">
                                {this.props.user.email}
                            </span>
                            <button onClick={this.props.handleAlterarDados}>
                                ALTERAR DADOS
                            </button>
                        </div>
                        <div className="info">
                            <div className="dados">
                                <div className="campo">Nome:</div>
                                <div className="dado">{this.props.user.name}</div>
                            </div>
                            <div className="dados">
                                <div className="campo">CPF:</div>
                                <div className="dado">123.456.789-00</div>
                            </div>
                            <div className="dados">
                                <div className="campo">Data de Nascimento:</div>
                                <div className="dado">12/12/1990</div>
                            </div>
                            <div className="dados">
                                <div className="campo">Telefone:</div>
                                <div className="dado">(12) 3456-7890</div>
                            </div>
                        </div>
                    </div>
                </ActionSection>
            )
        }else if(this.props.active == "/conta/alterar/email" ){
            return(
                <ActionSection>
                    <AlterarFormContainer>
                        <AlterarForm>
                            <h2>Alterar e-mail de acesso</h2>
                            <StyleInput>
                                <label htmlFor="">E-mail atual*</label>
                                <input type="email" name="email_atual" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Novo e-mail* </label>
                                <input type="email" name="novo_email" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Confirme seu novo e-mail*</label>
                                <input type="email" name="confirma_novo_email" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Digite a senha atual*</label>
                                <input type="password" name="senha" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <ButtonSubmit >ATUALIZAR</ButtonSubmit>
                            </StyleInput>
                        </AlterarForm>
                    </AlterarFormContainer>
                </ActionSection>
            )
        } else if (this.props.active == "/conta/alterar/senha") {
            return (
                <ActionSection>
                    <AlterarFormContainer>
                        <AlterarForm>
                            <h2>Alterar senha</h2>
                            <StyleInput>
                                <label htmlFor="">Senha atual*</label>
                                <input type="password" name="senha_atual" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Nova senha*</label>
                                <input type="password" name="nova_senha" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="">Confirme a nova senha*</label>
                                <input type="password" name="confirma_nova_senha" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <ButtonSubmit >ATUALIZAR</ButtonSubmit>
                            </StyleInput>
                        </AlterarForm>
                    </AlterarFormContainer>
                </ActionSection>
            )
        } else if (this.props.active == "/conta/pedidos") {
            return (
                <div>
                    <ActionSection
                        height={96}
                    >
                        <div className="meus-pedidos">
                            <div className="header">
                                <h3>Meus Pedidos</h3>
                            </div>
                            <div className="filter">
                                <span>
                                    Selecione o período
                                </span>
                                <select>
                                    <option value="6m" selected>últimos 6 meses</option>
                                    <option value="1a">último ano</option>
                                </select>
                            </div>
                        </div>
                    </ActionSection>
                    <CardAcompanharPedido 
                        nPedido="6952500413871403"
                        valor={3999.00}
                        imagem={ImgProduto}
                        nome={`IPhone 8 64GB Cinza Espacial Tela 4.7" IOS 4G Câmera 12MP - Apple`}
                        unidades={2}
                        precoFrete={15.99}
                        vendedor="Loja Apple Oficial"
                        nomeCliente="Humberto da Silva"
                        endereco="Rua Marquês de São Vicente, 111, Gávea 99999-999 - Rio de Janeiro/RJ"
                        descontos={0}
                        juros={0}
                    />
                    <CardAcompanharPedido 
                        nPedido="6952500413871403"
                        valor={3999.00}
                        imagem={ImgProduto}
                        nome={`IPhone 8 64GB Cinza Espacial Tela 4.7" IOS 4G Câmera 12MP - Apple`}
                        unidades={2}
                        precoFrete={15.99}
                        vendedor="Loja Apple Oficial"
                        nomeCliente="Humberto da Silva"
                        endereco="Rua Marquês de São Vicente, 111, Gávea 99999-999 - Rio de Janeiro/RJ"
                        descontos={0}
                        juros={0}
                    />
                </div>

            )
        } else if (this.props.active == "/conta/segunda-via") {
            return (
                <ActionSection>
                    <AlterarFormContainer>
                        <AlterarForm>
                            <h2>Solicitar 2ª via do boleto</h2>
                            <span>Digite as informações solicitadas para retirar a 2ª via do boleto.</span>
                            <StyleInput className="formBoleto">
                                <label htmlFor="">Número do pedido</label>
                                <input type="text" name="n_pedido" placeholder="" />
                            </StyleInput>
                            <StyleInput>
                                <ButtonSubmit >GERAR NOVO BOLETO</ButtonSubmit>
                            </StyleInput>
                        </AlterarForm>
                    </AlterarFormContainer>
                </ActionSection>
            )
        }else if (this.props.active == '/conta/enderecos' && this.props.alterarEndereco){
            return(
                <ActionSection>
                <AlterarFormContainer>
                    <AlterarForm>
                        <h2>Endereço</h2>
                        <StyleInput>
                            <div className="topInput">
                                <label htmlFor="cep">CEP*</label>
                                <span>Não sei meu cep</span>
                            </div>
                            <input type="text" name="cep" placeholder="Número do CEP" required />
                        </StyleInput>
                        <MultiInput>
                            <StyleInput>
                                <label htmlFor="endereco">Endereço*</label>
                                <input type="text" name="endereco" placeholder="Rua" required />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="numero">Número*</label>
                                <input type="text" name="numero" placeholder="Nº" required />
                            </StyleInput>
                        </MultiInput>
                        <StyleInput>
                            <label htmlFor="complemento">Complemento</label>
                            <input type="text" name="complemento" placeholder="Opcional" />
                        </StyleInput>
                        <StyleInput>
                            <label htmlFor="bairro">Bairro*</label>
                            <input type="text" name="bairro" placeholder="Nome do bairro" required />
                        </StyleInput>
                        <MultiInput>
                            <StyleInput>
                                <label htmlFor="cidade">Cidade*</label>
                                <input type="text" name="cidade" placeholder="Nome da cidade*" required />
                            </StyleInput>
                            <StyleInput>
                                <label htmlFor="estado">Estado*</label>             
                                <select id="estado" name="estado">
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                    <option value="EX">Estrangeiro</option>
                                </select>
                            </StyleInput>
                        </MultiInput>
                        <StyleInput>
                            <ButtonSubmit onClick={this.props.handleEnderecoSubmit}>SALVAR ALTERAÇÕES</ButtonSubmit>
                        </StyleInput>
                    </AlterarForm>
                </AlterarFormContainer>
            </ActionSection>
            )
        }else if(this.props.active == "/conta/enderecos" && !this.props.alterarEndereco){
            return(
                <ActionSection>
                <div className="enderecos">
                    <div className="action">
                        <div className="info">
                            <h3>Endereço de entrega</h3>
                            <span className="nomeCliente">
                                Humberto da Silva
                            </span>
                            <span className="endereco">
                                Rua Marquês de São Vicente, 111, Gávea 99999-999 - Rio de Janeiro/RJ
                            </span>
                        </div>
                        <button onClick={this.props.handleAlterarEndereco}>
                            EDITAR
                        </button>
                    </div>
                </div>
            </ActionSection>
            )
        }else if (this.props.active == '/conta/preferencias'){
            return(
                <ActionSection style={{justifyContent: 'flex-start'}}>
                <AlterarFormContainer style={{justifyContent: 'flex-start'}}>
                    <AlterarForm className="preferencias" style={{justifyContent: 'flex-start'}}>
                        <h2>Minhas preferências</h2>
                        <div className="title">
                            <span className="ola">Olá Humberto da Silva</span>
                            Deseja receber em seu e-mail lançamentos e ofertas Gazin? 
                        </div>
                        <label class="checkbox-label checkPreferencias">
                            <input name="ofertas" type="checkbox" />
                            <span class="checkbox-custom rectangular"></span>
                            Desejo receber ofertas e promoções da Gazin
                        </label>
                        <div className="checkGroup">
                            <h4>De que forma você deseja receber as nossas informações?</h4>
                            <ul>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="email" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        E-mail
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="telefone" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Telefone
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="sms" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        SMS
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="whatsapp" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Whatsapp
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div className="checkGroup">
                            <h4>Quais são os assuntos de sua preferência?</h4>
                            <ul>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="automotivo" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Automotivo
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="camaMesaBanho" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Cama, mesa e banho
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="comercio" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Comércio e negócios
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="eletronicos" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Eletrônicos
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="esporte" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Esporte e lazer
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="infantil" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Infantil
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="moveis" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Móveis
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="saude" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Saúde e beleza
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="casa" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Casa e segurança
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="eletrodomesticos" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Eletrodomésticos
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="eletroportateis" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Eletroportáteis
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="ferramentas" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Ferramentas
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="informatica" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Informática
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox-label checkPreferencias">
                                        <input name="telefonia" type="checkbox" />
                                        <span class="checkbox-custom rectangular"></span>
                                        Telefonia
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <StyleInput>
                            <ButtonSubmit>SALVAR</ButtonSubmit>
                        </StyleInput>
                    </AlterarForm>
                </AlterarFormContainer>
            </ActionSection>
            )
        } else{
            return null
        }
    }
}


export default class MinhaConta extends Component {        
    constructor(props){
        super(props);
        this.state = {
            alterarDados: false,
            alterarEndereco: false,
            user: {
                name: "Humberto da Silva",
                email: "humberto@email.com"
            }
        }
        this.handleAlterarEndereco = this.handleAlterarEndereco.bind(this)
        this.handleEnderecoSubmit = this.handleEnderecoSubmit.bind(this)
        this.handleAlterarDados = this.handleAlterarDados.bind(this)
        this.handleDadosSubmit = this.handleDadosSubmit.bind(this)

    }
    handleAlterarEndereco(){
        this.setState({alterarEndereco: true})
    }
    handleEnderecoSubmit(event){
        event.preventDefault();
        this.setState({alterarEndereco: false})
    }
    handleAlterarDados(){
        this.setState({alterarDados: true})
    }
    handleDadosSubmit(event){
        event.preventDefault();
        this.setState({alterarDados: false})
    }
    render() { 
        return ( 
            <div>
            <Helmet>
                <link rel="canonical" href="http://www.gazin.com.br"/>
                <meta name="description" content="Compre celulares, televisores, games, tablets, móveis, eletrodomésticos e muito mais aqui na Gazinshop.com.br, tudo em até 10x s/ juros. Aproveite!"/>
                <meta name="keywords" content="gazin, lojas gazin, gazin shop, moveis gazin, gazinshop, casas gazin, " />
                <meta name="author" content="Luis Alfredo Rosar" />
                <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
                <meta charSet="utf-8" />
                <title>Gazin - Loja de telefonia, eletrodomésticos e muito mais!</title>
            </Helmet>
            <Header
                from="/"
                IsLoggedIn={true}
                userName={this.state.user.name.split(" ")[0]}
            />
            <Categorias />
            <MinhaContaContainer>
                <BreadCrumbs
                    origin="Minha Conta"
                />
                <div className="container">
                    <LateralNavContainer >
                        <LateralNav>
                            <NavItem>
                                <div className="title">
                                    <div className="icon">
                                        <img src={IconUser} />
                                    </div>
                                    <h3>Minha Conta</h3>
                                </div>
                                    <ul>
                                    <li>
                                    <NavLink to={{
                                        pathname: "/conta/dados",
                                        state: {
                                            active: "meusDados"
                                        }
                                            }} active={this.props.from == "/conta/dados" || this.props.from == "/conta" ? true : false}>Meus Dados</NavLink>
                                    </li>
                                    <li>
                                    <NavLink to={{
                                        pathname: "/conta/alterar/email",
                                        state: {
                                            active: "alterarEmail" 
                                        }
                                    }} active={this.props.from == "/conta/alterar/email" ? true : false}>Alterar meu e-mail</NavLink>
                                    </li>
                                    <li>
                                    <NavLink to={{
                                        pathname: "/conta/alterar/senha",
                                        state: {
                                            active: "alterarSenha"
                                        }
                                    }} active={this.props.from == "/conta/alterar/senha" ? true : false}>Alterar senha</NavLink>
                                    </li>
                                </ul>
                            </NavItem>
                            <NavItem>
                                <div className="title">
                                    <div className="icon">
                                        <img src={IconPedidos} />
                                    </div>
                                    <h3>Meus pedidos</h3>
                                </div>
                                <ul>
                                    <li><NavLink to="/conta/pedidos" active={this.props.from == "/conta/pedidos" ? true : false}>Acompanhar pedidos</NavLink></li>
                                    <li><NavLink to="/conta/segunda-via" active={this.props.from == "/conta/segunda-via" ? true : false}>Imprimir 2ª via do boleto</NavLink></li>
                                </ul>
                            </NavItem>
                            <NavItem>
                                <div className="title">
                                    <div className="icon">
                                        <img src={IconEnderecos} />
                                    </div>
                                    <h3>Endereços</h3>
                                </div>
                                <ul>
                                    <li><NavLink to="/conta/enderecos" active={this.props.from == "/conta/enderecos" ? true : false}>Meus endereços</NavLink></li>
                                </ul>
                            </NavItem>
                            <NavItem>
                                <div className="title">
                                    <div className="icon">
                                        <img src={IconPreferencias} />
                                    </div>
                                    <h3>Preferências</h3>
                                </div>
                                <ul>
                                    <li><NavLink to="/conta/preferencias" active={this.props.from == "/conta/preferencias" ? true : false}>Minhas preferências</NavLink></li>
                                </ul>
                            </NavItem>
                        </LateralNav>
                        <Sair>
                            <img src={IconSair} />
                            <Link to="/"><h3>Sair</h3></Link>
                        </Sair>
                    </LateralNavContainer>
                    <Action 
                        alterarDados={this.state.alterarDados} 
                        alterarEndereco={this.state.alterarEndereco} 
                        handleAlterarDados={this.handleAlterarDados} 
                        handleDadosSubmit={this.handleDadosSubmit} 
                        handleAlterarEndereco={this.handleAlterarEndereco} 
                        handleEnderecoSubmit={this.handleEnderecoSubmit} 
                        handleInputChange={this.handleInputChange} 
                        user={this.state.user}
                        active={this.props.from ? this.props.from  : "/conta/dados"}
                    />
                </div>

            </MinhaContaContainer>
            <footer>
                <div className="container">
                <FooterAreaSecundaria/>
                </div>
            </footer>
            </div>
        )
    }
}
