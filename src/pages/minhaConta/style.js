import styled from 'styled-components'
import { Link } from 'react-router-dom';

import arrowSelectPedidos from '../../images/arrow-select-pedidos.svg'

export const MinhaContaContainer = styled.div`
    background-color: #F6F6F8;
    padding-bottom: 85px;

    .container{
        display: flex;
    }
    @media(max-width: 1200px) {
        padding-bottom: 40px;
        .container {
            flex-direction: column-reverse;
        }
    }
`;

export const LateralNavContainer = styled.div`
    margin-top: 40px;
`

export const LateralNav = styled.div`
    padding-top: 10px;
    padding-left: 24px;
    padding-right: 24px;
    width: 327px;
    height: 576px;
    background: #FFFFFF;
    box-sizing: border-box;
    border-radius: 6px;
    margin-bottom: 18px;
    @media(max-width: 1200px) {
        margin-right: 0;
        width: 100%;
        height: auto;
    }
`

export const NavItem = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    margin-top: 17px;
    padding-left: 10px;
    padding-bottom: 13px;
    border-bottom: 1px solid #EBEBF0;
    .title{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        .icon{
            width: 36px;
            height: 31px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        h3{
            padding-left: 12px;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 37px;
            color: #363843;
        }
    }

    &:last-child{
        border-bottom: none;
    }

    ul{
        margin-top: 7px;
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        justify-content: flex-start;
        li{
            margin-bottom: 16px;
        }
    }
`

export const NavLink = styled(Link)`
    text-align: left;
    padding-left: 48px;
    font-family: 'Inter';
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 17px;
    color: ${props => props.active ? '#0C64D3' : '#363843'};
    &:hover{
        color: #0C64D3;
    }

`

export const Sair = styled.div`
    padding-left: 34px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    width: 327px;
    height: 74px;
    background: #FFFFFF;
    box-sizing: border-box;
    border-radius: 6px;
    a{
        color: #363843;
        &:hover{
            color: #0C64D3;
        }
    }
    img{
        margin-right: 18px;
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
`

export const ActionSection = styled.div`
    &:first-child{
        margin-top: 40px;
    }
    margin-bottom: 15px;
    margin-left: 77px;
    width: 872px;
    height: ${props => props.height ? `${props.height}px` : 'fit-content'};
    background: #FFFFFF;
    border-radius: 6px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: ${props => props.height ? `0` : '43px'} 43px;
    .formBoleto{
        padding-top: 28px;
    }
    .enderecos{
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        justify-content: space-between;
        width: 788px;
        .action{
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            width: 100%;
            .info{
                display: flex;
                flex-direction: column;
                h3{
                    font: normal 600 18px/31px 'Inter';
                    color: #363843;
                    margin-bottom: 26px;
                }
                .nomeCliente{
                    font: normal 600 16px/27px 'Inter';
                    color: #646981
                }
                .endereco{
                    width: 306px;
                    font: normal normal 16px/27px 'Inter';
                    color: #646981
                }
            }

            button{
                margin-top: 36px;
                width: 281px;
                height: 58px;   
                border: 1px solid #094E99;
                font-family: Inter;
                font-style: normal;
                font-weight: normal;
                font-size: 16px;
                line-height: 23px;
                text-align: center;
                color: #094E99;
                background-color: #ffffff;
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 8px;
                transition: all .3s;
                &:hover{
                    background-color: #094E99;
                    color: #ffffff;
                    transition: all .3s;
                }
            }
        }
    }
    .minha-conta{
        display: flex;
        flex-direction: row;
        align-items: flex-start;
        justify-content: space-between;
        width: 788px;
        .action{
            display: flex;
            flex-direction: column;
            h3{
                font-family: 'Inter';
                font-style: normal;
                font-weight: 600;
                font-size: 18px;
                line-height: 37px;
                color: #363843;
                margin-bottom: 28px;
            }
            .nome{
                display: block;
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 16px;
                line-height: 28px;
                color: #363843;
            }
            .email{
                display: block;
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 16px;
                line-height: 28px;
                color: #646981
            }
            button{
                margin-top: 36px;
                width: 281px;
                height: 58px;   
                border: 1px solid #094E99;
                font-family: Inter;
                font-style: normal;
                font-weight: normal;
                font-size: 16px;
                line-height: 23px;
                text-align: center;
                color: #094E99;
                background-color: #ffffff;
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 8px;
                transition: all .3s;
                &:hover{
                    background-color: #094E99;
                    color: #ffffff;
                    transition: all .3s;
                }
            }
        }
        .info{
            width: 355px;
            height: 216px;
            background: #F6F6F8;
            border-radius: 3px;
            padding-left: 44px;
            padding-top: 33px;
            display: flex;
            flex-direction: column;
            .dados{
                display: flex;
                .campo{
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: 600;
                    font-size: 16px;
                    line-height: 36px;
                    color: #363843;
                }
                .dado{
                    padding-left: 6px;
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: normal;
                    font-size: 16px;
                    line-height: 36px;
                    color: #363843;
                }
            }
        }
    }

    .meus-pedidos{
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        width: 788px;
        .header{
            h3{
                font-family: 'Inter';
                font-style: normal;
                font-weight: bold;
                font-size: 24px;
                line-height: 37px;
                color: #363843;
            }
        }
        .filter{
            span{
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 15px;
                line-height: 18px;
                color: #363843;
                margin-right: 22px;
            }
            select {
                width: 199px;
                height: 54px;
                appearance: none;
                border: 1px solid #DCDDE3;
                border-radius: 6px;
                padding: 0px 20px;
                font-family: 'Inter';
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 17px;
                color: #363843;
                background: url(${arrowSelectPedidos}) no-repeat right 20px center;
            }
        }
        .info{
            width: 355px;
            height: 216px;
            background: #F6F6F8;
            border-radius: 3px;
            padding-left: 44px;
            padding-top: 33px;
            display: flex;
            flex-direction: column;
            .dados{
                display: flex;
                .campo{
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: 600;
                    font-size: 16px;
                    line-height: 36px;
                    color: #363843;
                }
                .dado{
                    padding-left: 6px;
                    font-family: 'Inter';
                    font-style: normal;
                    font-weight: normal;
                    font-size: 16px;
                    line-height: 36px;
                    color: #363843;
                }
            }
        }
    }
    @media(max-width: 1200px) {
        margin-left: 0;
        width: 100%;
        flex-direction: column;
        padding: 30px 20px;
        margin-top: 40px;
        margin-bottom: 0;
        height: auto;
        &:first-child {
            margin-top: 20px;
        }
        .minha-conta {
            width: 100%;
            flex-direction: column;
            align-items: center;
            .action {
                align-items: center;
                width: 100%;
                h3 {
                    line-height: 1;
                    margin-bottom: 15px;
                }
                button {
                    width: 100%;
                    height: 50px;
                    font-size: 15px;
                }
            }
            .info {
                width: 100%;
                padding: 20px;
                height: auto;
                margin-top: 30px;
            }
        }
        .enderecos {
            width: 100%;
            .action {
                flex-direction: column;
                align-items: flex-start;
                button {
                    width: 100%;
                    height: 50px;
                    font-size: 15px;
                }
            }
        }
        .meus-pedidos {
            width: 100%;
            flex-direction: column;
            align-items: center;
            .filter {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 100%;
                margin-top: 10px;
                span {
                    margin-right: 0;
                    margin-bottom: 15px;
                }
                select {
                    width: 100%;
                }
            }
        }
    }
`

export const AlterarFormContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    .preferencias{
        h2{
            margin-bottom: 33px;
        }
        .title{
            font: normal normal 16px/22px 'Inter';
            color: #646981;
            margin-bottom: 16px;
            .ola{
                color: #0C64D3;
                display: block;
            }
            
        }
        .checkGroup{
            margin-bottom: 44px;
            h4{
                font:normal normal 16px/22px 'Inter';
                color: #363843;
                margin-bottom: 16px;
            }
        }
        .checkbox-label {
            position: relative;
            margin-bottom: 15px;
            cursor: pointer;
            font-size: 22px;
            line-height: 24px;
            clear: both;
            padding-left: 37px;
            display: flex;
            align-items: center;
            font: normal normal 16px/19px 'Inter';
            color: #646981;
            height: 20px;
            margin-bottom: 15px;
        }

        .checkbox-label input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        .checkbox-label .checkbox-custom {
            position: absolute;
            top: 0px;
            left: 0px;
            height: 20px;
            width: 20px;
            background-color: transparent;
            border-radius: 5px;
            border: 1px solid #DCDDE3;
        }


        .checkbox-label input:checked ~ .checkbox-custom {
            background-color: #FFFFFF;
            border-radius: 5px;
            -webkit-transform: rotate(0deg) scale(1);
            -ms-transform: rotate(0deg) scale(1);
            transform: rotate(0deg) scale(1);
            opacity:1;
            border: 1px solid #0D71F0;
        }


        .checkbox-label .checkbox-custom::after {
            position: absolute;
            content: "";
            left: 12px;
            top: 12px;
            height: 0px;
            width: 0px;
            border-radius: 5px;
            border: solid #0D71F0;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(0deg) scale(0);
            -ms-transform: rotate(0deg) scale(0);
            transform: rotate(0deg) scale(0);
            opacity:1;
        }


        .checkbox-label input:checked ~ .checkbox-custom::after {
            -webkit-transform: rotate(45deg) scale(1);
            -ms-transform: rotate(45deg) scale(1);
            transform: rotate(45deg) scale(1);
            opacity:1;
            left: 5px;
            top: 1px;
            width: 6px;
            height: 10px;
            border: solid #0D71F0;
            border-width: 0 2px 2px 0;
            background-color: transparent;
            border-radius: 0;
        }

        .checkbox-label input:checked ~ .checkbox-custom::before {
            left: -3px;
            top: -3px;
            width: 24px;
            height: 24px;
            border-radius: 5px;
            -webkit-transform: scale(3);
            -ms-transform: scale(3);
            transform: scale(3);
            opacity:0;
            z-index: 999;
        }
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
    @media(max-width: 480px) {
        .preferencias {
            .checkbox-label {
                height: auto;
                .checkbox-custom {
                    margin-top: 2px;
                }
            }
        }
    }
`

export const AlterarForm = styled.form`
    width: 468px;
    h2{
        font-family: 'Inter';
        font-style: normal;
        font-weight: bold;
        font-size: 24px;
        line-height: 37px;
        color: #363843;
        margin-bottom: 20px;
    }
    label{
        font-family: 'Inter';
        font-style: normal;
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #363843;
    }
    @media(max-width: 1200px) {
        width: 100%;
    }
`

export const ButtonSubmit = styled.button`
    width: 100%;
    height: 56px;
    background-color: #0C64D3;
    border: 2px solid #0C64D3;
    border-radius: 6px;
    font-weight: 600;
    letter-spacing: -0.114286px;
    text-transform: uppercase;
    color: #FFFFFF;
    transition: all .3s;
    &:hover {
        background-color: transparent;
        color: #0C64D3;
        transition: all .3s;
    }
`;