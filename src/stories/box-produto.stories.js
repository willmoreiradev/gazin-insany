import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { BrowserRouter } from 'react-router-dom';

import ImgProduto01 from '../images/produto-01.jpg';
import BoxProduto from '../components/produtos/box-produto';

import '../scss/main.scss';


export const task = {
  id: '1',
  title: 'Box Produto',
  state: 'TASK_INBOX',
  parameters: {
    component: BoxProduto,
    componentSubtitle: 'Displays an image that represents a user or organization',
  },
  updatedAt: new Date(2019, 0, 1, 9, 0),
};


storiesOf('BoxProduto', module)
  .add('default', () => 
    <BrowserRouter>
      <BoxProduto 
        indisponivel={false}
        porcetagem_oferta="30%"
        caminho_img={ImgProduto01}
        nome_produto="Smartphone Apple Iphone 6S 32GB"
        valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
        valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
        valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
        task={task} 
      />
    </BrowserRouter>
  )
  .add('indisponivel', () => 
  <BrowserRouter>
    <BoxProduto 
      indisponivel={true}
      porcetagem_oferta="30%"
      caminho_img={ImgProduto01}
      nome_produto="Smartphone Apple Iphone 6S 32GB"
      valor_de={parseFloat("1489").toFixed(2).replace('.', ',')}
      valor_produto={parseFloat("989").toFixed(2).replace('.', ',')}
      valor_parcelado={parseFloat(989 / 12).toFixed(2).replace('.', ',')}
      task={task} 
    />
  </BrowserRouter>
)